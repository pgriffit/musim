#
# Options specific for a given job
# ie. setting of random number seed and name of output files
#

from Gauss.Configuration import *

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber        = 1082

#--Number of events
nEvts = 5
LHCbApp().EvtMax = nEvts
#simpath = '/st100-gr1/griffith/sim'
simpath = '/home/griffith/muon/leb/'
jname = 'ult_minbias_FTFP_BERT_HP'
name = '{}/{}_{}'.format(simpath,jname, nEvts)

#Gauss().OutputType = 'NONE'
#Gauss().Histograms = 'NONE'
#--Set name of output files for given job (uncomment the lines)
#  Note that if you do not set it Gauss will make a name based on event type,
#  number of events and the date
#idFile = 'GaussTest'
#HistogramPersistencySvc().OutputFile = idFile+'-histos.root'
#
#OutputStream("GaussTape").Output = "DATAFILE='PFN:%s.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"%idFile

#GenMonitor = GaudiSequencer( "GenMonitor" )
#SimMonitor = GaudiSequencer( "SimMonitor" )
#GenMonitor.Members += [ "GaussMonitor::CheckLifeTimeHepMC/HepMCLifeTime" ]
#SimMonitor.Members += [ "GaussMonitor::CheckLifeTimeMC/MCLifeTime" ]

#tape = OutputStream("GaussTape")
#tape.Output = "DATAFILE='PFN:{}.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'".format(name)
#ApplicationMgr( OutStream = [tape] )
histos_name = '{}-histos.root'.format(name)
HistogramPersistencySvc().OutputFile = histos_name
ApplicationMgr().ExtSvc += [ "NTupleSvc" ]
NTupleSvc().Output = ["FILE1 DATAFILE='{}.root' TYP='POOL_ROOTTREE' OPT='NEW'".format(name)]

#importOptions ("./Beam7000GeV-md100-nu7.6-HorExtAngle.py")
from Configurables import MuonHitChecker, MuonMultipleScatteringChecker
hit_monitor = MuonHitChecker('MuonHitChecker')
hit_monitor.DetailedMonitor = False
SimMonitor = GaudiSequencer( "SimMonitor" )
SimMonitor.Members += [
        hit_monitor,]

from Configurables import LHCbApp
from Configurables import CondDB

Gauss().DataType     = "Upgrade" 
CondDB().Upgrade     = True

#LHCbApp().DDDBtag   = "dddb-20190726" 
#LHCbApp().CondDBtag = "sim-20190726-vc-mu100"


#LHCbApp().DDDBtag   = "dddb-20171010"
#LHCbApp().DDDBtag   = "dddb-20171122"
#LHCbApp().DDDBtag   = "dddb-20190912"

#LHCbApp().CondDBtag = "sim-20170301-vc-md100"
#LHCbApp().CondDBtag = "sim-20171127-vc-md100"
#LHCbApp().CondDBtag = "sim-20190912-vc-mu100"
#CondDB().LocalTags["DDDB"] = ["muon-20160511","shielding-20170929"]

LHCbApp().DDDBtag   = "dddb-20190912" 
LHCbApp().CondDBtag = "sim-20190912-vc-mu100  "

## geometry ##

from Configurables import Gauss, CondDB
from Configurables import GiGaInputStream

#CondDB().Upgrade = True
#Gauss().DataType = "Upgrade"
#
#Gauss().DetectorGeo  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet'] }
#Gauss().DetectorSim  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon', 'Magnet' ] }
#Gauss().DetectorMoni = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon', 'Magnet' ] }


Gauss().DetectorGeo  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }
Gauss().DetectorSim  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }
Gauss().DetectorMoni = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon' ] }

#Gauss().DetectorGeo  = { "Detectors": ['VP', 'UT', 'FT', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }
#Gauss().DetectorSim  = { "Detectors": ['VP', 'UT', 'FT', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }
#Gauss().DetectorMoni = { "Detectors": ['VP', 'UT', 'FT', 'Ecal', 'Hcal', 'Muon' ] }

CondDB().Upgrade     = True
Gauss().DataType     = "Upgrade" 


geo = GiGaInputStream('Geo')
geo.StreamItems      += ["/dd/Structure/Infrastructure"]

## upgrade beam paste
from Gaudi.Configuration import *
from Configurables import Gauss

## # Here are beam settings as for various nu (i.e. mu and Lumi per bunch with
## # 25 ns bunch spacing are given
## This is the Run3 default luminosity 
##   nu=7.6 (i.e. mu=5.31, Lumi=2.0*(10**33) with2400 colliding bunches)
importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu7.6-HorExtAngle.py")
## Nominal Lumi to begin with
##   nu=3.8 (i.e. mu=2.66, Lumi=1.0*(10**33) )
#importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu3.8-HorExtAngle.py")
## For robustness studies
##   nu=11.4 (i.e. mu=4, Lumi=1.77*(10**33) )
#importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu11.4-HorExtAngle.py")


## # The spill-over is off for quick tests
## to enable spill-over use the followign options
#importOptions("$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py")

# And the upgrade database is picked up 
# The baseline upgrade configuration is given here with the latest supported global tag for this
# version of Gauss - the FT neutron shield is added by hand in the Baseline options
## LEB cuts ##

#from Configurables import GiGaPhysConstructorOp, GiGaPhysConstructorHpd

from Gaudi.Configuration import *
from GaudiKernel import SystemOfUnits
from Configurables import (GiGa, GiGaPhysListModular,
        GiGaRunActionSequence, TrCutsRunAction)

giga = GiGa()

def setProductionCuts():

    giga.addTool( GiGaPhysListModular("ModularPL") , name="ModularPL" ) 
    Gauss().setPhysList(False)
                                                            
    giga.ModularPL.CutForElectron = 0.5 * SystemOfUnits.mm
    giga.ModularPL.CutForPositron = 0.5 * SystemOfUnits.mm
    giga.ModularPL.CutForGamma    = 0.5 * SystemOfUnits.mm

def setTrackingCuts():
    giga.addTool( GiGaRunActionSequence("RunSeq") , name="RunSeq" )
    giga.RunSeq.addTool( TrCutsRunAction("TrCuts") , name = "TrCuts" )
                                                                
    giga.RunSeq.TrCuts.MuonTrCut     = 10.0 *  SystemOfUnits.MeV 
    giga.RunSeq.TrCuts.pKpiCut       = 0.1  *  SystemOfUnits.MeV
    giga.RunSeq.TrCuts.NeutrinoTrCut = 0.0  *  SystemOfUnits.MeV
    giga.RunSeq.TrCuts.NeutronTrCut  = 0.0  *  SystemOfUnits.MeV
    giga.RunSeq.TrCuts.GammaTrCut    = 0.03 *  SystemOfUnits.MeV
    giga.RunSeq.TrCuts.ElectronTrCut = 0.03 *  SystemOfUnits.MeV
    giga.RunSeq.TrCuts.OtherTrCut    = 0.0  *  SystemOfUnits.MeV


def muonLowEnergySim():
    from Configurables import SimulationSvc
    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/MuonLowEnergy.xml"

muonLowEnergySim()
appendPostConfigAction(setTrackingCuts)
appendPostConfigAction(setProductionCuts)

## physics
Gauss().DeltaRays = True
Gauss().PhysicsList = {"Em":'NoCuts', "Hadron":'FTFP_BERT_HP', "GeneralPhys":True, "LHCbPhys":True}

for det in ['Rich1Pmt, Rich2Pmt']:
    if det in Gauss().DetectorMoni:
        Gauss().DetectorMoni.remove(det)

