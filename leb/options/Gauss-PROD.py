from Gauss.Configuration import *

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber        = 1082

#--Number of events
nEvts = 1000
LHCbApp().EvtMax = nEvts
#simpath = '/st100-gr1/griffith/sim'
simpath = '/home/griffith/muon/leb/'
jname = 'ult_minbias_FTFP_BERT_HP'
name = '{}/{}_{}'.format(simpath,jname, nEvts)

histos_name = '{}-histos.root'.format(name)
HistogramPersistencySvc().OutputFile = histos_name
ApplicationMgr().ExtSvc += [ "NTupleSvc" ]
NTupleSvc().Output = ["FILE1 DATAFILE='{}.root' TYP='POOL_ROOTTREE' OPT='NEW'".format(name)]

from Configurables import MuonHitChecker, MuonMultipleScatteringChecker
hit_monitor = MuonHitChecker('MuonHitChecker')
hit_monitor.DetailedMonitor = False
SimMonitor = GaudiSequencer( "SimMonitor" )
SimMonitor.Members += [
        hit_monitor,]

from Configurables import LHCbApp
from Configurables import CondDB

Gauss().DataType     = "Upgrade" 
CondDB().Upgrade     = True

LHCbApp().DDDBtag   = "dddb-20190223" 
LHCbApp().CondDBtag = "sim-20180530-vc-md100"

importOptions('$LBPYTHIA8ROOT/options/Pythia8.py')
importOptions('$APPCONFIGOPTS/Gauss/TuningPythia8_Sim09.py')
importOptions('$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py')

## geometry ##
from Configurables import Gauss
from Configurables import GaussGeo

Gauss().DetectorGeo  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Infrastructure' ] }
Gauss().DetectorSim  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Infrastructure' ] }
Gauss().DetectorMoni = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon' ] }

geo = GaussGeo()
geo.GeoItemsNames      += ["/dd/Structure/Infrastructure"]

importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu7.6-HorExtAngle.py")


## physics
Gauss().DeltaRays = True
Gauss().PhysicsList = {"Em":'NoCuts', "Hadron":'FTFP_BERT_HP', "GeneralPhys":True, "LHCbPhys":False}

importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')

from Gaudi.Configuration import *
from GaudiKernel import SystemOfUnits
from Configurables import (GiGa, GiGaPhysListModular,
        GiGaRunActionSequence, TrCutsRunAction)

giga = GiGa()

def setProductionCuts():

    giga.addTool( GiGaPhysListModular("ModularPL") , name="ModularPL" ) 
    Gauss().setPhysList(False)
                                                            
    giga.ModularPL.CutForElectron = 0.5 * SystemOfUnits.mm
    giga.ModularPL.CutForPositron = 0.5 * SystemOfUnits.mm
    giga.ModularPL.CutForGamma    = 0.5 * SystemOfUnits.mm

def setTrackingCuts():
    giga.addTool( GiGaRunActionSequence("RunSeq") , name="RunSeq" )
    giga.RunSeq.addTool( TrCutsRunAction("TrCuts") , name = "TrCuts" )
                                                                
    giga.RunSeq.TrCuts.MuonTrCut     = 10.0 *  SystemOfUnits.MeV 
    giga.RunSeq.TrCuts.pKpiCut       = 0.1  *  SystemOfUnits.MeV
    giga.RunSeq.TrCuts.NeutrinoTrCut = 0.0  *  SystemOfUnits.MeV
    giga.RunSeq.TrCuts.NeutronTrCut  = 0.0  *  SystemOfUnits.MeV
    giga.RunSeq.TrCuts.GammaTrCut    = 0.03 *  SystemOfUnits.MeV
    giga.RunSeq.TrCuts.ElectronTrCut = 0.03 *  SystemOfUnits.MeV
    giga.RunSeq.TrCuts.OtherTrCut    = 0.0  *  SystemOfUnits.MeV


def muonLowEnergySim():
    from Configurables import SimulationSvc
    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/MuonLowEnergy.xml"

muonLowEnergySim()
#appendPostConfigAction(setProductionCuts)
#appendPostConfigAction(setTrackingCuts)


