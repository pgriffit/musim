############################################################################
# File for running Gauss with all Baseline Upgrade detectors as of May 2015
############################################################################

from Configurables import Gauss, CondDB
from Configurables import GiGaInputStream
from Gauss.Configuration import * 

CondDB().Upgrade = True
Gauss().DataType = "Upgrade"

Gauss().DetectorGeo  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet'] }
Gauss().DetectorSim  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon', 'Magnet' ] }
Gauss().DetectorMoni = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon', 'Magnet' ] }



geo = GiGaInputStream('Geo')
geo.StreamItems      += ["/dd/Structure/Infrastructure"]


