export simid=$RANDOM
opts="/home/griffith/muon/leb/options"
logdir="/home/griffith/muon/leb/outputs/lt/logs"
logfile=$logdir/lt_${simid}.log
#minbias
evtid="30000000"
optsfile=$DECFILESROOT/options/${evtid}.py
jobfile=$opts/Gauss-LEB-para.py

gaudirun.py $optsfile \
    $jobfile \
    > $logfile 
# Config summary
echo ---------------------------------------------------
echo ------------- GAUSS JOB CONFIGURATION -------------
echo ---------------------------------------------------
echo :: Job UID and seed: $simid ::
echo ---------------------------------------------------
echo ---------------------------------------------------
echo ---------------------------------------------------
