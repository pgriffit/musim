# File containing options to lower thresholds for low energy
# simulation for muon system.
# Options provided by E.Santovetti - 2005-11-03
#
# @author Peter Griffith
# @date 2017-09-27


from Gaudi.Configuration import *
from GaudiKernel import SystemOfUnits
from Configurables import Gauss
from Configurables import (GiGa, GiGaPhysListModular)
from Configurables import (GiGaRunActionSequence, TrCutsRunAction)

#Gauss().PhysicsList = {"Em":'NoCuts', "Hadron":'LHEP', "GeneralPhys":True, "LHCbPhys":True}

def setProductionCuts():
    giga = GiGa()
    giga.addTool( GiGaPhysListModular("ModularPL") , name="ModularPL" ) 
                                                            #2005 cuts:
    giga.ModularPL.CutForElectron = 0.01 * SystemOfUnits.mm #0.5mm
    giga.ModularPL.CutForPositron = 0.01 * SystemOfUnits.mm #0.5mm
    giga.ModularPL.CutForGamma    = 0.01 * SystemOfUnits.mm #0.5mm

def setTrackingCuts():
    giga.addTool( GiGaRunActionSequence("RunSeq") , name="RunSeq" )
    giga.RunSeq.addTool( TrCutsRunAction("TrCuts") , name = "TrCuts" )
                                                                 #2005 cuts:
    GiGa.RunSeq.TrCuts.MuonTrCut     = 1.0 *  SystemOfUnits.MeV #10.0
    GiGa.RunSeq.TrCuts.pKpiCut       = 0.1  *  SystemOfUnits.MeV #0.1
    GiGa.RunSeq.TrCuts.NeutrinoTrCut = 0.0  *  SystemOfUnits.MeV #0.0
    GiGa.RunSeq.TrCuts.NeutronTrCut  = 0.0  *  SystemOfUnits.MeV #0.0
    GiGa.RunSeq.TrCuts.GammaTrCut    = 0.03 *  SystemOfUnits.MeV #0.03
    GiGa.RunSeq.TrCuts.ElectronTrCut = 0.03 *  SystemOfUnits.MeV #0.03
    GiGa.RunSeq.TrCuts.OtherTrCut    = 0.0  *  SystemOfUnits.MeV #0.0


appendPostConfigAction(setProductionCuts)
appendPostConfigAction(setTrackingCuts)


