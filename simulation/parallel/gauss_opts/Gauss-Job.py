import os
#
# Options specific for a given job
# ie. setting of random number seed and name of output files
#

from Gauss.Configuration import *
#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1

uid = os.environ['simid']
GaussGen.RunNumber = int(uid)

#--Number of events
nEvts = int(os.environ['nevts'])
LHCbApp().EvtMax = nEvts
simpath = '/st100-gr1/griffith/sim/out'
jname = '{}_{}'.format(
    os.environ['gtype'],
    os.environ['etype'])

lumi = os.environ['lumi']
name = '{}/{}_{}_{}_uid{}'.format(simpath,jname, lumi, nEvts, uid)


#Gauss().OutputType = 'NONE'
#Gauss().Histograms = 'NONE'
#--Set name of output files for given job (uncomment the lines)
#  Note that if you do not set it Gauss will make a name based on event type,
#  number of events and the date
#idFile = 'GaussTest'
#HistogramPersistencySvc().OutputFile = idFile+'-histos.root'
#
#OutputStream("GaussTape").Output = "DATAFILE='PFN:%s.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"%idFile

#GenMonitor = GaudiSequencer( "GenMonitor" )
#SimMonitor = GaudiSequencer( "SimMonitor" )
#GenMonitor.Members += [ "GaussMonitor::CheckLifeTimeHepMC/HepMCLifeTime" ]
#SimMonitor.Members += [ "GaussMonitor::CheckLifeTimeMC/MCLifeTime" ]

Gauss.DeltaRays = True

tape = OutputStream("GaussTape")
tape.Output = "DATAFILE='PFN:{}.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'".format(name)
ApplicationMgr( OutStream = [tape] )
histos_name = '{}-histos.root'.format(name)
HistogramPersistencySvc().OutputFile = histos_name
ApplicationMgr().ExtSvc += [ "NTupleSvc" ]
NTupleSvc().Output = ["FILE1 DATAFILE='{}.root' TYP='POOL_ROOTTREE' OPT='NEW'".format(name)]

#importOptions ("./Beam7000GeV-md100-nu7.6-HorExtAngle.py")
from Configurables import MuonHitChecker, MuonMultipleScatteringChecker
hit_monitor = MuonHitChecker('MuonHitChecker')
hit_monitor.DetailedMonitor = False
SimMonitor = GaudiSequencer( "SimMonitor" )
SimMonitor.Members += [
        hit_monitor,]
