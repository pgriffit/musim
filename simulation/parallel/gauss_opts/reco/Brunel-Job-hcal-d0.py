from Gaudi.Configuration import *
from Configurables import Brunel, LHCbApp, CondDB
from Configurables import DDDBConf
#LHCbApp().DDDBtag   = "Sim08-20130503-1"
#LHCbApp().CondDBtag = "Sim08-20130503-1-vc-md100"

DDDBConf(DbRoot = "/st100-gr1/griffith/databases/upgrade_reg_hcal/lhcb.xml")
LHCbApp().DDDBtag   = "dddb-20150424"
LHCbApp().CondDBtag = "sim-20140825-vc-mu100"

inputFiles = ['/st100-gr1/griffith/sim/hcal_d0_1000.digi']

from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(inputFiles)
# sets output and histogram file names
CondDB().Upgrade = True

Brunel().Detectors = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon', 'Magnet', 'Tr' ]
Brunel().DataType     = "Upgrade"
#Brunel().DetectorDigi = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon']
#Brunel().DetectorLink = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon', 'Tr']
#Brunel().DetectorMoni = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon', 'Tr', 'MC']

Brunel().DatasetName = inputFiles[0].replace('Moore','Brunel').replace('.digi','')
Brunel().InputType = "DIGI" # input has the format digi
Brunel().WithMC    = True   # use the MC truth information in the digi file


