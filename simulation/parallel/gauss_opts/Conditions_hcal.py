from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB
from Configurables import DDDBConf
#DDDBConf(DbRoot = "/afs/cern.ch/work/p/pgriffit/MUON/panoramix/dddb_private_MF4/lhcb.xml")     
#DDDBConf(DbRoot = "/afs/cern.ch/work/p/pgriffit/public/upgrade_new_plug/lhcb.xml")
DDDBConf(DbRoot = "/st100-gr1/griffith/databases/upgrade_reg_hcal/lhcb.xml")
LHCbApp().DDDBtag   = "dddb-20150424"
LHCbApp().CondDBtag = "sim-20140825-vc-mu100"
#CondDB().LocalTags["DDDB"] = ['Hcal-Local-ShieldingPlug-20140306']

#LHCbApp().DDDBtag   = "dddb-20150724" #current
#LHCbApp().CondDBtag = "sim-20161124-2-vc-mu100" #current

