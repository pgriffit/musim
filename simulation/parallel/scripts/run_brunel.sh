#location of options files
opts='$MUSIMDIR/simulation/parallel/brunel_opts'
source SetupProject.sh Boole v30r2

export simdata=$1

if [[ $1 == *"hcal"* ]]; then
    export gtype='hcal'
elif [[ $1 == *"wall"* ]]; then
    export gtype='wall'
else
    echo "Can't tell what geometry the file ${simdata} uses"
    exit 1
fi

logfile=$2
lb-run Brunel/v51r1 gaudirun.py $opts/Brunel-Conf.py $APPCONFIGOPTS/Brunel/patchUpgrade1.py  > $logfile
