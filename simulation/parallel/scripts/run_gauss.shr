#START=$(date +%s.%N)

#location of Gauss options files
opts='$MUSIMDIR/simulation/parallel/gauss_opts'
#location of log files
logdir='$MUSIMDIR/simulation/parallel/logs/gauss'

# geomtry type, event type and number of events
# e.g ./run_gauss.sh hcal b0 1000
export gtype=$1
export etype=$2
export nevts=$3

export lumi='HL'
#export lumi='1b'

#check geometry arg
if [ $1 == 'hcal' ]
then
    opt_conditions='Conditions_hcal.py'
elif [ $1 == 'wall' ]
then
    opt_conditions='Conditions_muwall.py'
else
    echo geometry type $gtype not recognised
    exit
fi

if [ $lumi == 'HL' ]
then
    opt_beam='Beam-Conditions-Lumi2x.py'
elif [ $1 == '1b' ]
then
    opt_beam='Beam-Conditions-LumiHL.py'
else
    echo Luminosity option $lumi not recognised
    exit
fi

#check event type arg
if [ $2 == 'minbias' ]
then
    evtid='30000000'
#D* -> (D^0-> K pi) pi
elif [ $2 == 'dstar' ]
then
    evtid='27163003'
elif [ $2 == 'b0' ]
then
    evtid='11144050'
else
    echo Event type $etype not recognised
    exit
fi

#create a random id for the job (TODO: verify it is unique)
#also used as random see for Gauss
export simid=$RANDOM

#Setup and run Gauss
source SetupProject.sh Gauss

#Location of DECFILE for decay type
opt_dec=$DECFILESROOT/options/${evtid}.py

#build log file path
logfile=$logdir/${gtype}_${nevts}_${etype}_${simid}.log

# Config summary
echo ---------------------------------------------------
echo ------------- GAUSS JOB CONFIGURATION -------------
echo ---------------------------------------------------
echo :: Geometry type: $gtype ::
echo :: Event type: $etype $evtid ::
echo :: Events: $nevts ::
echo :: Job UID and seed: $simid ::
echo ---------------------------------------------------
echo ---------------------------------------------------
echo ---------------------------------------------------

# Run Gauss
gaudirun.py $opt_dec $opts/$opt_conditions \
    $opts/Gauss-Job.py $opts/$opt_beam \
    $LBPYTHIA8ROOT/options/Pythia8.py \
    $APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py \
    $opts/Gauss-Upgrade-Geom.py \
    $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
    $opts/IgnoreCaliboffDB_LHCBv38r7.py \
    > $logfile

#END=$(date +%s.%N)
#TDIFF=$(echo "$END - $START" | bc)
ENDDATE=$(date)
endstr="Gauss job $simid \n $gtype $etype $evtid $nevts \n $ENDDATE"

#write completion information to end of logfile
echo $endstr >> $logfile

#notify me on telegram
~/moniscripts/jobdone.sh $endstr 

