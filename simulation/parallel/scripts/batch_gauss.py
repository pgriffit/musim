import os
from time import sleep

scripts_path = '$MUSIMDIR/simulation/parallel/'

#number of jobs per condition
njobs = 5

#geometry types
geo = ['hcal', 'wall']

#event types
etype = ['minbias']

#events per job per batch
nevts = ['250']

for g in geo:
    for e in etype:
        for n in nevts:
            for i in range(0, njobs):
                print('submitting {} job with {} and {} events'.format(e, g, n))
                os.system(
                        scripts_path+'scripts/run_gauss.sh {} {} {} &'.format(g, e, n))
                sleep(10)



