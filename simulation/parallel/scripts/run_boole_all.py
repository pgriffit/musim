import os
import sys
scripts_path = '$MUSIMDIR/simulation/parallel/'
for simfile_name in sys.argv[1:]:
    if '.sim' not in simfile_name:
        print('Skipping file: '+ simfile_name)
        continue

    logfile_name = os.path.join(
            scripts_path,
            'logs/boole/',
            simfile_name.split('/')[-1].replace('.sim', '.log'))
    print(logfile_name)
    print('{} {} {} &'.format(
            os.path.join(scripts_path, 'scripts/run_boole.sh'),
            simfile_name,
            logfile_name))

#    print('Submitting Job for sim file: '+simfile_name)

#    os.system('{} {} {} &'.format(
#        os.path.join(scripts_path, 'scripts/run_boole.sh'),
#        simfile_name,
#        logfile_name))






