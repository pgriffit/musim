############################################################################
# File for running Gauss with all Baseline Upgrade detectors as of May 2015
############################################################################

from Configurables import Gauss, CondDB
from Configurables import GiGaInputStream
from Gauss.Configuration import * 

Gauss().DataType = "2016"

#Gauss().DetectorGeo  = { "Detectors": ['Velo', 'PuVeto', 'Spd', 'Prs',   'Rich1', 'Rich2', 'Ecal', 'Hcal', 'Muon', 'Magnet'] }
#Gauss().DetectorSim  = { "Detectors": ['Velo', 'PuVeto', 'Spd', 'Prs',   'Rich1', 'Rich2', 'Ecal', 'Muon', 'Magnet' ] }
#Gauss().DetectorMoni = { "Detectors": ['Velo', 'PuVeto', 'Spd', 'Prs',   'Rich1', 'Rich2', 'Ecal', 'Muon', 'Magnet' ] }


geo = GiGaInputStream('Geo')
geo.StreamItems      += ["/dd/Structure/Infrastructure"]


