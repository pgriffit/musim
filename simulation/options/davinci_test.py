from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

# Stream and stripping line we want to use
stream = 'AllStreams'
line = 'D2hhCompleteEventPromptDst2D2RSLine'

# Create an ntuple to capture D*+ decays from the StrippingLine line
dtt = DecayTreeTuple('DstToD0pi_D0ToKpi')
dtt.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, line)]
dtt.Decay = '[D*(2010)+ -> (D0 -> K- pi+) pi+]CC'
from Configurables import DaVinci

# Configure DaVinci
DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'D0ntuple.root'
DaVinci().PrintFreq = 10
DaVinci().DataType = 'Upgrade'
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
#DaVinci().DDDBtag   = "dddb-20150424"
#DaVinci().CondDBtag = "sim-20140825-vc-mu100"

from GaudiConf import IOHelper

# Use the local input data
IOHelper().inputFiles([
    '/st100-gr1/griffith/sim/wall_d0_1000.dst'
], clear=True)
