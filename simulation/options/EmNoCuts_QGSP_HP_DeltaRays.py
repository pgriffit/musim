from Configurables import Gauss

Gauss.DeltaRays = True
Gauss().PhysicsList = {"Em":'NoCuts', "Hadron":'QGSP_BERT_HP', "GeneralPhys":True, "LHCbPhys":True}
