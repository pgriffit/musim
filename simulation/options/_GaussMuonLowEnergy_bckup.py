# File containing options to lower thresholds for low energy
# simulation for muon system.
# Options provided by E.Santovetti - 2005-11-03
#
# @author Peter Griffith
# @date 2017-09-27


from Gaudi.Configuration import *
from GaudiKernel import SystemOfUnits
from Configurables import (GiGa, GiGaPhysListModular,
        GiGaRunActionSequence, TrCutsRunAction)
from Configurables import GiGaPhysConstructorOp, GiGaPhysConstructorHpd


def setProductionCuts():
    giga = GiGa()
    giga.addTool( GiGaPhysListModular("ModularPL") , name="ModularPL" ) 
    #giga.PhysicsList = "GiGaPhysListModular/ModularPL"
#    giga.ModularPL.addTool( GiGaPhysConstructorOp, name = "GiGaPhysConstructorOp" )
#    giga.ModularPL.addTool( GiGaPhysConstructorHpd, name = "GiGaPhysConstructorHpd" )
#    giga.ModularPL.GiGaPhysConstructorOp.RichOpticalPhysicsProcessActivate = False
#    giga.ModularPL.GiGaPhysConstructorHpd.RichHpdPhysicsProcessActivate = False

    giga.ModularPL.CutForElectron = 0.01 * SystemOfUnits.mm #0.5mm
    giga.ModularPL.CutForPositron = 0.01 * SystemOfUnits.mm #0.5mm
    giga.ModularPL.CutForGamma    = 0.01 * SystemOfUnits.mm #0.5mm

    giga.addTool( GiGaRunActionSequence("RunSeq") , name="RunSeq" )
    giga.RunSeq.addTool( TrCutsRunAction("TrCuts") , name = "TrCuts" )
                                                                 #2005 cuts:
    giga.RunSeq.TrCuts.MuonTrCut     = 1.0 *  SystemOfUnits.MeV #10.0
    giga.RunSeq.TrCuts.pKpiCut       = 0.1  *  SystemOfUnits.MeV #0.1
    giga.RunSeq.TrCuts.NeutrinoTrCut = 0.0  *  SystemOfUnits.MeV #0.0
    giga.RunSeq.TrCuts.NeutronTrCut  = 0.0  *  SystemOfUnits.MeV #0.0
    giga.RunSeq.TrCuts.GammaTrCut    = 0.03 *  SystemOfUnits.MeV #0.03
    giga.RunSeq.TrCuts.ElectronTrCut = 0.03 *  SystemOfUnits.MeV #0.03
    giga.RunSeq.TrCuts.OtherTrCut    = 0.0  *  SystemOfUnits.MeV #0.0
                                                            #2005 cuts:

#def setTrackingCuts():


appendPostConfigAction(setProductionCuts)
#appendPostConfigAction(setTrackingCuts)


