from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB
from Configurables import DDDBConf
#DDDBConf(DbRoot = "/afs/cern.ch/work/p/pgriffit/MUON/panoramix/dddb_private_MF4/lhcb.xml")     
#DDDBConf(DbRoot = "/afs/cern.ch/work/p/pgriffit/public/upgrade_new_plug/lhcb.xml")

#LHCbApp().DDDBtag   = "dddb-20150724" #current
#LHCbApp().CondDBtag = "sim-20170721-2-vc-md100" #current
LHCbApp().DDDBtag   = "dddb-20170721-3"
LHCbApp().CondDBtag = "sim-20170721-2-vc-md100"

CondDB().LocalTags["DDDB"] = ['shielding-20170929']

