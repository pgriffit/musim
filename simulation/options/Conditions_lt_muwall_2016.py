from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB
from Configurables import DDDBConf
#DDDBConf(DbRoot = "/afs/cern.ch/work/p/pgriffit/MUON/panoramix/dddb_private_MF4/lhcb.xml")     
#DDDBConf(DbRoot = "/afs/cern.ch/work/p/pgriffit/public/upgrade_new_plug/lhcb.xml")
#CondDB().LocalTags["DDDB"] = ['Hcal-Local-ShieldingPlug-20140306']

#LHCbApp().DDDBtag   = "dddb-20150724" #current
#LHCbApp().CondDBtag = "sim-20161124-2-vc-mu100" #current

#test:
#LHCbApp().DDDBtag   = "dddb-20170724"
#LHCbApp().CondDBtag = "sim-20170301-vc-md100"

#actual
DDDBConf(DbRoot = "/st100-gr1/griffith/databases/new/2016_iron_wall/lhcb.xml")
LHCbApp().DDDBtag   = "dddb-20171030-3"
LHCbApp().CondDBtag = "sim-20170721-2-vc-md100"
