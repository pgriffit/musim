from Configurables import CondDB
CondDB().Upgrade     = True

from Configurables import Boole
#--Set database tags using those for Sim08
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20150424"
LHCbApp().CondDBtag = "sim-20140825-vc-mu100"

from Gaudi.Configuration import *
from Configurables import DDDBConf
DDDBConf(DbRoot = "/afs/cern.ch/work/p/pgriffit/MUON/databases/upgrade_iron_hcal/lhcb.xml")

from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['/st100-gr1/griffith/sim/muwall_minbias_1000.sim'])

#Boole().DatasetName =  output
from Gaudi.Configuration import *
OutputStream("DigiWriter").Output = "DATAFILE='PFN:{}' TYP='POOL_ROOTTREE' OPT='REC'".format('test_muwall') 
