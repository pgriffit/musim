import sys
import os
import shutil
import fileinput
import string
import subprocess

disk = os.environ['$DISK']
outputpath=disk+'/sim/gangaout'
#outputpath='/home/data_drive/'
#outputpath='/home/data_drive/'
#outputpath = '/afs/cern.ch/user/p/pgriffit/eos/lhcb/user/p/pgriffit/temp2012MagUp'
home = os.environ['HOME']
workdir = os.environ['WORK']
job = sys.argv[1]
input = sys.argv[2]
#outfile = sys.argv[3]
path = workdir+'gangadir/workspace/pgriffit/LocalXML/'+str(job)+'/*/output/'
#path = '/home/pgriffit/gangatemp/'+str(job)+'/*/output/' #only works on desktop (uses local storage)

print path
outfile = input.replace('.root','_'+str(job)+'_merged.root')
output=outputpath+'/'+outfile
command = 'find '+path+' -name '+input+' | xargs hadd -f ' + output

os.system(command)

