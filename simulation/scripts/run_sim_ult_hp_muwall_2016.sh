opts='$MUSIMDIR/simulation/options'

#lblogin='x86_64-slc6-gcc48-opt'
#export CMTCONFIG=${lblogin}
#source LbLogin.sh -c ${lblogin}
#appconfig='v3r210'

#source SetupProject.sh Gauss v47r2 --use "AppConfig '${appconfig}'"

#minbias
evtid="30000000"

#D* -> (D^0-> K pi) pi
#evtid="27163003"

#B0->(jpsi -> mu mu) K pi
#evtid="11144050" 
optsfile=$DECFILESROOT/options/${evtid}.py

# Run Gauss


gaudirun.py $optsfile \
    $opts/Gauss-Job_ult_hp_muwall.py  \
    $opts/Conditions_lt_muwall_2016.py \
    $opts/EmNoCuts_QGSP_HP_DeltaRays.py \
    $APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py\
    $LBPYTHIA8ROOT/options/Pythia8.py \
    $opts/Gauss-Geom-Infra.py \
    $opts/G4Phys_ult.py \
#    $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
#    $opts/IgnoreCaliboffDB_LHCBv38r7.py
   
    #$GAUSSOPTS/ProductionCuts-Low.py \
    #$opts/EMstudy_PGun.py \

#gaudirun.py $optsfile  $opts/Conditions_lt.py $opts/Gauss-Job_lt.py  \
#    $APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py \
#    $opts/GaussMuonLowEnergy.py \
#    $opts/Beam-Conditions-Lumi2x.py \
#    $LBPYTHIA8ROOT/options/Pythia8.py \
#    $opts/Gauss-Upgrade-Geom-Infra.py \
#    $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
## Prepare output
#mv `ls *.sim` Gauss.sim

# Run Boole
#source SetupProject.sh Boole v29r12 --use "AppConfig "${appconfig}
#
#echo "from Gaudi.Configuration import *" >> Boole-Files.py
#echo "EventSelector().Input = [\"DATAFILE='PFN:./Gauss.sim' TYP='POOL_ROOTTREE' OPT='READ'\"]" >> Boole-Files.py
#
#gaudirun.py $opts/Conditions.py Boole-Files.py $APPCONFIGOPTS/Boole/Default.py \
#    $opts/Boole-Upgrade-Geom-OnlyVelo.py $opts/xdigi.py \
#    $opts/VPClusterlink.py $opts/IgnoreCaliboffDB_LHCBv38r7.py




