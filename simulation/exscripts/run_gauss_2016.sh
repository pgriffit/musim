source SetupProject.sh Gauss

#minbias
#evtid="30000000"

#D* -> (D^0-> K pi) pi
#evtid="27163003"
#B0->(jpsi -> mu mu) K pi
#evtid="11144050" 

evtid = $1
decfile=$DECFILESROOT/options/${evtid}.py

# Run Gauss
gaudirun.py $decfile \
    $GAUSSOPTS/Gauss-2016.py \
    $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
    $APPCONFIGOPTS/Conditions/IgnoreCaliboffDB_LHCBv38r7.py 
