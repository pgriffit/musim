import sys
import gaudimodule
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles([sys.argv[1]])

appMgr = gaudimodule.AppMgr()
sel = appMgr.evtsel()
sel.PrintFreq = 100
evt = appMgr.evtsvc()
run_nevents = 1000
ntotevts=1
while ntotevts<=run_nevents :
    ntotevts=ntotevts+1
    appMgr.run(1)
    if not evt['MC/Particles']: break

    hepmcevents = evt['Gen/HepMCEvents']
    mcmuonhits=evt['MC/Muon/Hits']
    for hit in mcmuonhits:
        print hit
        print "==="
        print muon_det.stationID(hit.sensDetID())
