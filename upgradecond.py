from Configurables import LHCbApp
from Configurables import CondDB

Gauss().DataType     = "Upgrade" 
CondDB().Upgrade     = True

LHCbApp().DDDBtag   = "dddb-20190223" 
LHCbApp().CondDBtag = "sim-20180530-vc-md100"
