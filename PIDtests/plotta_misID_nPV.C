
void plotta_misID_nPV(){
 
 
  //TFile *_filepi = TFile::Open("/data/desimone/BDT/AllHits_full2/configurations/apply/pion_nomasscut_wSW_pW_2_pion_1200_5_25_0.5_2_masscut_withPreweight.root");
  TFile *_filepi = TFile::Open("/data/desimone/BDT/CrossedHits_full2/configurations/apply/pion_nomasscut_wSW_pW_2_pion_1200_5_25_0.5_2_masscut_withPreweight.root");
  //TFile *_filepi = TFile::Open("/data/desimone/BDT/CrossedHits_Test5_full2/configurations/apply/pion_nomasscut_wSW_pW_2_pion_1200_5_20_0.5_2_masscut_withPreweight.root");

  TTree* tpi = (TTree*)_filepi->Get("t4");

  ///////
  // Call LHCb style file
  ///////
  gROOT->ProcessLine(".x lhcbStyle.C");

    // create canvas
  Int_t canvasWidth  = 600;
  Int_t canvasHeight = 400;
  TCanvas *c02 = new TCanvas("c02","multipads",canvasWidth,canvasHeight);
  Int_t npx = 1;
  Int_t npy = 3;
  // First, make one large pad to allow fpr space for axis titles:
  //TPad *pad = new TPad("pad","pad", 0.07, 0.07, 0.95, 0.95);
  //TPad *pad = new TPad("pad","pad", 0.07, 0.07, 0.9, 0.9);
  // Subdivide the pad, with no space between the pads:
  c02->Divide(npx,npy,0.0001,0.0001);
  //c02->Draw();
  //pad->Draw();

    // Best is to make titles ourselves:
  TLatex* tyax = new TLatex(0.062, 0.40, "pion MisID probability");
  tyax->SetNDC(kTRUE);    
  tyax->SetTextSize(0.05);      
  tyax->SetTextAngle(90.);
  tyax->Draw();
  TLatex* txax = new TLatex( 0.82, 0.04,"nPV");
  txax->SetNDC(kTRUE);    
  txax->SetTextSize(0.05);      
  txax->Draw();

  Int_t Icol[4] = {1,4,3,6}; // R1=>nero, R2=>blue, R3=>verde, R4=>lilla 
  TMultiGraph *mg[3];
  TGraphErrors *misID[12];

  Int_t nbin = 11;
  Double_t *mis  = new Double_t[nbin];
  Double_t *emis = new Double_t[nbin];
  
  Double_t *npv = new Double_t[nbin];
  Double_t *enpv = new Double_t[nbin];
  npv[0]  = 0.;
  enpv[0] = 0.5;
  for (Int_t i=1;i<nbin;i++){
    npv[i]  = npv[i-1]+1.;
    enpv[i] = 0.5;
  }

  //TCanvas *c1 = new TCanvas();
  Int_t iGraph = 0; 

  for (Int_t iPbin=1; iPbin<4; iPbin++) {

    c02->cd(iPbin); 
    mg[iPbin] = new TMultiGraph;
    TString binP = ""; 
     
    if(iPbin==1){
      binP = "&&(MuP>3000&&MuP<6000)";
    } else if(iPbin==2){
      binP = "&&(MuP>6000&&MuP<10000)";
    } else if(iPbin==3){
      binP = "&&(MuP>10000)";
    } 
  
    for (Int_t iReg=1; iReg<5; iReg++) {

      TString cutNumP = "sigw*(";
      TString cutDenP = "sigw*(";
      TString cutUp   = "(IsMuon==1&&MuPT>800)";
      TString cutDown = "(MuPT>800)";   
      TString CutReg = "";
      if (iReg==1) CutReg = "&&(Reg==1)";
      if (iReg==2) CutReg = "&&(Reg==2)";
      if (iReg==3) CutReg = "&&(Reg==3)";
      if (iReg==4) CutReg = "&&(Reg==4)"; 
      //sprintf(CutReg,"&&(Reg==%d)",iReg);

      cutUp   += binP;
      cutDown += binP; 
      cutUp   += CutReg;
      cutDown += CutReg;
      cutNumP += cutUp+")";
      cutDenP += cutDown+")";
      cout << "Graph index:      " << iPbin << endl;       
      cout << "GraphError index: " << iGraph << endl;
      cout << "CUT pions IsMuon ---------------->" << cutNumP << endl; 
      cout << "CUT pions all    ---------------->" << cutDenP << endl;
      cout << "---------------------------------"  << endl;
      
      // pion sample
      // hnump
      // c1->cd(); 
      tpi->Draw("Npv>>hnump(11,-0.5,10.5)",cutNumP);
      TH1D *hnump = (TH1D*)gDirectory->Get("hnump");      
      // hdenp 
      tpi->Draw("Npv>>hdenp(11,-0.5,10.5)",cutDenP);
      TH1D *hdenp = (TH1D*)gDirectory->Get("hdenp");
     
      for (Int_t i=0;i<nbin;i++){
        mis[i] = 0.0;
        emis[i] = 0.0;
      }
      //=====================:  
      for (Int_t i=0;i<nbin;i++){
	Double_t num = hnump->Integral(i,i+1);
	Double_t den = hdenp->Integral(i,i+1);
        if (den!=0) {
	  mis[i]  = (num/den);
	  emis[i] = sqrt(mis[i]*(1-mis[i])/den);
	}
      }
      
      misID[iGraph] = new TGraphErrors(nbin,npv,mis,enpv,emis);
      //
      misID[iGraph]->GetXaxis()->SetLabelFont(133);    
      misID[iGraph]->GetXaxis()->SetLabelSize(15);    
      misID[iGraph]->GetYaxis()->SetLabelFont(133);    
      misID[iGraph]->GetYaxis()->SetLabelSize(15);    
      misID[iGraph]->GetYaxis()->SetNdivisions(410); // Reduce the number ot ticks
      misID[iGraph]->SetLineWidth(1);  
      misID[iGraph]->SetMarkerSize(0.7);  
      //misID[iGraph]->GetXaxis()->SetTitleSize(0.0); // Don't write automatic axis titles
      //misID[iGraph]->GetYaxis()->SetTitleSize(0.0); // Don't write automatic axis titles
      //
      misID[iGraph]->SetMarkerStyle(21);
      misID[iGraph]->SetMarkerColor(Icol[iReg-1]);
      misID[iGraph]->SetLineColor(Icol[iReg-1]);
      mg[iPbin]->Add(misID[iGraph]);
      iGraph++;

    }
    mg[iPbin]->Draw("ap");
    mg[iPbin]->GetXaxis()->SetLimits(0.0,8.);
  }

}
