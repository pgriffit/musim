// $Id: $
// Include files
//====================================
//
#include <TString.h>
#include <iostream>
#include <fstream>

void confrontaMisID_3bins_soloD0(Int_t WhichCut = 1) {

  TFile *_filepiW = TFile::Open("/data/desimone/MC_wall/histos/wall_Dstar.root");
  TFile *_filepiH = TFile::Open("/data/desimone/MC_wall/histos/hcal_Dstar.root");

  //TFile *_filemuW = TFile::Open("/data/desimone/MC_wall/histos/wall_B0JpsiKpi.root");
  //TFile *_filemuH = TFile::Open("/data/desimone/MC_wall/histos/hcal_B0JpsiKpi.root");

  TTree* tpiW = (TTree*)_filepiW->Get("TupleDstToD0pi_D0ToKpi/DecayTree");
  TTree* tpiH = (TTree*)_filepiH->Get("TupleDstToD0pi_D0ToKpi/DecayTree");

  //TTree* tmuW = (TTree*)_filemuW->Get("TupleB0ToJpsiKpi/DecayTree");
  //TTree* tmuH = (TTree*)_filemuH->Get("TupleB0ToJpsiKpi/DecayTree");

  ///////
  // Call LHCb style file
  ///////
  gROOT->ProcessLine(".x ../../kumache/lhcbStyle.C");

    // create canvas
  Int_t canvasWidth  = 600;
  Int_t canvasHeight = 400;
  TCanvas *c02 = new TCanvas("c02","multipads",canvasWidth,canvasHeight);
  Int_t npx = 1;
  Int_t npy = 2;

  // Subdivide the pad, with no space between the pads:
  c02->Divide(npx,npy,0.0001,0.0001);
  //c02->Draw();
  //pad->Draw();

    // Best is to make titles ourselves:
  TLatex* tyax = new TLatex(0.062, 0.40, "pion MisID probability");
  tyax->SetNDC(kTRUE);    
  tyax->SetTextSize(0.05);      
  tyax->SetTextAngle(90.);
  //tyax->Draw();
  TLatex* txax = new TLatex( 0.82, 0.04,"P(MeV)");
  txax->SetNDC(kTRUE);    
  txax->SetTextSize(0.05);      
  //txax->Draw();

  Int_t Icol[3] = {4,3,2}; 

  const Int_t nbin = 3;  

  Double_t Pbin[nbin] = {3., 8., 55.}; 
  Double_t eP[nbin] = {3., 2., 45.}; 

  Double_t misW[nbin] = {0., 0., 0.};
  Double_t emisW[nbin] = {0., 0., 0.};
  Double_t misH[nbin] = {0., 0., 0.};
  Double_t emisH[nbin] = {0., 0., 0.};

  // WhichCut = 1 ==> IsMuon
  // WhichCut = 2 ==> IsMuonTight
  // WhichCut = 3 ==> IsMuon && (MuonDLL-MuonBkgLL)>2.

  if (WhichCut==1) {
    TString cutNumpp = "((TMath::Abs(piplus_MC_MOTHER_ID)==421)&&(TMath::Abs(piplus_TRUEID)==211)&&(piplus_InAccMuon==1)&&(piplus_isMuon==1))";
    TString cutNumps = "((TMath::Abs(pisoft_MC_MOTHER_ID)==413)&&(TMath::Abs(pisoft_TRUEID)==211)&&(pisoft_InAccMuon==1)&&(pisoft_isMuon==1))";
    //TString cutNumpm = "((TMath::Abs(B0_TRUEID)==511)&&(TMath::Abs(piplus_TRUEID)==211)&&(piplus_InAccMuon==1)&&(piplus_isMuon==1))";
  }
  else if (WhichCut==2){ 
    TString cutNumpp = "((TMath::Abs(piplus_MC_MOTHER_ID)==421)&&(TMath::Abs(piplus_TRUEID)==211)&&(piplus_InAccMuon==1)&&(piplus_isMuonTight==1))";
    TString cutNumps = "((TMath::Abs(pisoft_MC_MOTHER_ID)==413)&&(TMath::Abs(pisoft_TRUEID)==211)&&(pisoft_InAccMuon==1)&&(pisoft_isMuonTight==1))";
    //TString cutNumpm = "((TMath::Abs(B0_TRUEID)==511)&&(TMath::Abs(piplus_TRUEID)==211)&&(piplus_InAccMuon==1)&&(piplus_isMuonTight==1))";
  }
  else if (WhichCut==3) {
    TString cutNumpp = "((TMath::Abs(piplus_MC_MOTHER_ID)==421)&&(TMath::Abs(piplus_TRUEID)==211)&&(piplus_InAccMuon==1)&&(piplus_isMuon==1)&&((piplus_MuonMuLL-piplus_MuonBkgLL)>0.4))";
    TString cutNumps = "((TMath::Abs(pisoft_MC_MOTHER_ID)==413)&&(TMath::Abs(pisoft_TRUEID)==211)&&(pisoft_InAccMuon==1)&&(pisoft_isMuon==1)&&((pisoft_MuonMuLL-pisoft_MuonBkgLL)>0.4))";
    //TString cutNumpm = "((TMath::Abs(B0_TRUEID)==511)&&(TMath::Abs(piplus_TRUEID)==211)&&(piplus_InAccMuon==1)&&(piplus_isMuon==1)&&((piplus_MuonMuLL-piplus_MuonBkgLL)>0.4))";
  }

  TString cutDenpp = "((TMath::Abs(piplus_MC_MOTHER_ID)==421)&&(TMath::Abs(piplus_TRUEID)==211)&&(piplus_InAccMuon==1))";
  TString cutDenps = "((TMath::Abs(pisoft_MC_MOTHER_ID)==413)&&(TMath::Abs(pisoft_TRUEID)==211)&&(pisoft_InAccMuon==1))";   
  //TString cutDenpm = "((TMath::Abs(B0_TRUEID)==511)&&(TMath::Abs(piplus_TRUEID)==211)&&(piplus_InAccMuon==1))";

   c02->cd(1); 
   // spettri impulso
   tpiW->Draw("piplus_P>>momeWpp(10,0.,100000.)",cutDenpp);
   TH1D *momeWpp = (TH1D*)gDirectory->Get("momeWpp");   
   Int_t numWpp = (Int_t) momeWpp->GetEntries(); 
   tpiH->Draw("piplus_P>>momeHpp(10,0.,100000.)",cutDenpp);
   TH1D *momeHpp = (TH1D*)gDirectory->Get("momeHpp");
   Int_t numHpp = (Int_t) momeHpp->GetEntries();   

   //tmuW->Draw("piplus_P>>momeWpm(10,0.,100000.)",cutDenpm);
   //TH1D *momeWpm = (TH1D*)gDirectory->Get("momeWpm");   
   //Int_t numWpm  = (Int_t) momeWpm->GetEntries(); 
   //tmuH->Draw("piplus_P>>momeHpm(10,0.,100000.)",cutDenpm);
   //TH1D *momeHpm = (TH1D*)gDirectory->Get("momeHpm");
   //Int_t numHpm = (Int_t) momeHpm->GetEntries();   

   tpiW->Draw("pisoft_P>>momeWps(10,0.,100000.)",cutDenps);
   TH1D *momeWps = (TH1D*)gDirectory->Get("momeWps");
   Int_t numWps = (Int_t) momeWps->GetEntries();   
   tpiH->Draw("pisoft_P>>momeHps(10,0.,100000.)",cutDenps);
   TH1D *momeHps = (TH1D*)gDirectory->Get("momeHps");  
   Int_t numHps = (Int_t) momeHps->GetEntries();    

   //cout << "Hcal : num pion from D0 = "<<  numHpp <<",   num soft pions = "<< numHps <<", num pion from B0 = " << numHpm << endl; 
   //cout << "Wall : num pion from D0 = "<<  numWpp <<",   num soft pions = "<< numWps <<", num pion from B0 = " << numWpm << endl;
   cout << "Hcal : num pion from D0 = "<<  numHpp <<",   num soft pions = "<< numHps << endl; 
   cout << "Wall : num pion from D0 = "<<  numWpp <<",   num soft pions = "<< numWps << endl;


   tpiW->Draw("(piplus_MuonMuLL-piplus_MuonBkgLL)>>ppDLLW(20,-2.,8.)",cutDenpp);
   TH1D *ppDDLW = (TH1D*)gDirectory->Get("ppDLLW");   
   tpiW->Draw("(pisoft_MuonMuLL-pisoft_MuonBkgLL)>>psDLLW(20,-2.,8.)",cutDenps);
   TH1D *psDDLW = (TH1D*)gDirectory->Get("psDLLW");   

   tpiH->Draw("(piplus_MuonMuLL-piplus_MuonBkgLL)>>ppDLLH(20,-2.,8.)",cutDenpp);
   TH1D *ppDDLH = (TH1D*)gDirectory->Get("ppDLLH");   
   tpiH->Draw("(pisoft_MuonMuLL-pisoft_MuonBkgLL)>>psDLLH(20,-2.,8.)",cutDenpp);
   TH1D *psDDLH = (TH1D*)gDirectory->Get("psDLLH");   

   //plotta
 
   momeHpp->SetLineWidth(1);  
   momeHpp->SetLineStyle(2); 
   momeHpp->SetLineColor(Icol[0]);
   momeHps->SetLineWidth(1);  
   momeHps->SetLineStyle(2); 
   momeHps->SetLineColor(Icol[1]);
   //momeHpm->SetLineWidth(1);  
   //momeHpm->SetLineStyle(2); 
   //momeHpm->SetLineColor(Icol[2]);

   momeWpp->SetLineWidth(1);  
   momeWpp->SetLineStyle(1); 
   momeWpp->SetLineColor(Icol[0]);
   momeWps->SetLineWidth(1);  
   momeWps->SetLineStyle(1); 
   momeWps->SetLineColor(Icol[1]);
   //momeWpm->SetLineWidth(1);  
   //momeWpm->SetLineStyle(1); 
   //momeWpm->SetLineColor(Icol[2]);
   
   /*momeHpm->Draw(""); 
   momeWpm->Draw("same");
   momeHps->Draw("same");
   momeWps->Draw("same"); 
   momeHpp->Draw("same");
   momeWpp->Draw("same");*/
 

   ppDLLH->SetLineWidth(1);  
   ppDLLH->SetLineStyle(2); 
   ppDLLH->SetLineColor(Icol[0]);
   psDLLH->SetLineWidth(1);  
   psDLLH->SetLineStyle(2); 
   psDLLH->SetLineColor(Icol[1]);

   ppDLLW->SetLineWidth(1);  
   ppDLLW->SetLineStyle(1); 
   ppDLLW->SetLineColor(Icol[0]);
   psDLLW->SetLineWidth(1);  
   psDLLW->SetLineStyle(1); 
   psDLLW->SetLineColor(Icol[1]);
   
   psDLLH->Draw("");
   ppDLLW->Draw("same");
   ppDLLW->Draw("same");
   psDLLH->Draw("same"); 

   c02->cd(2); 
   Double_t numpp, numps, numpm;
   Double_t denpp, denps, denpm;
   Double_t num;
   Double_t den;

  for (Int_t iPbin=1; iPbin<4; iPbin++) {
 
    TString binPpp = ""; 
    TString cutNpp = "("+cutNumpp; 
    TString cutDpp = "("+cutDenpp; 
    TString binPps = ""; 
    TString cutNps = "("+cutNumps; 
    TString cutDps = "("+cutDenps; 
    
    //TString cutNpm = "("+cutNumpm; 
    //TString cutDpm = "("+cutDenpm; 
    
    if(iPbin==1){
      binPpp = "&&((piplus_P>3000)&&(piplus_P<6000))";
      binPps = "&&((pisoft_P>3000)&&(pisoft_P<6000))";
    } else if(iPbin==2){
      binPpp = "&&((piplus_P>6000)&&(piplus_P<10000))";
      binPps = "&&((pisoft_P>6000)&&(pisoft_P<10000))";
    } else if(iPbin==3){
      binPpp = "&&(piplus_P>10000)";
      binPps = "&&(pisoft_P>10000)";
    } 

    cutNpp += binPpp+")";
    cutDpp += binPpp+")";
    cutNps += binPps+")";
    cutDps += binPps+")";
    //cutNpm += binPpp+")";
    //cutDpm += binPpp+")";
  
    tpiW->Draw("piplus_P>>hnumWpp(1,0.,100000.)",cutNpp);
    TH1D *hnumWpp = (TH1D*)gDirectory->Get("hnumWpp"); 
    tpiW->Draw("piplus_P>>hdenWpp(1,0.,100000.)",cutDpp);
    TH1D *hdenWpp = (TH1D*)gDirectory->Get("hdenWpp"); 
    tpiW->Draw("pisoft_P>>hnumWps(1,0.,100000.)",cutNps);
    TH1D *hnumWps = (TH1D*)gDirectory->Get("hnumWps"); 
    tpiW->Draw("pisoft_P>>hdenWps(1,0.,100000.)",cutDps);
    TH1D *hdenWps = (TH1D*)gDirectory->Get("hdenWps"); 

    //tmuW->Draw("piplus_P>>hnumWpm(1,0.,100000.)",cutNpm);
    //TH1D *hnumWpm = (TH1D*)gDirectory->Get("hnumWpm"); 
    //tmuW->Draw("piplus_P>>hdenWpm(1,0.,100000.)",cutDpm);
    //TH1D *hdenWpm = (TH1D*)gDirectory->Get("hdenWpm"); 
   
    tpiH->Draw("piplus_P>>hnumHpp(1,0.,100000.)",cutNpp);
    TH1D *hnumHpp = (TH1D*)gDirectory->Get("hnumHpp"); 
    tpiH->Draw("piplus_P>>hdenHpp(1,0.,100000.)",cutDpp);
    TH1D *hdenHpp = (TH1D*)gDirectory->Get("hdenHpp"); 
    tpiH->Draw("pisoft_P>>hnumHps(1,0.,100000.)",cutNps);
    TH1D *hnumHps = (TH1D*)gDirectory->Get("hnumHps"); 
    tpiH->Draw("pisoft_P>>hdenHps(1,0.,100000.)",cutDps);
    TH1D *hdenHps = (TH1D*)gDirectory->Get("hdenHps"); 

    //tmuH->Draw("piplus_P>>hnumHpm(1,0.,100000.)",cutNpm);
    //TH1D *hnumHpm = (TH1D*)gDirectory->Get("hnumHpm"); 
    //tmuH->Draw("piplus_P>>hdenHpm(1,0.,100000.)",cutDpm);
    //TH1D *hdenHpm = (TH1D*)gDirectory->Get("hdenHpm"); 

    Int_t i = iPbin -1;  
    numpp = hnumWpp->GetBinContent(1);
    denpp = hdenWpp->GetBinContent(1);
    numps = hnumWps->GetBinContent(1);
    denps = hdenWps->GetBinContent(1);

    //numpm = hnumWpm->GetBinContent(1);
    //denpm = hdenWpm->GetBinContent(1);

    //num = numpp + numps + numpm;
    //den = denpp + denps + denpm;
    num = numpp + numps;
    den = denpp + denps;
    cout << "MisID check - wall = "<<  iPbin <<", "<< num <<", "<< den << endl; 
    if (den!=0) {
      misW[i]  = (num/den);
      emisW[i] = sqrt(misW[i]*(1-misW[i])/den);
      cout << " MisID pion wall  = " << misW[i]<<"+-"<< emisW[i] << endl;
    }
  
    numpp = hnumHpp->GetBinContent(1);
    denpp = hdenHpp->GetBinContent(1);
    numps = hnumHps->GetBinContent(1);
    denps = hdenHps->GetBinContent(1);

    //numpm = hnumHpm->GetBinContent(1);
    //denpm = hdenHpm->GetBinContent(1);

    //num = numpp + numps + numpm;
    //den = denpp + denps + denpm;
    num = numpp + numps;
    den = denpp + denps;
    cout << "MisID check - Hcal = "<<  iPbin <<", "<< num <<", "<< den << endl;
    if (den!=0) {
      misH[i]  = (num/den);
      emisH[i] = sqrt(misH[i]*(1-misH[i])/den);
      cout << " MisID pion Hcal  = " << misH[i]<<"+-"<< emisH[i] << endl;
    }
    delete hnumWpp;
    delete hdenWpp;
    delete hnumHpp;
    delete hdenHpp;
    delete hnumWps;
    delete hdenWps;
    delete hnumHps;
    delete hdenHps; 
    //delete hnumWpm;
    //delete hdenWpm;
    //delete hnumHpm;
    //delete hdenHpm; 
  }

  TGraphErrors *misIDW = new TGraphErrors(nbin,Pbin,misW,eP,emisW);
  TGraphErrors *misIDH = new TGraphErrors(nbin,Pbin,misH,eP,emisH);

  misIDH->GetXaxis()->SetLabelFont(133);    
  misIDH->GetXaxis()->SetLabelSize(15);    
  misIDH->GetYaxis()->SetLabelFont(133);    
  misIDH->GetYaxis()->SetLabelSize(15);    
  misIDH->GetYaxis()->SetNdivisions(410); // Reduce the number ot ticks
  misIDH->SetLineWidth(1);  
  misIDH->SetMarkerSize(0.7);  
  misIDH->SetMarkerStyle(21);
  misIDH->SetMarkerColor(Icol[0]);
  misIDH->SetLineColor(Icol[0]);
  if (WhichCut==1) {
    misIDH->SetMinimum(0.001);
    misIDH->SetMaximum(0.45);
  }
  if (WhichCut==2) {
    misIDH->SetMinimum(0.01);
    misIDH->SetMaximum(0.25);
25);
  }
  if (WhichCut==3) {
    misIDH->SetMinimum(0.0);
    misIDH->SetMaximum(0.25);
  }

  misIDW->SetLineWidth(1);  
  misIDW->SetMarkerSize(0.7);  
  misIDW->SetMarkerStyle(21);
  misIDW->SetMarkerColor(Icol[1]);
  misIDW->SetLineColor(Icol[1]);

  misIDH->Draw("AP"); 
  misIDW->Draw("PSame"); 

  if (WhichCut==1) c02 -> SaveAs("MisID_IsMuon_3bins_D0.eps");
  if (WhichCut==2) c02 -> SaveAs("MisID_IsMuonTight_3bins_D0.eps");
  if (WhichCut==3) c02 -> SaveAs("MisID_muDLL_3bins_D0.eps");

//////
}
