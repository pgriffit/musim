import ROOT
from uncertainties import ufloat, umath
from array import array

f_old = ROOT.TFile.Open("$DISK/ganga_files/bu_oldm1.root")
t_old = f_old.Get("TupleBuToJpsiK/DecayTree")

assert(t_old)


old_tot_b1 = 0
old_ismu_b1 = 0
old_tot_b2 = 0
old_ismu_b2 = 0

new_tot_b1 = 0
new_ismu_b1 = 0
new_tot_b2 = 0
new_ismu_b2 = 0

for e in t_old:
    if ROOT.TMath.Abs(e.muplus_TRUEID) == 13:
        if e.muplus_P < 50000:
            old_tot_b1+=1
        else:
            old_tot_b2+=1
    
        if e.muplus_isMuonTight == 1:
            if e.muplus_P < 50000:
                old_ismu_b1+=1
            else:
                old_ismu_b2+=1
        
        if e.muminus_P < 50000:
            old_tot_b1+=1
        else:
            old_tot_b2+=1
    
        if e.muminus_isMuonTight == 1:
            if e.muminus_P < 50000:
                old_ismu_b1+=1
            else:
                old_ismu_b2+=1

f_new = ROOT.TFile.Open("$DISK/ganga_files/bu_newm1.root")
t_new = f_new.Get("TupleBuToJpsiK/DecayTree")
assert(t_new)

for e in t_new:
    if ROOT.TMath.Abs(e.muplus_TRUEID) == 13:
        if e.muplus_P < 50000:
            new_tot_b1+=1
        else:
            new_tot_b2+=1
    
        if e.muplus_isMuonTight == 1:
            if e.muplus_P < 50000:
                new_ismu_b1+=1
            else:
                new_ismu_b2+=1
        
        if e.muminus_P < 50000:
            new_tot_b1+=1
        else:
            new_tot_b2+=1
    
        if e.muminus_isMuonTight == 1:
            if e.minus_P < 50000:
                new_ismu_b1+=1
            else:
                new_ismu_b2+=1

print old_tot_b1, old_tot_b2, old_ismu_b1, old_ismu_b2
print new_tot_b1, new_tot_b2, new_ismu_b1, new_ismu_b2

def unc(v):
    return ufloat(v, umath.sqrt(v))

u_old_tot_b1 = unc(old_tot_b1)
u_old_tot_b2 = unc(old_tot_b2)
u_old_ismu_b1 = unc(old_ismu_b1)
u_old_ismu_b2 = unc(old_ismu_b2)

u_new_tot_b1 = unc(new_tot_b1)
u_new_tot_b2 = unc(new_tot_b2)
u_new_ismu_b1 = unc(new_ismu_b1)
u_new_ismu_b2 = unc(new_ismu_b2)

old_eff_bins = [
        u_old_ismu_b1/u_old_tot_b1,
        u_old_ismu_b2/u_old_tot_b2]

new_eff_bins = [
        u_new_ismu_b1/u_new_tot_b1,
        u_new_ismu_b2/u_new_tot_b2]

old_eff_vals = array('d') 
old_eff_vals.fromlist([v.n for v in old_eff_bins])
old_eff_errs = array('d') 
old_eff_errs.fromlist([v.s for v in old_eff_bins])

new_eff_vals = array('d') 
new_eff_vals.fromlist([v.n for v in new_eff_bins])
new_eff_errs = array('d') 
new_eff_errs.fromlist([v.s for v in new_eff_bins])

binxe = array('d')
binxe.fromlist([25000, 75000])


print old_eff_bins
print new_eff_bins
