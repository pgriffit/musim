## Guide for Panoramix v23r2p2
### Setup
Setup project as usual:

    SetupProject Panoramix v23r2p2
And then, depending on the platform you might need to do some additional setup for the openGL drivers. If Panoramix crashes on startup, try setting the following:

    export PATH=/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/bin:${PATH}
    export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/Resources/exlib/exlib/py
    export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/Resources/CoinPython/scripts
    export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/Resources/HEPVis/scripts/Python
    export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/Resources/OnX/scripts/Python
    export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/Resources/OnXLab/scripts/Python
    export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/bin
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/bin
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/lib
    export OSC_HOME_DIR=/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt
    export OSCLIB=/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/lib
    export OSCBIN=/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/bin
    export OSC_home=/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt
    unset SESSION_MANAGER
I've also pushed a shell script to the musim repo with this in it which you can source after running SetupProject

### Running
Panoramix parses options like `gaudirun.py` but requires a few other switches. For geometry checking the best way to run it is like so:

```python $myPanoramix --BareConfig 1 -f none -v my_config.py```
which will skip the default Panoramix setup and use your config (`my_config.py`) instead and tell it to not load an event file.
### Config File
The config file `my_config.py` can look something like:
```
fom Configurables import LHCbApp
from Configurables import DDDBConf
from Configurables import CondDB
DDDBConf(DbRoot = "/afs/cern.ch/work/p/pgriffit/MUON/databases/upgrade_mf4_shield_fnl/lhcb.xml")
LHCbApp().DDDBtag   = "dddb-20150424"
LHCbApp().CondDBtag = "sim-20140825-vc-mu100"

LHCbApp().Simulation = True
CondDB().Upgrade = True
```
for loading a private xml dump from the DDDB, or just the global and local tags if you are using the official databases. Very similar to Gauss etc. You don't have to worry about including the infrastructure in the stream or anything like that as it loads everything from those db tags which you can enable/disable in the GUI.




