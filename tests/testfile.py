import sys
from Gaudi.Configuration import *
from Gauss.Configuration import *
from . import config

opts = config.Config(*sys.argv[1:])
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = opts.seed
nevts = opts.evts
jname = opts.name

tape = OutputStream("GaussTape")
tape.Output = "DATAFILE='PFN:{}_{}.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'".format(
								jname,
								nevts)
ApplicationMgr( OutStream = [tape] )
histos_name = '{}-{}-sim-histos.root'.format(
					jname,
					nevts)
HistogramPersistencySvc().OutputFile = histos_name
ApplicationMgr().ExtSvc += [ "NTupleSvc" ]
NTupleSvc().Output = ["FILE1 DATAFILE='ntout_{}_{}.root' TYP='POOL_ROOTTREE' OPT='NEW'".format(
										jname,
										nevts )]

from Configurables import MuonHitChecker, MuonMultipleScatteringChecker
hit_monitor = MuonHitChecker('MuonHitChecker')
hit_monitor.DetailedMonitor = False
SimMonitor = GaudiSequencer( "SimMonitor" )
SimMonitor.Members += [
        hit_monitor,]


