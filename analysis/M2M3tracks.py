from ROOT import TH1D,TH2D
from ROOT import MakeNullPointer,Math, TMath

from Configurables import LHCbApp
from Configurables import EventClockSvc

from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import *
import GaudiPython 
from GaudiPython import gbl
from GaudiConf import IOHelper

import pickle

from params import *
#speed_of_light, T, tvt, dt, \
#        M2R1area, M2R2area, M3R1area, M3R2area, \
#        WiresPerRegion, CatPadsPerRegion, \
#        M2R1chmarea, jname, run_nevents
#
from stations import MuStation

mu = MuStation(WiresPerRegion, CatPadsPerRegion, tvt, dt)



IOHelper('ROOT').inputFiles(['{}/{}.sim'.format(simpath, jname)]),
### MORE IMPORTS ###

importOptions('$APPCONFIGOPTS/DisableLFC.py') 
EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"
lhcbApp  = LHCbApp()
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'res')
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
import PartProp.Service, PartProp.decorators
DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle'
DataOnDemandSvc().AlgMap['/Event/MC/Vertices']  = 'UnpackMCVertex'
DataOnDemandSvc().AlgMap['/Event/MC/Muon/Hits']  = 'DataPacking::Unpack<LHCb::MCMuonHitPacker>/UnpackMCMuonHits'
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
lhcbApp.DataType = "Upgrade"
from Configurables import CondDB
CondDB().Upgrade = True
lhcbApp.Simulation  = True
import GaudiPython,ROOT,math,sys
from ROOT import *
from math import *
from array import *
import GaudiKernel.SystemOfUnits as units
import pickle, pprint
appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
sel.PrintFreq = 10000
evt = appMgr.evtsvc()

### HISTOGRAMS AND GRAPHS###

hs=[]
hnvtx=TH1D("hnvtx","nvtx",11,-0.5,10.5); hs.append(hnvtx)
hentz=TH1D("hentz","z of entry",100,10e3,20e3); hs.append(hentz)
horiz=TH1D("horiz","z of particle origin",150,10e3,25e3); hs.append(horiz)
horixy=TH2D("horixy","xy of particle origin",140,-7000.0,7000.0,140,-7000.0,7000.0); hs.append(horixy)
horizx=TH2D("horixz","zx of particle origin",300,10e3,25e3,140,-7000.0,7000.0); hs.append(horizx)
horizy=TH2D("horiyz","zy of particle origin",300,10e3,25e3,140,-7000.0,7000.0); hs.append(horizy)
hhitspertrack=TH1D("hhitspertrack","number of hits produced by one track",41,-0.5,40.5); hs.append(hhitspertrack)
hkeymulti=TH1D("hkeymulti","occurrence of keys",20001,-0.5,20000.5); hs.append(hkeymulti)
hentt=TH1D("hentt","time of entry",100,0.0,200.0); hs.append(hentt)
hdispz=TH1D("hdispz","z displacement",100,-10.0,10.0); hs.append(hdispz)
htprime=TH1D("htprime","path-corrected time",100,-50.0,150.0); hs.append(htprime)
hentxyM1=TH2D("horixyM1","xy of tracks entries in M1",140,-7000.0,7000.0,140,-7000.0,7000.0); hs.append(hentxyM1)
hentxyM2=TH2D("horixyM2","xy of tracks entries in M2",140,-7000.0,7000.0,140,-7000.0,7000.0); hs.append(hentxyM2)
hentxyM3=TH2D("horixyM3","xy of tracks entries in M3",140,-7000.0,7000.0,140,-7000.0,7000.0); hs.append(hentxyM3)
hentxyM4=TH2D("horixyM4","xy of tracks entries in M4",140,-7000.0,7000.0,140,-7000.0,7000.0); hs.append(hentxyM4)
hentxyM5=TH2D("horixyM5","xy of tracks entries in M5",140,-7000.0,7000.0,140,-7000.0,7000.0); hs.append(hentxyM5)

# special M2R1R2 histograms

hentxyM2R1R2small=TH2D("horixyM2R1R2small","xy of hits entries in M2R1R2",200,-1400.0,1400.0,200,-1200.0,1200.0); hs.append(hentxyM2R1R2small)
hentxyM2R1R2medium=TH2D("horixyM2R1R2medium","xy of hits entries in M2R1R2",100,-1400.0,1400.0,100,-1200.0,1200.0); hs.append(hentxyM2R1R2medium)
hentxyM2R1R2big=TH2D("horixyM2R1R2big","xy of hits entries in M2R1R2",50,-1400.0,1400.0,50,-1200.0,1200.0); hs.append(hentxyM2R1R2big)
hentxyM2R1R2huge=TH2D("horixyM2R1R2huge","xy of hits entries in M2R1R2",25,-1400.0,1400.0,25,-1200.0,1200.0); hs.append(hentxyM2R1R2huge)

hentxyM2R1R2small2=TH2D("horixyM2R1R2small2","xy of hits entries in M2R1R2",200,-1240.5,1240.5,200,-1020.5,1020.5); hs.append(hentxyM2R1R2small2)
hentxyM2R1R2medium2=TH2D("horixyM2R1R2medium2","xy of hits entries in M2R1R2",100,-1240.5,1240.5,100,-1020.5,1020.5); hs.append(hentxyM2R1R2medium2)
hentxyM2R1R2big2=TH2D("horixyM2R1R2big2","xy of hits entries in M2R1R2",50,-1240.5,1240.5,50,-1020.5,1020.5); hs.append(hentxyM2R1R2big2)
hentxyM2R1R2huge2=TH2D("horixyM2R1R2huge2","xy of hits entries in M2R1R2",25,-1240.5,1240.5,25,-1020.5,1020.5); hs.append(hentxyM2R1R2huge2)

# M2R1 histograms
hentxyM2R1=TH2D("horixyM2R1","xy of tracks entries in M2R1",100,-700.0,700.0,100,-600.0,600.0); hs.append(hentxyM2R1)
hhitspertrackM2R1=TH1D("hhitspertrackM2R1","number of hits in M2R1 produced by one track",31,-0.5,30.5); hs.append(hhitspertrackM2R1)
hhitspertrackM2R1.SetFillColor(38)
hentChannelM2R1=TH2D("hentChannelM2R1","row vs col in M2R1",192,-0.5,191.5,32,-0.5,31.5); hs.append(hentChannelM2R1)
hentWireM2R1=TH2D("hentWireM2R1","wire tracks in M2R1",192,-0.5,191.5,4,-0.5,3.5); hs.append(hentWireM2R1)
hentCathodePadM2R1=TH2D("hentCathodePadM2R1","cathode pad tracks in M2R1",32,-0.5,31.5,32,-0.5,31.5); hs.append(hentCathodePadM2R1)
htracksM2R1=TH1D("htracksM2R1","number of tracks per event in M2R1",501,-0.5,500.5); hs.append(htracksM2R1)
htracksM2R1.SetFillColor(38)
hangleYZM2R1=TH2D("hangleYZM2R1","Number of gaps penetrated by one track depending on YZ angle (in M2R1)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleYZM2R1)
(hangleYZM2R1.GetXaxis()).SetTitle("YZ angle [rad]"); (hangleYZM2R1.GetXaxis()).SetTitleOffset(2.0); (hangleYZM2R1.GetXaxis()).CenterTitle(True)
(hangleYZM2R1.GetYaxis()).SetTitle("#gaps"); (hangleYZM2R1.GetYaxis()).SetTitleOffset(2.0); (hangleYZM2R1.GetYaxis()).CenterTitle(True)
hangleXZM2R1=TH2D("hangleXZM2R1","Number of gaps penetrated by one track depending on XZ angle (in M2R1)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleXZM2R1)
(hangleXZM2R1.GetXaxis()).SetTitle("XZ angle [rad]"); (hangleXZM2R1.GetXaxis()).SetTitleOffset(2.0); (hangleXZM2R1.GetXaxis()).CenterTitle(True)
(hangleXZM2R1.GetYaxis()).SetTitle("#gaps"); (hangleXZM2R1.GetYaxis()).SetTitleOffset(2.0); (hangleXZM2R1.GetYaxis()).CenterTitle(True)
hangleYXM2R1=TH2D("hangleYXM2R1","Number of gaps penetrated by one track depending on YX angle (in M2R1)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleYXM2R1)
(hangleYXM2R1.GetXaxis()).SetTitle("YX angle [rad]"); (hangleYXM2R1.GetXaxis()).SetTitleOffset(2.0); (hangleYXM2R1.GetXaxis()).CenterTitle(True)
(hangleYXM2R1.GetYaxis()).SetTitle("#gaps"); (hangleYXM2R1.GetYaxis()).SetTitleOffset(2.0); (hangleYXM2R1.GetYaxis()).CenterTitle(True)

hentChannelM2R1C11=TH2D("hentChannelM2R1C11","row vs col in M2R1C11",48,-0.5,47.5,8,-0.5,7.5); hs.append(hentChannelM2R1C11)
hentWireM2R1C11=TH2D("hentWireM2R1C11","wire tracks in M2R1C11",48,-0.5,47.5,1,-0.5,0.5); hs.append(hentWireM2R1C11)
hentCathodePadM2R1C11=TH2D("hentCathodePadM2R1C11","cathode pad in M2R1C11",8,-0.5,7.5,8,-0.5,7.5); hs.append(hentCathodePadM2R1C11)
htracksM2R1C11=TH1D("htracksM2R1C11","number of tracks per event in M2R1C11",241,-0.5,240.5); hs.append(htracksM2R1C11)

hentChannelM2R1C3=TH2D("hentChannelM2R1C3","row vs col in M2R1C3",48,47.5,95.5,8,-0.5,7.5); hs.append(hentChannelM2R1C3)
hentWireM2R1C3=TH2D("hentWireM2R1C3","wire tracks in M2R1C3",48,47.5,95.5,1,-0.5,0.5); hs.append(hentWireM2R1C3)
hentCathodePadM2R1C3=TH2D("hentCathodePadM2R1C3","cathode pad tracks in M2R1C3",8,7.5,15.5,8,-0.5,7.5); hs.append(hentCathodePadM2R1C3)
htracksM2R1C3=TH1D("htracksM2R1C3","number of tracks per event in M2R1C3",241,-0.5,240.5); hs.append(htracksM2R1C3)

hentChannelM2R1C4=TH2D("hentChannelM2R1C4","row vs col in M2R1C4",48,95.5,143.5,8,-0.5,7.5); hs.append(hentChannelM2R1C4)
hentWireM2R1C4=TH2D("hentWireM2R1C4","wire tracks in M2R1C4",48,95.5,143.5,1,-0.5,0.5); hs.append(hentWireM2R1C4)
hentCathodePadM2R1C4=TH2D("hentCathodePadM2R1C4","cathode pad tracks in M2R1C4",8,15.5,23.5,8,-0.5,7.5); hs.append(hentCathodePadM2R1C4)
htracksM2R1C4=TH1D("htracksM2R1C4","number of tracks per event in M2R1C4",241,-0.5,240.5); hs.append(htracksM2R1C4)

hentChannelM2R1C12=TH2D("hentChannelM2R1C12","row vs col in M2R1C12",48,143.5,191.5,8,-0.5,7.5); hs.append(hentChannelM2R1C12)
hentWireM2R1C12=TH2D("hentWireM2R1C12","wire tracks in M2R1C12",48,143.5,191.5,1,-0.5,0.5); hs.append(hentWireM2R1C12)
hentCathodePadM2R1C12=TH2D("hentCathodePadM2R1C12","cathode pad tracks in M2R1C12",8,23.5,31.5,8,-0.5,7.5); hs.append(hentCathodePadM2R1C12)
htracksM2R1C12=TH1D("htracksM2R1C12","number of tracks per event in M2R1C12",241,-0.5,240.5); hs.append(htracksM2R1C12)

hentChannelM2R1C7=TH2D("hentChannelM2R1C7","row vs col in M2R1C7",48,-0.5,47.5,8,7.5,15.5); hs.append(hentChannelM2R1C7)
hentWireM2R1C7=TH2D("hentWireM2R1C7","wire tracks in M2R1C7",48,-0.5,47.5,1,0.5,1.5); hs.append(hentWireM2R1C7)
hentCathodePadM2R1C7=TH2D("hentCathodePadM2R1C7","cathode pad tracks in M2R1C7",8,-0.5,7.5,8,7.5,15.5); hs.append(hentCathodePadM2R1C7)
htracksM2R1C7=TH1D("htracksM2R1C7","number of tracks per event in M2R1C7",241,-0.5,240.5); hs.append(htracksM2R1C7)

hentChannelM2R1C8=TH2D("hentChannelM2R1C8","row vs col in M2R1C8",48,143.5,191.5,8,7.5,15.5); hs.append(hentChannelM2R1C8)
hentWireM2R1C8=TH2D("hentWireM2R1C8","wire tracks in M2R1C8",48,143.5,191.5,1,0.5,1.5); hs.append(hentWireM2R1C8)
hentCathodePadM2R1C8=TH2D("hentCathodePadM2R1C8","cathode pad tracks in M2R1C8",8,23.5,31.5,8,7.5,15.5); hs.append(hentCathodePadM2R1C8)
htracksM2R1C8=TH1D("htracksM2R1C8","number of tracks per event in M2R1C8",241,-0.5,240.5); hs.append(htracksM2R1C8)

hentChannelM2R1C5=TH2D("hentChannelM2R1C5","row vs col in M2R1C5",48,-0.5,47.5,8,15.5,23.5); hs.append(hentChannelM2R1C5)
hentWireM2R1C5=TH2D("hentWireM2R1C5","wire tracks in M2R1C5",48,-0.5,47.5,1,1.5,2.5); hs.append(hentWireM2R1C5)
hentCathodePadM2R1C5=TH2D("hentCathodePadM2R1C5","cathode pad tracks in M2R1C5",8,-0.5,7.5,8,15.5,23.5); hs.append(hentCathodePadM2R1C5)
hhitspertrackM2R1C5=TH1D("hhitspertrackM2R1C5","number of hits in M2R1C5 produced by one track",21,-0.5,20.5); hs.append(hhitspertrackM2R1C5)
hhitspertrackM2R1C5.SetFillColor(38)
htracksM2R1C5=TH1D("htracksM2R1C5","number of tracks per event in M2R1C5",241,-0.5,240.5); hs.append(htracksM2R1C5)

hentChannelM2R1C6=TH2D("hentChannelM2R1C6","row vs col in M2R1C6",48,143.5,191.5,8,15.5,23.5); hs.append(hentChannelM2R1C6)
hentWireM2R1C6=TH2D("hentWireM2R1C6","wire tracks in M2R1C6",48,143.5,191.5,1,1.5,2.5); hs.append(hentWireM2R1C6)
hentCathodePadM2R1C6=TH2D("hentCathodePadM2R1C6","cathode pad tracks in M2R1C6",8,23.5,31.5,8,15.5,23.5); hs.append(hentCathodePadM2R1C6)
hhitspertrackM2R1C6=TH1D("hhitspertrackM2R1C6","number of hits in M2R1C6 produced by one track",21,-0.5,20.5); hs.append(hhitspertrackM2R1C6)
hhitspertrackM2R1C6.SetFillColor(38)
htracksM2R1C6=TH1D("htracksM2R1C6","number of tracks per event in M2R1C6",241,-0.5,240.5); hs.append(htracksM2R1C6)

hentChannelM2R1C9=TH2D("hentChannelM2R1C9","row vs col in M2R1C9",48,-0.5,47.5,8,23.5,31.5); hs.append(hentChannelM2R1C9)
hentWireM2R1C9=TH2D("hentWireM2R1C9","wire tracks in M2R1C9",48,-0.5,47.5,1,2.5,3.5); hs.append(hentWireM2R1C9)
hentCathodePadM2R1C9=TH2D("hentCathodePadM2R1C9","cathode pad tracks in M2R1C9",8,-0.5,7.5,8,23.5,31.5); hs.append(hentCathodePadM2R1C9)
hhitspertrackM2R1C9=TH1D("hhitspertrackM2R1C9","number of hits in M2R1C9 produced by one track",21,-0.5,20.5); hs.append(hhitspertrackM2R1C9)
hhitspertrackM2R1C9.SetFillColor(38)
htracksM2R1C9=TH1D("htracksM2R1C9","number of tracks per event in M2R1C9",241,-0.5,240.5); hs.append(htracksM2R1C9)

hentChannelM2R1C1=TH2D("hentChannelM2R1C1","row vs col in M2R1C1",48,47.5,95.5,8,23.5,31.5); hs.append(hentChannelM2R1C1)
hentWireM2R1C1=TH2D("hentWireM2R1C1","wire tracks in M2R1C1",48,47.5,95.5,1,2.5,3.5); hs.append(hentWireM2R1C1)
hentCathodePadM2R1C1=TH2D("hentCathodePadM2R1C1","cathode pad tracks in M2R1C1",8,7.5,15.5,8,23.5,31.5); hs.append(hentCathodePadM2R1C1)
hhitspertrackM2R1C1=TH1D("hhitspertrackM2R1C1","number of hits in M2R1C1 produced by one track",21,-0.5,20.5); hs.append(hhitspertrackM2R1C1)
hhitspertrackM2R1C1.SetFillColor(38)
htracksM2R1C1=TH1D("htracksM2R1C1","number of tracks per event in M2R1C1",241,-0.5,240.5); hs.append(htracksM2R1C1)

hentChannelM2R1C2=TH2D("hentChannelM2R1C2","row vs col in M2R1C2",48,95.5,143.5,8,23.5,31.5); hs.append(hentChannelM2R1C2)
hentWireM2R1C2=TH2D("hentWireM2R1C2","wire tracks in M2R1C2",48,95.5,143.5,1,2.5,3.5); hs.append(hentWireM2R1C2)
hentCathodePadM2R1C2=TH2D("hentCathodePadM2R1C2","cathode pad tracks in M2R1C2",8,15.5,23.5,8,23.5,31.5); hs.append(hentCathodePadM2R1C2)
hhitspertrackM2R1C2=TH1D("hhitspertrackM2R1C2","number of hits in M2R1C2 produced by one track",21,-0.5,20.5); hs.append(hhitspertrackM2R1C2)
hhitspertrackM2R1C2.SetFillColor(38)
htracksM2R1C2=TH1D("htracksM2R1C2","number of tracks per event in M2R1C2",241,-0.5,240.5); hs.append(htracksM2R1C2)

hentChannelM2R1C10=TH2D("hentChannelM2R1C10","row vs col in M2R1C10",48,143.5,191.5,8,23.5,31.5); hs.append(hentChannelM2R1C10)
hentWireM2R1C10=TH2D("hentWireM2R1C10","wire tracks in M2R1C10",48,143.5,191.5,1,2.5,3.5); hs.append(hentWireM2R1C10)
hentCathodePadM2R1C10=TH2D("hentCathodePadM2R1C10","cathode pad tracks in M2R1C10",8,23.5,31.5,8,23.5,31.5); hs.append(hentCathodePadM2R1C10)
hhitspertrackM2R1C10=TH1D("hhitspertrackM2R1C10","number of hits in M2R1C10 produced by one track",21,-0.5,20.5); hs.append(hhitspertrackM2R1C10)
hhitspertrackM2R1C10.SetFillColor(38)
htracksM2R1C10=TH1D("htracksM2R1C10","number of tracks per event in M2R1C10",241,-0.5,240.5); hs.append(htracksM2R1C10)


# M2R2 histograms
hentxyM2R2=TH2D("horixyM2R2","xy of hits entries in M2R2",200,-1400.0,1400.0,200,-1200.0,1200.0); hs.append(hentxyM2R2)
hhitspertrackM2R2=TH1D("hhitspertrackM2R2","number of hits in M2R2 produced by one track",31,-0.5,30.5); hs.append(hhitspertrackM2R2)
hhitspertrackM2R2.SetFillColor(38)
hentChannelM2R2=TH2D("hentChannelM2R2","row vs col in M2R2",192,-0.5,191.5,32,-0.5,31.5); hs.append(hentChannelM2R2)
hentWireM2R2=TH2D("hentWireM2R2","wire tracks in M2R2",192,-0.5,191.5,4,-0.5,3.5); hs.append(hentWireM2R2)
hentCathodePadM2R2=TH2D("hentCathodePadM2R2","cathode pad tracks in M2R2",32,-0.5,31.5,32,-0.5,31.5); hs.append(hentCathodePadM2R2)
htracksM2R2=TH1D("htracksM2R2","number of tracks per event in M2R2",501,-0.5,500.5); hs.append(htracksM2R2)
htracksM2R2.SetFillColor(38)
hangleYZM2R2=TH2D("hangleYZM2R2","Number of gaps penetrated by one track depending on YZ angle (in M2R2)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleYZM2R2)
(hangleYZM2R2.GetXaxis()).SetTitle("YZ angle [rad]"); (hangleYZM2R2.GetXaxis()).SetTitleOffset(2.0); (hangleYZM2R2.GetXaxis()).CenterTitle(True)
(hangleYZM2R2.GetYaxis()).SetTitle("#gaps"); (hangleYZM2R2.GetYaxis()).SetTitleOffset(2.0); (hangleYZM2R2.GetYaxis()).CenterTitle(True)
hangleXZM2R2=TH2D("hangleXZM2R2","Number of gaps penetrated by one track depending on XZ angle (in M2R2)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleXZM2R2)
(hangleXZM2R2.GetXaxis()).SetTitle("XZ angle [rad]"); (hangleXZM2R2.GetXaxis()).SetTitleOffset(2.0); (hangleXZM2R2.GetXaxis()).CenterTitle(True)
(hangleXZM2R2.GetYaxis()).SetTitle("#gaps"); (hangleXZM2R2.GetYaxis()).SetTitleOffset(2.0); (hangleXZM2R2.GetYaxis()).CenterTitle(True)
hangleYXM2R2=TH2D("hangleYXM2R2","Number of gaps penetrated by one track depending on YX angle (in M2R2)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleYXM2R2)
(hangleYXM2R2.GetXaxis()).SetTitle("YX angle [rad]"); (hangleYXM2R2.GetXaxis()).SetTitleOffset(2.0); (hangleYXM2R2.GetXaxis()).CenterTitle(True)
(hangleYXM2R2.GetYaxis()).SetTitle("#gaps"); (hangleYXM2R2.GetYaxis()).SetTitleOffset(2.0); (hangleYXM2R2.GetYaxis()).CenterTitle(True)


# M3R1 histograms
hentxyM3R1=TH2D("horixyM3R1","xy of hits entries in M3R1",100,-700.0,700.0,100,-600.0,600.0); hs.append(hentxyM3R1)
hhitspertrackM3R1=TH1D("hhitspertrackM3R1","number of hits in M3R1 produced by one track",31,-0.5,30.5); hs.append(hhitspertrackM3R1)
hhitspertrackM3R1.SetFillColor(38)
hentChannelM3R1=TH2D("hentChannelM3R1","row vs col in M3R1",192,-0.5,191.5,32,-0.5,31.5); hs.append(hentChannelM3R1)
hentWireM3R1=TH2D("hentWireM3R1","wire tracks in M3R1",192,-0.5,191.5,4,-0.5,3.5); hs.append(hentWireM3R1)
hentCathodePadM3R1=TH2D("hentCathodePadM3R1","cathode pad tracks in M3R1",32,-0.5,31.5,32,-0.5,31.5); hs.append(hentCathodePadM3R1)
htracksM3R1=TH1D("htracksM3R1","number of tracks per event in M3R1",501,-0.5,500.5); hs.append(htracksM3R1)
htracksM3R1.SetFillColor(38)
hangleYZM3R1=TH2D("hangleYZM3R1","Number of gaps penetrated by one track depending on YZ angle (in M3R1)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleYZM3R1)
(hangleYZM3R1.GetXaxis()).SetTitle("YZ angle [rad]"); (hangleYZM3R1.GetXaxis()).SetTitleOffset(2.0); (hangleYZM3R1.GetXaxis()).CenterTitle(True)
(hangleYZM3R1.GetYaxis()).SetTitle("#gaps"); (hangleYZM3R1.GetYaxis()).SetTitleOffset(2.0); (hangleYZM3R1.GetYaxis()).CenterTitle(True)
hangleXZM3R1=TH2D("hangleXZM3R1","Number of gaps penetrated by one track depending on XZ angle (in M3R1)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleXZM3R1)
(hangleXZM3R1.GetXaxis()).SetTitle("XZ angle [rad]"); (hangleXZM3R1.GetXaxis()).SetTitleOffset(2.0); (hangleXZM3R1.GetXaxis()).CenterTitle(True)
(hangleXZM3R1.GetYaxis()).SetTitle("#gaps"); (hangleXZM3R1.GetYaxis()).SetTitleOffset(2.0); (hangleXZM3R1.GetYaxis()).CenterTitle(True)
hangleYXM3R1=TH2D("hangleYXM3R1","Number of gaps penetrated by one track depending on YX angle (in M3R1)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleYXM3R1)
(hangleYXM3R1.GetXaxis()).SetTitle("YX angle [rad]"); (hangleYXM3R1.GetXaxis()).SetTitleOffset(2.0); (hangleYXM3R1.GetXaxis()).CenterTitle(True)
(hangleYXM3R1.GetYaxis()).SetTitle("#gaps"); (hangleYXM3R1.GetYaxis()).SetTitleOffset(2.0); (hangleYXM3R1.GetYaxis()).CenterTitle(True)



# M3R2 histograms
hentxyM3R2=TH2D("horixyM3R2","xy of hits entries in M3R2",200,-1400.0,1400.0,200,-1200.0,1200.0); hs.append(hentxyM3R2)
hhitspertrackM3R2=TH1D("hhitspertrackM3R2","number of hits in M3R2 produced by one track",31,-0.5,30.5); hs.append(hhitspertrackM3R2)
hhitspertrackM3R2.SetFillColor(38)
hentChannelM3R2=TH2D("hentChannelM3R2","row vs col in M3R2",192,-0.5,191.5,32,-0.5,31.5); hs.append(hentChannelM3R2)
hentWireM3R2=TH2D("hentWireM3R2","wire tracks in M3R2",192,-0.5,191.5,4,-0.5,3.5); hs.append(hentWireM3R2)
hentCathodePadM3R2=TH2D("hentCathodePadM3R2","cathode pad tracks in M3R2",32,-0.5,31.5,32,-0.5,31.5); hs.append(hentCathodePadM3R2)
htracksM3R2=TH1D("htracksM3R2","number of tracks per event in M3R2",501,-0.5,500.5); hs.append(htracksM3R2)
htracksM3R2.SetFillColor(38)
hangleYZM3R2=TH2D("hangleYZM3R2","Number of gaps penetrated by one track depending on YZ angle (in M3R2)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleYZM3R2)
(hangleYZM3R2.GetXaxis()).SetTitle("YZ angle [rad]"); (hangleYZM3R2.GetXaxis()).SetTitleOffset(2.0); (hangleYZM3R2.GetXaxis()).CenterTitle(True)
(hangleYZM3R2.GetYaxis()).SetTitle("#gaps"); (hangleYZM3R2.GetYaxis()).SetTitleOffset(2.0); (hangleYZM3R2.GetYaxis()).CenterTitle(True)
hangleXZM3R2=TH2D("hangleXZM3R2","Number of gaps penetrated by one track depending on XZ angle (in M3R2)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleXZM3R2)
(hangleXZM3R2.GetXaxis()).SetTitle("XZ angle [rad]"); (hangleXZM3R2.GetXaxis()).SetTitleOffset(2.0); (hangleXZM3R2.GetXaxis()).CenterTitle(True)
(hangleXZM3R2.GetYaxis()).SetTitle("#gaps"); (hangleXZM3R2.GetYaxis()).SetTitleOffset(2.0); (hangleXZM3R2.GetYaxis()).CenterTitle(True)
hangleYXM3R2=TH2D("hangleYXM3R2","Number of gaps penetrated by one track depending on YX angle (in M3R2)",41,-2.05,2.05,5,0.5,5.5); hs.append(hangleYXM3R2)
(hangleYXM3R2.GetXaxis()).SetTitle("YX angle [rad]"); (hangleYXM3R2.GetXaxis()).SetTitleOffset(2.0); (hangleYXM3R2.GetXaxis()).CenterTitle(True)
(hangleYXM3R2.GetYaxis()).SetTitle("#gaps"); (hangleYXM3R2.GetYaxis()).SetTitleOffset(2.0); (hangleYXM3R2.GetYaxis()).CenterTitle(True)



### INITIATING SOME LISTS ###

M2R1hits_list = list()
M2R1particles_list = list()
M2R1tracks2hits_list = list()
M2R1tracks2angles_list = list()

M2R2hits_list = list()
M2R2particles_list = list()
M2R2tracks2hits_list = list()
M2R2tracks2angles_list = list()

M3R1hits_list = list()
M3R1particles_list = list()
M3R1tracks2hits_list = list()
M3R1tracks2angles_list = list()

M3R2hits_list = list()
M3R2particles_list = list()
M3R2tracks2hits_list = list()
M3R2tracks2angles_list = list()

for i in range(12):
    M2R1hits_list.append(0) 
    M3R1hits_list.append(0)
    M2R1tracks2hits_list.append(dict())
    M3R1tracks2hits_list.append(dict())
    M2R1tracks2angles_list.append(dict())
    M3R1tracks2angles_list.append(dict())
    
for i in range(24):
    M2R2hits_list.append(0)
    M3R2hits_list.append(0)
    M2R2tracks2hits_list.append(dict())
    M3R2tracks2hits_list.append(dict())
    M2R2tracks2angles_list.append(dict())
    M3R2tracks2angles_list.append(dict())
    

### MAIN LOOP ###
    
ntotevts=1
while ntotevts<=run_nevents :

    ntotevts=ntotevts+1
    appMgr.run(1)
    if not evt['MC/Particles']: break

    hepmcevents = evt['Gen/HepMCEvents']
    #hnvtx.Fill(hepmcevents.size())

    for i in range(12):
        M2R1particles_list.append(list())
        M3R1particles_list.append(list())

    for i in range(24):
        M2R2particles_list.append(list())
        M3R2particles_list.append(list())

    ntracksM2R1=0
    ntracksM2R2=0
    ntracksM3R1=0
    ntracksM3R2=0

    ntracksM2R1C1=0
    ntracksM2R1C2=0
    ntracksM2R1C3=0
    ntracksM2R1C4=0
    ntracksM2R1C5=0
    ntracksM2R1C6=0
    ntracksM2R1C7=0
    ntracksM2R1C8=0
    ntracksM2R1C9=0
    ntracksM2R1C10=0
    ntracksM2R1C11=0
    ntracksM2R1C12=0
    
    mcmuonhits=evt['MC/Muon/Hits']
    for hit in mcmuonhits:

        id=hit.sensDetID()
        chamber_id=id&0x1FFFFC
        q=id>>15
        m=(id>>12)&7
        r=(id>>10)&3
        chm=(id>>2)&255
        gap=id&3
        
        entx=hit.entry().X()
        enty=hit.entry().Y()
        entz=hit.entry().Z()
        entr=(entx**2+enty**2)**0.5
        entt=hit.time()
        
        dispx=hit.displacement().X()
        dispy=hit.displacement().Y()
        dispz=hit.displacement().Z()
        
        L=(entx**2+enty**2+entz**2)**0.5
        tprime=entt-L/speed_of_light
        TAE=int(round(tprime/25.0))
        M1=(entz>=11900.0 and entz<=12300.0)
        M2=(entz>=15000.0 and entz<=15500.0)
        M3=(entz>=16200.0 and entz<=16700.0)
        M4=(entz>=17400.0 and entz<=17900.0)
        M5=(entz>=18600.0 and entz<=19100.0)
        bk=(dispz<0.0)

        p=hit.mcParticle()
        pid=p.particleID().pid()
        key=p.key()
        momentum=p.momentum()

        angleYZ = TMath.ATan(momentum.Py()/(momentum.Pz()+0.000001))
        angleXZ = TMath.ATan(momentum.Px()/(momentum.Pz()+0.000001))
        angleYX = TMath.ATan(momentum.Py()/(momentum.Px()+0.000001))
        trackangles = (angleYZ, angleXZ, angleYX)

        hkeymulti.Fill(key)

        orix=p.originVertex().position().x()
        oriy=p.originVertex().position().y()
        oriz=p.originVertex().position().z()
                
        if hitdebug and M2 and r==0:
            print "%5d"%pid,
            print "%6x"%id,
            print "q%1dm%1dr%1dch%3dg%1d"%(q,m,r,chm,gap),
            print "%5.0f"%entx,
            print "%5.0f"%enty,
            print "%5.0f"%entz,
            print "%8.3f"%dispx,
            print "%8.3f"%dispy,
            print "%8.3f"%dispz,
            print "%5.3f"%hit.energy(),
            print "%4.1f"%entt,
            print "%5.0f"%hit.p(),
            print "%5.0f"%oriz,
            print "%3d"%p.originVertex().type(),
            print key,
            print
                    
        hentz.Fill(entz)
        horiz.Fill(oriz)       
        horixy.Fill(orix,oriy)
        horizx.Fill(oriz,orix)
        horizy.Fill(oriz,oriy)         
        hentt.Fill(entt)
        htprime.Fill(tprime)
        hdispz.Fill(dispz)

        if not key in mu.tracks2hits:
            mu.tracks2hits[key]=1
        else:
            mu.tracks2hits[key]+=1
                
        if M2:
            
            hentxyM2.Fill(entx,enty)
	    hentxyM2R1R2small2.Fill(entx,enty)
	    hentxyM2R1R2medium2.Fill(entx,enty)
	    hentxyM2R1R2big2.Fill(entx,enty)
	    hentxyM2R1R2huge2.Fill(entx,enty)
            
            if r==0:

                M2R1hits_list[chm]+=1
                
                if chm==6:
                    if not key in mu.M2R1C5tracks2hits:
                        mu.M2R1C5tracks2hits[key]=1
                    else:
                        mu.M2R1C5tracks2hits[key]+=1
                        
                elif chm==7:
                    if not key in mu.M2R1C6tracks2hits:
                        mu.M2R1C6tracks2hits[key]=1
                    else:
                        mu.M2R1C6tracks2hits[key]+=1
                        
                elif chm==8:
                    if not key in mu.M2R1C9tracks2hits:
                        mu.M2R1C9tracks2hits[key]=1
                    else:
                        mu.M2R1C9tracks2hits[key]+=1
                elif chm==9:
                    if not key in mu.M2R1C1tracks2hits:
                        mu.M2R1C1tracks2hits[key]=1
                    else:
                        mu.M2R1C1tracks2hits[key]+=1
                        
                elif chm==10:
                    if not key in mu.M2R1C2tracks2hits:
                        mu.M2R1C2tracks2hits[key]=1
                    else:
                        mu.M2R1C2tracks2hits[key]+=1

                elif chm==11:
                    if not key in mu.M2R1C10tracks2hits:
                        mu.M2R1C10tracks2hits[key]=1
                    else:
                        mu.M2R1C10tracks2hits[key]+=1
                    
                if not key in M2R1particles_list[chm]:

                    M2R1particles_list[chm].append(key)
                    M2R1tracks2hits_list[chm][key]=1
                    #if momentum.Pz()>=100.0:
                    M2R1tracks2angles_list[chm][key]=trackangles
                
                    hentxyM2R1.Fill(entx,enty)
		    hentxyM2R1R2small.Fill(entx,enty)
		    hentxyM2R1R2medium.Fill(entx,enty)
		    hentxyM2R1R2big.Fill(entx,enty)
		    hentxyM2R1R2huge.Fill(entx,enty)
                    (row,col)=mu.M2R1globxy2globrc(entx,enty,chm)
                    hentChannelM2R1.Fill(col,row)
                    hentWireM2R1.Fill(col,int(row/8))
                    hentCathodePadM2R1.Fill(int(col/6),row)
                    ntracksM2R1+=1

                    if chm==0:
                        hentChannelM2R1C11.Fill(col,row)
                        hentWireM2R1C11.Fill(col,int(row/8))
                        hentCathodePadM2R1C11.Fill(int(col/6),row)
			ntracksM2R1C11+=1
                        
                    elif chm==1:
                        hentChannelM2R1C3.Fill(col,row)
                        hentWireM2R1C3.Fill(col,int(row/8))
                        hentCathodePadM2R1C3.Fill(int(col/6),row)
			ntracksM2R1C3+=1

                    elif chm==2:
                        hentChannelM2R1C4.Fill(col,row)
                        hentWireM2R1C4.Fill(col,int(row/8))
                        hentCathodePadM2R1C4.Fill(int(col/6),row)
			ntracksM2R1C4+=1

                    elif chm==3:
                        hentChannelM2R1C12.Fill(col,row)
                        hentWireM2R1C12.Fill(col,int(row/8))
                        hentCathodePadM2R1C12.Fill(int(col/6),row)
			ntracksM2R1C12+=1

                    elif chm==4:
                        hentChannelM2R1C7.Fill(col,row)
                        hentWireM2R1C7.Fill(col,int(row/8))
                        hentCathodePadM2R1C7.Fill(int(col/6),row)
			ntracksM2R1C7+=1

                    elif chm==5:
                        hentChannelM2R1C8.Fill(col,row)
                        hentWireM2R1C8.Fill(col,int(row/8))
                        hentCathodePadM2R1C8.Fill(int(col/6),row)
			ntracksM2R1C8+=1

                    elif chm==6:
                        hentChannelM2R1C5.Fill(col,row)
                        hentWireM2R1C5.Fill(col,int(row/8))
                        hentCathodePadM2R1C5.Fill(int(col/6),row)
			ntracksM2R1C5+=1
  
                    elif chm==7:
                        hentChannelM2R1C6.Fill(col,row)
                        hentWireM2R1C6.Fill(col,int(row/8))
                        hentCathodePadM2R1C6.Fill(int(col/6),row)
			ntracksM2R1C6+=1

                    elif chm==8:
                        hentChannelM2R1C9.Fill(col,row)
                        hentWireM2R1C9.Fill(col,int(row/8))
                        hentCathodePadM2R1C9.Fill(int(col/6),row)
			ntracksM2R1C9+=1

                    elif chm==9:
                        hentChannelM2R1C1.Fill(col,row)
                        hentWireM2R1C1.Fill(col,int(row/8))
                        hentCathodePadM2R1C1.Fill(int(col/6),row)
			ntracksM2R1C1+=1

                    elif chm==10:
                        hentChannelM2R1C2.Fill(col,row)
                        hentWireM2R1C2.Fill(col,int(row/8))
                        hentCathodePadM2R1C2.Fill(int(col/6),row)
			ntracksM2R1C2+=1

                    else:
                        hentChannelM2R1C10.Fill(col,row)
                        hentWireM2R1C10.Fill(col,int(row/8))
                        hentCathodePadM2R1C10.Fill(int(col/6),row)
			ntracksM2R1C10+=1

                else:
		    try:
                        M2R1tracks2hits_list[chm][key]+=1
		    except KeyError as e:
		        print e
                    
            elif r==1:

                M2R2hits_list[chm]+=1
                
                if not key in M2R2particles_list[chm]:

                    M2R2particles_list[chm].append(key)
                    M2R2tracks2hits_list[chm][key]=1
                    #if momentum.Pz()>=100.0:
                    M2R2tracks2angles_list[chm][key]=trackangles

                    hentxyM2R2.Fill(entx,enty)
 		    hentxyM2R1R2small.Fill(entx,enty)
		    hentxyM2R1R2medium.Fill(entx,enty)
		    hentxyM2R1R2big.Fill(entx,enty)
		    hentxyM2R1R2huge.Fill(entx,enty)
                    (row,col)=mu.M2R2globxy2globrc(entx,enty,chm)
                    hentChannelM2R2.Fill(col,row)
                    hentWireM2R2.Fill(col,int(row/8))
                    hentCathodePadM2R2.Fill(int(col/6),row)
                    ntracksM2R2+=1
                    
                else:
		    try:
                      M2R2tracks2hits_list[chm][key]+=1
                    except KeyError as e:
		      print e
        elif M3:

            hentxyM3.Fill(entx,enty)

            if r==0:

                M3R1hits_list[chm]+=1
               
                if not key in M3R1particles_list[chm]:

                    M3R1particles_list[chm].append(key)
                    M3R1tracks2hits_list[chm][key]=1
                    #if momentum.Pz()>=100.0:
                    M3R1tracks2angles_list[chm][key]=trackangles

                    hentxyM3R1.Fill(entx,enty)
                    (row,col)=mu.M3R1globxy2globrc(entx,enty,chm)
                    hentChannelM3R1.Fill(col,row)
                    hentWireM3R1.Fill(col,int(row/8))
                    hentCathodePadM3R1.Fill(int(col/6),row)
                    ntracksM3R1+=1
                    
                else:
		    try:
                      M3R1tracks2hits_list[chm][key]+=1
		    except KeyError as e:
		      print e
                    
            elif r==1:

                M3R2hits_list[chm]+=1
               
                if not key in M3R2particles_list[chm]:

                    M3R2particles_list[chm].append(key)
                    M3R2tracks2hits_list[chm][key]=1
                    #if momentum.Pz()>=100.0:
                    M3R2tracks2angles_list[chm][key]=trackangles

                    hentxyM3R2.Fill(entx,enty)
                    (row,col)=mu.M3R2globxy2globrc(entx,enty,chm)
                    hentChannelM3R2.Fill(col,row)
                    hentWireM3R2.Fill(col,int(row/8))
                    hentCathodePadM3R2.Fill(int(col/6),row)
                    ntracksM3R2+=1
                    
                else:
                    M3R2tracks2hits_list[chm][key]+=1

    del M2R1particles_list[0:11]
    del M2R2particles_list[0:23]
    del M3R1particles_list[0:11]
    del M3R2particles_list[0:23]
    htracksM2R1.Fill(ntracksM2R1)
    htracksM2R2.Fill(ntracksM2R2)
    htracksM3R1.Fill(ntracksM3R1)
    htracksM3R2.Fill(ntracksM3R2)

    htracksM2R1C1.Fill(ntracksM2R1C1)
    htracksM2R1C2.Fill(ntracksM2R1C2)
    htracksM2R1C3.Fill(ntracksM2R1C3)
    htracksM2R1C4.Fill(ntracksM2R1C4)
    htracksM2R1C5.Fill(ntracksM2R1C5)
    htracksM2R1C6.Fill(ntracksM2R1C6)
    htracksM2R1C7.Fill(ntracksM2R1C7)
    htracksM2R1C8.Fill(ntracksM2R1C8)
    htracksM2R1C9.Fill(ntracksM2R1C9)
    htracksM2R1C10.Fill(ntracksM2R1C10)
    htracksM2R1C11.Fill(ntracksM2R1C11)
    htracksM2R1C12.Fill(ntracksM2R1C12)

    for t in mu.tracks2hits:
        hhitspertrack.Fill(mu.tracks2hits[t])

    for t in mu.M2R1C1tracks2hits:
        hhitspertrackM2R1C1.Fill(mu.M2R1C1tracks2hits[t])

    for t in mu.M2R1C2tracks2hits:
        hhitspertrackM2R1C2.Fill(mu.M2R1C2tracks2hits[t])
        
    for t in mu.M2R1C5tracks2hits:
        hhitspertrackM2R1C5.Fill(mu.M2R1C5tracks2hits[t])

    for t in mu.M2R1C6tracks2hits:
        hhitspertrackM2R1C6.Fill(mu.M2R1C6tracks2hits[t])

    for t in mu.M2R1C9tracks2hits:
        hhitspertrackM2R1C9.Fill(mu.M2R1C9tracks2hits[t])

    for t in mu.M2R1C10tracks2hits:
        hhitspertrackM2R1C10.Fill(mu.M2R1C10tracks2hits[t])

    for c in range(12):
        for t in M2R1tracks2hits_list[c]:
            hhitspertrackM2R1.Fill(M2R1tracks2hits_list[c][t])
        for t in M3R1tracks2hits_list[c]:
            hhitspertrackM3R1.Fill(M3R1tracks2hits_list[c][t])

    for c in range(24):
        for t in M2R2tracks2hits_list[c]:
            hhitspertrackM2R2.Fill(M2R2tracks2hits_list[c][t])
        for t in M3R2tracks2hits_list[c]:
            hhitspertrackM3R2.Fill(M3R2tracks2hits_list[c][t])

    for c in range(12):
        for t in M2R1tracks2angles_list[c]:
            hangleYZM2R1.Fill(M2R1tracks2angles_list[c][t][0],M2R1tracks2hits_list[c][t])
            hangleXZM2R1.Fill(M2R1tracks2angles_list[c][t][1],M2R1tracks2hits_list[c][t])
            hangleYXM2R1.Fill(M2R1tracks2angles_list[c][t][2],M2R1tracks2hits_list[c][t])
        for t in M3R1tracks2angles_list[c]:
            hangleYZM3R1.Fill(M3R1tracks2angles_list[c][t][0],M3R1tracks2hits_list[c][t])
            hangleXZM3R1.Fill(M3R1tracks2angles_list[c][t][1],M3R1tracks2hits_list[c][t])
            hangleYXM3R1.Fill(M3R1tracks2angles_list[c][t][2],M3R1tracks2hits_list[c][t])

    for c in range(24):
        for t in M2R2tracks2angles_list[c]:
            hangleYZM2R2.Fill(M2R2tracks2angles_list[c][t][0],M2R2tracks2hits_list[c][t])
            hangleXZM2R2.Fill(M2R2tracks2angles_list[c][t][1],M2R2tracks2hits_list[c][t])
            hangleYXM2R2.Fill(M2R2tracks2angles_list[c][t][2],M2R2tracks2hits_list[c][t])
        for t in M3R2tracks2angles_list[c]:
            hangleYZM3R2.Fill(M3R2tracks2angles_list[c][t][0],M3R2tracks2hits_list[c][t])
            hangleXZM3R2.Fill(M3R2tracks2angles_list[c][t][1],M3R2tracks2hits_list[c][t])
            hangleYXM3R2.Fill(M3R2tracks2angles_list[c][t][2],M3R2tracks2hits_list[c][t])

    mu.tracks2hits.clear()
    mu.M2R1C1tracks2hits.clear()
    mu.M2R1C2tracks2hits.clear()
    mu.M2R1C5tracks2hits.clear()
    mu.M2R1C6tracks2hits.clear()
    mu.M2R1C9tracks2hits.clear()
    mu.M2R1C10tracks2hits.clear()
    for c in range(12):
        M2R1tracks2hits_list[c].clear()
        M3R1tracks2hits_list[c].clear()
        M2R1tracks2angles_list[c].clear()
        M3R1tracks2angles_list[c].clear()
    for c in range(24):
        M2R2tracks2hits_list[c].clear()
        M3R2tracks2hits_list[c].clear()
        M2R2tracks2angles_list[c].clear()
        M3R2tracks2angles_list[c].clear()

    #outputfileM2R1C9.write("\n\n")
    #outputfileM2R1C10.write("\n\n")
    
### END OF MAIN LOOP ###
    
MaximumContent = hentChannelM2R1.GetBinContent(hentChannelM2R1.GetMaximumBin())
print('MaximumContent', MaximumContent)

hhitsperchannelM2R1=TH1D("hhitsperchannelM2R1","number of channels vs hits per channel in M2R1",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1)
hhitsperchannelM2R1C1=TH1D("hhitsperchannelM2R1C1","number of channels vs hits per channel in M2R1C1",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C1)
hhitsperchannelM2R1C2=TH1D("hhitsperchannelM2R1C2","number of channels vs hits per channel in M2R1C2",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C2)
hhitsperchannelM2R1C3=TH1D("hhitsperchannelM2R1C3","number of channels vs hits per channel in M2R1C3",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C3)
hhitsperchannelM2R1C4=TH1D("hhitsperchannelM2R1C4","number of channels vs hits per channel in M2R1C4",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C4)
hhitsperchannelM2R1C5=TH1D("hhitsperchannelM2R1C5","number of channels vs hits per channel in M2R1C5",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C5)
hhitsperchannelM2R1C6=TH1D("hhitsperchannelM2R1C6","number of channels vs hits per channel in M2R1C6",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C6)
hhitsperchannelM2R1C7=TH1D("hhitsperchannelM2R1C7","number of channels vs hits per channel in M2R1C7",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C7)
hhitsperchannelM2R1C8=TH1D("hhitsperchannelM2R1C8","number of channels vs hits per channel in M2R1C8",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C8)
hhitsperchannelM2R1C9=TH1D("hhitsperchannelM2R1C9","number of channels vs hits per channel in M2R1C9",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C9)
hhitsperchannelM2R1C10=TH1D("hhitsperchannelM2R1C10","number of channels vs hits per channel in M2R1C10",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C10)
hhitsperchannelM2R1C11=TH1D("hhitsperchannelM2R1C11","number of channels vs hits per channel in M2R1C11",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C11)
hhitsperchannelM2R1C12=TH1D("hhitsperchannelM2R1C12","number of channels vs hits per channel in M2R1C12",int(MaximumContent)+1,-0.5,MaximumContent+0.5); hs.append(hhitsperchannelM2R1C12)

for iy in range(1,9):
    for ix in range(1,49):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C11.Fill(numberofhits)

for iy in range(1,9):
    for ix in range(49,97):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C3.Fill(numberofhits)

for iy in range(1,9):
    for ix in range(97,145):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C4.Fill(numberofhits)

for iy in range(1,9):
    for ix in range(145,193):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C12.Fill(numberofhits)

for iy in range(9,17):
    for ix in range(1,49):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C7.Fill(numberofhits)

for iy in range(9,17):
    for ix in range(145,193):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C8.Fill(numberofhits)

for iy in range(17,25):
    for ix in range(1,49):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C5.Fill(numberofhits)

for iy in range(17,25):
    for ix in range(145,193):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C6.Fill(numberofhits)

for iy in range(25,33):
    for ix in range(1,49):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C9.Fill(numberofhits)

for iy in range(25,33):
    for ix in range(49,97):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C1.Fill(numberofhits)

for iy in range(25,33):
    for ix in range(97,145):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C2.Fill(numberofhits)

for iy in range(25,33):
    for ix in range(145,193):
        numberofhits = hentChannelM2R1.GetBinContent(ix,iy)
        hhitsperchannelM2R1.Fill(numberofhits)
        hhitsperchannelM2R1C10.Fill(numberofhits)

        
M2R1totalhits=0
M2R2totalhits=0
M3R1totalhits=0
M3R2totalhits=0

for i in range(12):
    M2R1totalhits+=M2R1hits_list[i]
    M3R1totalhits+=M3R1hits_list[i]

for i in range(24):
    M2R2totalhits+=M2R2hits_list[i]
    M3R2totalhits+=M3R2hits_list[i]

### Write the results to outputfile ###

try:
    outputfile = open("results/"+jname+"_m2m3.txt","w")
except IOError as e:
    raise e

outputfile.write("Parameters of the simulations\nBeam energy:\t7 TeV\nLuminosity:\t3.13*10^32 cm^-2 s^-1\nBunch spacing:\t50 ns\nEvents:\t\t%d\n\n" %run_nevents)

outputfile.write("Hits multiplicities in region\n")
outputfile.write("M2R1: %10d  +-  %6d\n" %(M2R1totalhits,sqrt(M2R1totalhits)))
outputfile.write("M2R2: %10d  +-  %6d\n" %(M2R2totalhits,sqrt(M2R2totalhits)))
outputfile.write("M3R1: %10d  +-  %6d\n" %(M3R1totalhits,sqrt(M3R1totalhits)))
outputfile.write("M3R2: %10d  +-  %6d\n\n" %(M3R2totalhits,sqrt(M3R2totalhits)))

outputfile.write("Average hits multiplicities in region per cm^-2 * s^-1\n")
outputfile.write("M2R1: %10.1f  +-  %6.1f\n" %(M2R1totalhits/(M2R1area*tvt),mu.RegionsTracksError(M2R1totalhits,M2R1area)))
outputfile.write("M2R2: %10.1f  +-  %6.1f\n" %(M2R2totalhits/(M2R2area*tvt),mu.RegionsTracksError(M2R2totalhits,M2R2area)))
outputfile.write("M3R1: %10.1f  +-  %6.1f\n" %(M3R1totalhits/(M3R1area*tvt),mu.RegionsTracksError(M3R1totalhits,M3R1area)))
outputfile.write("M3R2: %10.1f  +-  %6.1f\n\n" %(M3R2totalhits/(M3R2area*tvt),mu.RegionsTracksError(M3R2totalhits,M3R2area)))

M2R1entries = hentChannelM2R1.GetEntries()
M2R2entries = hentChannelM2R2.GetEntries()
M3R1entries = hentChannelM3R1.GetEntries()
M3R2entries = hentChannelM3R2.GetEntries()

outputfile.write("Tracks multiplicities in region\n")
outputfile.write("M2R1: %10d  +-  %6d\n" %(M2R1entries,sqrt(M2R1entries)))
outputfile.write("M2R2: %10d  +-  %6d\n" %(M2R2entries,sqrt(M2R2entries)))
outputfile.write("M3R1: %10d  +-  %6d\n" %(M3R1entries,sqrt(M3R1entries)))
outputfile.write("M3R2: %10d  +-  %6d\n\n" %(M3R2entries,sqrt(M3R2entries)))

outputfile.write("Average tracks multiplicities in region per cm^-2 * s^-1\n")
outputfile.write("M2R1: %10.1f  +-  %6.1f\n" %(M2R1entries/(M2R1area*tvt),mu.RegionsTracksError(M2R1entries,M2R1area)))
outputfile.write("M2R2: %10.1f  +-  %6.1f\n" %(M2R2entries/(M2R2area*tvt),mu.RegionsTracksError(M2R2entries,M2R2area)))
outputfile.write("M3R1: %10.1f  +-  %6.1f\n" %(M3R1entries/(M3R1area*tvt),mu.RegionsTracksError(M3R1entries,M3R1area)))
outputfile.write("M3R2: %10.1f  +-  %6.1f\n\n" %(M3R2entries/(M3R2area*tvt),mu.RegionsTracksError(M3R2entries,M3R2area)))

outputfile.write("Ratio hits per track\n")
"""
outputfile.write("M2R1: %10.2f  +-  %6.2f\n" %(M2R1totalhits/M2R1entries,mu.RatioError(M2R1totalhits,M2R1entries)))
"""
outputfile.write("M2R2: %10.2f  +-  %6.2f\n" %(M2R2totalhits/M2R2entries,mu.RatioError(M2R2totalhits,M2R2entries)))

outputfile.write("M3R1: %10.2f  +-  %6.2f\n" %(M3R1totalhits/M3R1entries,mu.RatioError(M3R1totalhits,M3R1entries)))

outputfile.write("M3R2: %10.2f  +-  %6.2f\n\n" %(M3R2totalhits/M3R2entries,mu.RatioError(M3R2totalhits,M3R2entries)))

outputfile.write("Average tracks multiplicities in wire per s^-1\n")
outputfile.write("M2R1: %10.1f  +-  %6.1f\n" %(M2R1entries/(WiresPerRegion*tvt),mu.WiresTracksError(M2R1entries)))
outputfile.write("M2R2: %10.1f  +-  %6.1f\n" %(M2R2entries/(WiresPerRegion*tvt),mu.WiresTracksError(M2R2entries)))
outputfile.write("M3R1: %10.1f  +-  %6.1f\n" %(M3R1entries/(WiresPerRegion*tvt),mu.WiresTracksError(M3R1entries)))
outputfile.write("M3R2: %10.1f  +-  %6.1f\n\n" %(M3R2entries/(WiresPerRegion*tvt),mu.WiresTracksError(M3R2entries)))

outputfile.write("Average tracks multiplicities in cathode pad per s^-1\n")
outputfile.write("M2R1: %10.1f  +-  %6.1f\n" %(M2R1entries/(CatPadsPerRegion*tvt),mu.CatPadsTracksError(M2R1entries)))
outputfile.write("M2R2: %10.1f  +-  %6.1f\n" %(M2R2entries/(CatPadsPerRegion*tvt),mu.CatPadsTracksError(M2R2entries)))
outputfile.write("M3R1: %10.1f  +-  %6.1f\n" %(M3R1entries/(CatPadsPerRegion*tvt),mu.CatPadsTracksError(M3R1entries)))
outputfile.write("M3R2: %10.1f  +-  %6.1f\n\n" %(M3R2entries/(CatPadsPerRegion*tvt),mu.CatPadsTracksError(M3R2entries)))

TracksErrorC1 = mu.RegionsTracksError(hentChannelM2R1C1.GetEntries(),M2R1chmarea)
TracksErrorC2 = mu.RegionsTracksError(hentChannelM2R1C2.GetEntries(),M2R1chmarea)
TracksErrorC3 = mu.RegionsTracksError(hentChannelM2R1C3.GetEntries(),M2R1chmarea)
TracksErrorC4 = mu.RegionsTracksError(hentChannelM2R1C4.GetEntries(),M2R1chmarea)
TracksErrorC5 = mu.RegionsTracksError(hentChannelM2R1C5.GetEntries(),M2R1chmarea)
TracksErrorC6 = mu.RegionsTracksError(hentChannelM2R1C6.GetEntries(),M2R1chmarea)
TracksErrorC7 = mu.RegionsTracksError(hentChannelM2R1C7.GetEntries(),M2R1chmarea)
TracksErrorC8 = mu.RegionsTracksError(hentChannelM2R1C8.GetEntries(),M2R1chmarea)
TracksErrorC9 = mu.RegionsTracksError(hentChannelM2R1C9.GetEntries(),M2R1chmarea)
TracksErrorC10 = mu.RegionsTracksError(hentChannelM2R1C10.GetEntries(),M2R1chmarea)
TracksErrorC11 = mu.RegionsTracksError(hentChannelM2R1C11.GetEntries(),M2R1chmarea)
TracksErrorC12 = mu.RegionsTracksError(hentChannelM2R1C12.GetEntries(),M2R1chmarea)

outputfile.write("Average tracks multiplicities in each chamber of M2R1 per cm^-2 * s^-1\n")
outputfile.write("C1:  %11.1f  +-  %6.1f\n" %(hentChannelM2R1C1.GetEntries()/(M2R1chmarea*tvt),TracksErrorC1))
outputfile.write("C2:  %11.1f  +-  %6.1f\n" %(hentChannelM2R1C2.GetEntries()/(M2R1chmarea*tvt),TracksErrorC2))
outputfile.write("C3:  %11.1f  +-  %6.1f\n" %(hentChannelM2R1C3.GetEntries()/(M2R1chmarea*tvt),TracksErrorC3))
outputfile.write("C4:  %11.1f  +-  %6.1f\n" %(hentChannelM2R1C4.GetEntries()/(M2R1chmarea*tvt),TracksErrorC4))
outputfile.write("C5:  %11.1f  +-  %6.1f\n" %(hentChannelM2R1C5.GetEntries()/(M2R1chmarea*tvt),TracksErrorC5))
outputfile.write("C6:  %11.1f  +-  %6.1f\n" %(hentChannelM2R1C6.GetEntries()/(M2R1chmarea*tvt),TracksErrorC6))
outputfile.write("C7:  %11.1f  +-  %6.1f\n" %(hentChannelM2R1C7.GetEntries()/(M2R1chmarea*tvt),TracksErrorC7))
outputfile.write("C8:  %11.1f  +-  %6.1f\n" %(hentChannelM2R1C8.GetEntries()/(M2R1chmarea*tvt),TracksErrorC8))
outputfile.write("C9:  %11.1f  +-  %6.1f\n" %(hentChannelM2R1C9.GetEntries()/(M2R1chmarea*tvt),TracksErrorC9))
outputfile.write("C10: %11.1f  +-  %6.1f\n" %(hentChannelM2R1C10.GetEntries()/(M2R1chmarea*tvt),TracksErrorC10))
outputfile.write("C11: %11.1f  +-  %6.1f\n" %(hentChannelM2R1C11.GetEntries()/(M2R1chmarea*tvt),TracksErrorC11))
outputfile.write("C12: %11.1f  +-  %6.1f\n\n" %(hentChannelM2R1C12.GetEntries()/(M2R1chmarea*tvt),TracksErrorC12))

averCgr1tracks = (hentChannelM2R1C1.GetEntries()+hentChannelM2R1C2.GetEntries()+hentChannelM2R1C3.GetEntries()+hentChannelM2R1C4.GetEntries())/4
averCgr2tracks = (hentChannelM2R1C5.GetEntries()+hentChannelM2R1C6.GetEntries()+hentChannelM2R1C7.GetEntries()+hentChannelM2R1C8.GetEntries())/4
averCgr3tracks = (hentChannelM2R1C9.GetEntries()+hentChannelM2R1C10.GetEntries()+hentChannelM2R1C11.GetEntries()+hentChannelM2R1C12.GetEntries())/4

TracksErrorCgr1 = sqrt(TMath.Power(TracksErrorC1/4,2) + TMath.Power(TracksErrorC2/4,2) + TMath.Power(TracksErrorC3/4,2) + TMath.Power(TracksErrorC4/4,2))
TracksErrorCgr2 = sqrt(TMath.Power(TracksErrorC5/4,2) + TMath.Power(TracksErrorC6/4,2) + TMath.Power(TracksErrorC7/4,2) + TMath.Power(TracksErrorC8/4,2))
TracksErrorCgr3 = sqrt(TMath.Power(TracksErrorC9/4,2) + TMath.Power(TracksErrorC10/4,2) + TMath.Power(TracksErrorC11/4,2) + TMath.Power(TracksErrorC12/4,2))

outputfile.write("Average tracks multiplicities in each group of chambers of M2R1 per cm^-2 * s^-1\n")
outputfile.write("Cgr1: %10.1f  +-  %6.1f\n" %(averCgr1tracks/(M2R1chmarea*tvt),TracksErrorCgr1))
outputfile.write("Cgr2: %10.1f  +-  %6.1f\n" %(averCgr2tracks/(M2R1chmarea*tvt),TracksErrorCgr2))
outputfile.write("Cgr3: %10.1f  +-  %6.1f\n\n" %(averCgr3tracks/(M2R1chmarea*tvt),TracksErrorCgr3))
try:
    outputfile.close()
except IOError:
    pass

### Write the histograms to a rootfile ###
f=TFile("results/{}_m2m3.root".format(jname),"recreate")
for h in hs:
    h.Write()
f.Close()

