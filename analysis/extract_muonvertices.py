import sys
from ROOT import TH1D,TH2D
from ROOT import MakeNullPointer,Math, TMath

from Configurables import LHCbApp
from Configurables import EventClockSvc

from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import *
import GaudiPython
from GaudiPython import gbl
from GaudiConf import IOHelper

import leb

import pickle
import gaudimodule
#from params import *
#from stations import MuStation

#mu = MuStation(WiresPerRegion, CatPadsPerRegion, tvt, dt)

n_evts = 48
#IOHelper('ROOT').inputFiles(['{}/{}.sim'.format(simpath, jname)]),
input_files = sys.argv[1:]
if n_evts < 0:
    n_evts = sum([[int(s) for s in f.split('_') if s.isdigit()][0] for f in input_files])
     
print "TOTAL EVENTS TO PROCESS: ", n_evts
IOHelper('ROOT').inputFiles(sys.argv[1:])
#IOHelper('ROOT').inputFiles(['{}/{}'.format(simpath, f) for f in data]),
print IOHelper('ROOT').inputFiles

### MORE IMPORTS ###

#importOptions('$APPCONFIGOPTS/DisableLFC.py')
#EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"
lhcbApp  = LHCbApp()
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'res')
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
import PartProp.Service, PartProp.decorators
DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle'
DataOnDemandSvc().AlgMap['/Event/MC/Vertices']  = 'UnpackMCVertex'
DataOnDemandSvc().AlgMap['/Event/MC/Muon/Hits']  = 'DataPacking::Unpack<LHCb::MCMuonHitPacker>/UnpackMCMuonHits'
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
lhcbApp.DataType = "Upgrade"
from Configurables import CondDB
CondDB().Upgrade = True
lhcbApp.Simulation  = True

lhcbApp.DDDBtag   = "dddb-20190223" 
lhcbApp.CondDBtag = "sim-20180530-vc-md100"

import GaudiPython,ROOT,math,sys
from ROOT import *
from math import *
from array import *
import GaudiKernel.SystemOfUnits as units
import pickle, pprint

appMgr = gaudimodule.AppMgr()
appMgr.initialize()
#appMgr = GaudiPython.AppMgr()
#appMgr.config(files=["upgradecond.py"])
sel    = appMgr.evtsel()
sel.PrintFreq = 100
evt = appMgr.evtsvc()

det= appMgr.detSvc()
print det
#muon_det = det['dd/Structure/LHCb/DownstreamRegion/Muon']
muondet = det['/dd/Structure/LHCb/DownstreamRegion/Muon']

h_m2_hitxy=TH2D("M2 hitxy","xy of hits ",200,-6000,6000,200,5000,5000);
h_m3_hitxy=TH2D("M3 hitxy","xy of hits ",200,-6000,6000,200,5000,5000);
h_m4_hitxy=TH2D("M4 hitxy","xy of hits ",200,-6000,6000,200,5000,5000);
h_m5_hitxy=TH2D("M5 hitxy","xy of hits ",200,-6000,6000,200,5000,5000);

h_m2_hitxy=TH2D("M2 r","r of hits ",200,-6000,6000,200,5000,5000);

f_out = ROOT.TFile.Open('modelplots_tests.root', 'RECREATE')
f_out.cd()
tree = TTree('vars', 'vars')

m_x = array('f', [0])
tree.Branch('m_x', m_x, 'm_x/F')
m_y = array('f', [0])
tree.Branch('m_y', m_y, 'm_y/F')
m_z = array('f', [0])
tree.Branch('m_z', m_z, 'm_z/F')
m_r = array('f', [0])
tree.Branch('m_r', m_r, 'm_r/F')
m_phi = array('f', [0])
tree.Branch('m_phi', m_phi, 'm_phi/F')
m_stat = array('f', [0])
tree.Branch('m_stat', m_stat, 'm_stat/F')
m_reg = array('f', [0])
tree.Branch('m_reg', m_reg, 'm_reg/F')
m_time = array('f', [0])
tree.Branch('m_time', m_time, 'm_time/F')

m_evt = array('i', [0])
tree.Branch('m_evt', m_evt, 'm_evt/I')

#local track vars
m_loc_phi = array('f', [0])
tree.Branch('m_loc_phi', m_loc_phi, 'm_loc_phi/F')

m_loc_theta = array('f', [0])
tree.Branch('m_loc_theta', m_loc_theta, 'm_loc_theta/F')

m_pid = array('f', [0])
tree.Branch('m_pid', m_pid, 'm_pid/F')

#run_nevents = 1000*len(sys.argv[1:])
ntotevts=1
while ntotevts <= n_evts :
    print "EVENT:", ntotevts 
    ntotevts=ntotevts+1
    appMgr.run(1)
    if not evt['MC/Particles']: break

    hepmcevents = evt['Gen/HepMCEvents']
    mcmuonhits=evt['MC/Muon/Hits']
    mcevents = evt['/Event/Gen']
    mcmuonverts=evt['MC/Vertices']
#    for hit in mcmuonhits:
#        sens_id = hit.sensDetID()
#        sens_id = sens_id
#        m_stat[0] = muondet.stationID(sens_id)
#        chamber = muondet.stationID(sens_id)
#        m_time[0] = hit.time()
#        m_x[0] = hit.entry().x()
#        m_y[0] = hit.entry().y()
#        m_z[0] = hit.entry().z()
#        m_r[0] = leb.radius(hit)
#        m_phi[0] = hit.entry().phi()
#        m_reg[0] = muondet.regionID(sens_id)
#        m_evt[0] = ntotevts
#
#	#get local track vector
#	track = hit.exit()-hit.entry()
#	m_loc_phi[0] = track.phi()
#	m_loc_theta[0] = track.theta()
#	
#	m_pid[0] = hit.mcParticle().particleID().pid()
#
#        tree.Fill()
# 
#f_out.Write()
#f_out.Close()
#        #print hitdict
#        #print leb.radius(hitdict)
#        #print hit.entry().r()
#        #print hit.entry().phi()
#        
#        
#
##print hit
##        print "==="
##        print muondet.stationID(hit.sensDetID())
##
##        id=hit.sensDetID()
##        chamber_id=id&0x1FFFFC
##        q=id>>15
##        m=(id>>12)&7
##        r=(id>>10)&3
##        chm=(id>>2)&255
##        gap=id&3
#
##        print m, r
