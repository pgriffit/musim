from math import sqrt
from array import array

def radius(hit):
    x = hit.entry().x()
    y = hit.entry().y()
    return sqrt(x*x +y*y)

def phi_loc(hit):
    
    return

#def trace(det, hits):
    
class TreeBookeeper:
    
    def __init__(self, tree, branch_dict = {}):
        self.tree = tree
        self.branches = dict()
    #{'branch name', 'ctype'}
        if branch_dict:
            for b in branch_dict.items:
                self.add_branch(k, v)
                print 'TreeBookeeper: Added branch {} of type {}'.format(
            k,
            v)
    
    def add_branch(self, name, ctype):
        var = array(ctype.lower(), [0])
        branch = self.tree.Branch(name, var, '{}/{}'.format(name, ctype.upper()))
        self.branches[name] = var

    def fill_branch(self, name, val):
        self.branches[name][0] = val
        

class Tracker:
    """
    A container class that stores all tracks made from gas gap hits
    """
    def __init__(self, det):
        self.tracks = []
        self.det = det

    def add_hit(self, hit):
        """
        add a new hit to a track. The current contained tracks are
        scanned to see if a hit from that this unique particle in 
        this particular muon chamber already exists. If it does not,
        a new track is created. Else, it is appended to the existing track
        """
        
        # get sensitive detector ID and use it to find station, chamber and gap of hit 
        sid = hit.sensDetID()
        station = int(self.det.stationID(sid))
        chamber = int(self.det.chamberID(sid))
        gap = int(self.det.gapID(sid))
        particle = hit.mcParticle()

        #create a unique ID for this particular mcParticle hitting this chamber
        uid = '{}_{}_{}'.format(station, chamber, particle.key())
        
        # if no track with this unique ID exists, create a new track
        if uid not in [t.uid for t in self.tracks]:
            self.tracks.append(Track(hit, station, chamber, particle, gap))
        else:
        # if it does exist, check if there is already a hit in this 
        # particular gap to avoid counting multiple MCHits it one gap.
        # if new gap hit, add it to the existing track
            for t in self.tracks:
                if uid == t.uid and not self.gap_hit_exists(t, hit):
                    t.add_hit(hit, gap)
    
    def gap_hit_exists(self, t, h):
        """
        Compare hit with existing hits in track to see if there is already
        a hit in this gap
        """
        new_gap = int(self.det.gapID(h.sensDetID()))
        for hit in t.hits:
            sid = hit.sensDetID()
            gap = int(self.det.gapID(hit.sensDetID()))
            if gap == new_gap:
                return True
        return False

    def track_filter(self, gaps = -1, station = -1):
        """
        Function for returning track types per station
        """
        if gaps > -1:
            gap_filter = True
        if station > -1:
            stat_filter = True
    
        if all([gap_filter, stat_filter]):
            return [t for t in self.tracks if t.track_type() == gaps and t.stat == station]
        elif gap_filter:
            return [t for t in self.tracks if t.track_type() == gaps]
        elif stat_filter:
            return [t for t in self.tracks if t.stat == station]
        else:
            return []

    
class Track:
    """
    Track object. Represents a track made up of between 1 and 4 MCHits
    in gas gaps
    """
    def __init__(self, hit, station, chamber, particle, gap):
        
        # uid attribute is a unique identifier for this track
        self.uid = '{}_{}_{}'.format(station, chamber, particle.key())

        self.hits = []
        self.gaps = []
#        self.ttype = 1

        self.stat = station
        self.chamb = chamber

        self.time = hit.time()

        self.en_x = hit.entry().x()  
        self.en_y = hit.entry().y()  
        self.en_z = hit.entry().z()

        self.ex_x = hit.exit().x()  
        self.ex_y = hit.exit().y()  
        self.ex_z = hit.exit().z()

        self.r = radius(hit)

        self.length = sqrt(hit.displacement().Mag2())

        track = hit.exit()-hit.entry()
    
        self.phi = hit.entry().phi()
        self.loc_phi = track.phi()
        self.loc_theta = track.theta()
    
        self.pid = hit.mcParticle().particleID().pid()
    
        self.add_hit(hit, gap)

    def add_hit(self, hit, gap):
        self.hits.append(hit)
        self.gaps.append(gap)

    def track_type(self):
        #check track type by seeing len() of set of gaps. 
        #set() should be redundant due to the check for unique gap hits
        # in Tracker class. (Tracker.gas_hit_exists())
        return len(set(self.gaps))


        
        
#class Hit:
#    def __init__(self, hit, det):
#        self.det = det
#   self.sid = hit.getSensID()
#   self.stat = det.stationID(self.sid)
#   self.chamb = det.chamberID(self.sid)
#
#   self.time = hit.time()
#
#        self.en_x = hit.entry().x()  
#        self.en_y = hit.entry().x()  
#        self.en_z = hit.entry().x()
#
#        self.ex_x = hit.exit().x()  
#        self.ex_y = hit.exit().x()  
#        self.ex_z = hit.exit().x()
#
#   self.r = radius(hit)
#
#   self.length = sqrt(hit.displacement().Mag2())
#
#   track = hit.exit()-hit.entry()
#   
#   self.phi = hit.entry().phi()
#   self.loc_phi = track.phi()
#   self.loc_theta = track.theta()
#   
#   self.pid = hit.mcParticle().particleID().pid()
#
