import math
from ROOT import MakeNullPointer,Math, TMath
class MuStation:
    """Data structs for station and region chambers"""
    def __init__(self, wires, pads, tvt, dt):
        self.WiresPerRegion = wires
        self.CatPadsPerRegion = pads
        self.tvt = tvt
        self.dt = dt


        #dictionary with number of hits produced by given tracks (all muon system)
        self.tracks2hits={}
        self.M2R1C1tracks2hits={}
        self.M2R1C2tracks2hits={}
        self.M2R1C5tracks2hits={}
        self.M2R1C6tracks2hits={}
        self.M2R1C9tracks2hits={}
        self.M2R1C10tracks2hits={}
        
        #define chamber->row,column dictionary for R1
        R1ch2rc={}
        R1ch2rc[0]=(0,0)
        R1ch2rc[1]=(0,1)
        R1ch2rc[2]=(0,2)
        R1ch2rc[3]=(0,3)
        R1ch2rc[4]=(1,0)
        R1ch2rc[5]=(1,3)
        R1ch2rc[6]=(2,0)
        R1ch2rc[7]=(2,3)
        R1ch2rc[8]=(3,0)
        R1ch2rc[9]=(3,1)
        R1ch2rc[10]=(3,2)
        R1ch2rc[11]=(3,3)
        self.R1ch2rc = R1ch2rc

        #define chamber->row,column dictionary for R2
        R2ch2rc={}
        R2ch2rc[0]=(0,0)
        R2ch2rc[1]=(0,1)
        R2ch2rc[2]=(0,2)
        R2ch2rc[3]=(0,3)
        R2ch2rc[4]=(1,0)
        R2ch2rc[5]=(1,1)
        R2ch2rc[6]=(1,2)
        R2ch2rc[7]=(1,3)
        R2ch2rc[8]=(2,0)
        R2ch2rc[9]=(2,3)
        R2ch2rc[10]=(3,0)
        R2ch2rc[11]=(3,3)
        R2ch2rc[12]=(4,0)
        R2ch2rc[13]=(4,3)
        R2ch2rc[14]=(5,0)
        R2ch2rc[15]=(5,3)
        R2ch2rc[16]=(6,0)
        R2ch2rc[17]=(6,1)
        R2ch2rc[18]=(6,2)
        R2ch2rc[19]=(6,3)
        R2ch2rc[20]=(7,0)
        R2ch2rc[21]=(7,1)
        R2ch2rc[22]=(7,2)
        R2ch2rc[23]=(7,3)
        self.R2ch2rc = R2ch2rc

        #define chamber->x,y coordinates dictionary for M2R1
        M2R1ch2xy={}
        M2R1ch2xy[0]=(458.2,375.69)
        M2R1ch2xy[1]=(158.2,378.09)
        M2R1ch2xy[2]=(-158.6,374.01)
        M2R1ch2xy[3]=(-461.9,376.41)
        M2R1ch2xy[4]=(466.7,128.88)
        M2R1ch2xy[5]=(-465.4,126.21)
        M2R1ch2xy[6]=(458.2,-124.61)
        M2R1ch2xy[7]=(-461.9,-126.68)
        M2R1ch2xy[8]=(466.7,-380.71)
        M2R1ch2xy[9]=(159.4,-379.01)
        M2R1ch2xy[10]=(-161.4,-381.20)
        M2R1ch2xy[11]=(-465.4,-380.68)
        self.M2R1ch2xy = M2R1ch2xy

        #define chamber->x,y coordinates dictionary for M2R2
        M2R2ch2xy={}
        M2R2ch2xy[0]=(914.4,881.08)
        M2R2ch2xy[1]=(307.8,875.88)
        M2R2ch2xy[2]=(-310.7,879.41)
        M2R2ch2xy[3]=(-910.6,874.21)
        M2R2ch2xy[4]=(921.3,634.78)
        M2R2ch2xy[5]=(313.5,638.58)
        M2R2ch2xy[6]=(-313.0,633.11)
        M2R2ch2xy[7]=(-927.6,636.91)
        M2R2ch2xy[8]=(914.4,378.08)
        M2R2ch2xy[9]=(-910.6,374.01)
        M2R2ch2xy[10]=(921.3,127.89)
        M2R2ch2xy[11]=(-927.6,127.21)
        M2R2ch2xy[12]=(914.4,-125.01)
        M2R2ch2xy[13]=(-910.6,-126.28)
        M2R2ch2xy[14]=(921.3,-379.01)
        M2R2ch2xy[15]=(-927.6,-382.39)
        M2R2ch2xy[16]=(914.4,-628.01)
        M2R2ch2xy[17]=(307.8,-624.81)
        M2R2ch2xy[18]=(-310.7,-629.68)
        M2R2ch2xy[19]=(-910.6,-626.48)
        M2R2ch2xy[20]=(921.3,-885.81)
        M2R2ch2xy[21]=(313.5,-890.41)
        M2R2ch2xy[22]=(-313.0,-887.48)
        M2R2ch2xy[23]=(-927.6,-892.08)
        self.M2R2ch2xy = M2R2ch2xy

        #define chamber->x,y coordinates dictionary for M3R1
        M3R1ch2xy={}
        M3R1ch2xy[0]=(495.1,404.95)
        M3R1ch2xy[1]=(171.3,407.34)
        M3R1ch2xy[2]=(-171.7,402.92)
        M3R1ch2xy[3]=(-498.9,405.29)
        M3R1ch2xy[4]=(503.6,138.42)
        M3R1ch2xy[5]=(-502.4,135.42)
        M3R1ch2xy[6]=(495.1,-134.94)
        M3R1ch2xy[7]=(-498.9,-137.38)
        M3R1ch2xy[8]=(503.6,-410.92)
        M3R1ch2xy[9]=(172.4,-409.15)
        M3R1ch2xy[10]=(-174.6,-412.98)
        M3R1ch2xy[11]=(-502.4,-411.17)
        self.M3R1ch2xy = M3R1ch2xy

        #define chamber->x,y coordinates dictionary for M3R2
        M3R2ch2xy={}
        M3R2ch2xy[0]=(986.9,949.97)
        M3R2ch2xy[1]=(332.7,944.90)
        M3R2ch2xy[2]=(-335.7,948.01)
        M3R2ch2xy[3]=(-983.3,942.4)
        M3R2ch2xy[4]=(994.0,683.92)
        M3R2ch2xy[5]=(338.4,687.74)
        M3R2ch2xy[6]=(-338.0,681.91)
        M3R2ch2xy[7]=(-991.8,685.63)
        M3R2ch2xy[8]=(987.0,407.34)
        M3R2ch2xy[9]=(-983.3,402.91)
        M3R2ch2xy[10]=(994.0,137.45)
        M3R2ch2xy[11]=(-1000.3,136.42)
        M3R2ch2xy[12]=(987.0,-135.34)
        M3R2ch2xy[13]=(-983.3,-136.97)
        M3R2ch2xy[14]=(994.0,-409.14)
        M3R2ch2xy[15]=(-1000.0,-412.98)
        M3R2ch2xy[16]=(986.6,-677.94)
        M3R2ch2xy[17]=(332.7,-674.84)
        M3R2ch2xy[18]=(-335.7,-680.02)
        M3R2ch2xy[19]=(-983.3,-676.87)
        M3R2ch2xy[20]=(992.9,-955.61)
        M3R2ch2xy[21]=(338.4,-960.22)
        M3R2ch2xy[22]=(-338.0,-957.72)
        M3R2ch2xy[23]=(-1000.3,-962.28)
        self.M3R2ch2xy = M3R2ch2xy


    def M2R1globxy2locxy(self,globx,globy,chm):
        return (globx-self.M2R1ch2xy[chm][0],globy-self.M2R1ch2xy[chm][1])

    def M2R2globxy2locxy(self,globx,globy,chm):
        return (globx-self.M2R2ch2xy[chm][0],globy-self.M2R2ch2xy[chm][1])

    def M3R1globxy2locxy(self,globx,globy,chm):
        return (globx-self.M3R1ch2xy[chm][0],globy-self.M3R1ch2xy[chm][1])

    def M3R2globxy2locxy(self,globx,globy,chm):
        return (globx-self.M3R2ch2xy[chm][0],globy-self.M3R2ch2xy[chm][1])

    def M2R1locxy2locrc(self,locx,locy):
        locrow = 7.0 - (math.floor(locy/31.625)+4.0)
        loccol = 47.0 - (math.floor(locx/6.417)+24.0)
        return(locrow,loccol)

    def M2R2locxy2locrc(self,locx,locy):
        locrow = 3.0 - (math.floor(locy/63.250)+2.0)
        loccol = 47.0 - (math.floor(locx/12.750)+24.0)
        return(locrow,loccol)

    def M3R1locxy2locrc(self,locx,locy):
        locrow = 7.0 - (math.floor(locy/34.125)+4.0)
        loccol = 47.0 - (math.floor(locx/6.917)+24.0)
        return(locrow,loccol)

    def M3R2locxy2locrc(self,locx,locy):
        locrow = 3.0 - (math.floor(locy/68.250)+2.0)
        loccol = 47.0 - (math.floor(locx/13.800)+24.0)
        return(locrow,loccol)

    def R1locrc2globrc(self,locrow,loccol,chm):
        globrow = int(self.R1ch2rc[chm][0]*8 + locrow)
        globcol = int(self.R1ch2rc[chm][1]*48 + loccol)
        return(globrow,globcol)

    def R2locrc2globrc(self,locrow,loccol,chm):
        globrow = int(self.R2ch2rc[chm][0]*4 + locrow)
        globcol = int(self.R2ch2rc[chm][1]*48 + loccol)
        return(globrow,globcol)


    def M2R1globxy2globrc(self,globx,globy,chm):
        (locx,locy)=self.M2R1globxy2locxy(globx,globy,chm)
        (locrow,loccol)=self.M2R1locxy2locrc(locx,locy)
        return self.R1locrc2globrc(locrow,loccol,chm)

    def M2R2globxy2globrc(self,globx,globy,chm):
        (locx,locy)=self.M2R2globxy2locxy(globx,globy,chm)
        (locrow,loccol)=self.M2R2locxy2locrc(locx,locy)
        return self.R2locrc2globrc(locrow,loccol,chm)

    def M3R1globxy2globrc(self,globx,globy,chm):
        (locx,locy)=self.M3R1globxy2locxy(globx,globy,chm)
        (locrow,loccol)=self.M3R1locxy2locrc(locx,locy)
        return self.R1locrc2globrc(locrow,loccol,chm)

    def M3R2globxy2globrc(self,globx,globy,chm):
        (locx,locy)=self.M3R2globxy2locxy(globx,globy,chm)
        (locrow,loccol)=self.M3R2locxy2locrc(locx,locy)
        return self.R2locrc2globrc(locrow,loccol,chm)

    def RegionsTracksError(self,x,S):
        dfx=TMath.Power(math.sqrt(x)/(S*self.tvt),2)
        dfs=TMath.Power((x*0.01*S)/(S*S*self.tvt),2)
        dft=TMath.Power((x*self.dt)/(S*self.tvt*self.tvt),2)
        return math.sqrt(dfx+dfs+dft)

    def WiresTracksError(self,x):
        dfx=TMath.Power(math.sqrt(x)/(self.WiresPerRegion*self.tvt),2)
        dft=TMath.Power((x*self.dt)/(self.WiresPerRegion*self.tvt*self.tvt),2)
        return math.sqrt(dfx+dft)

    def CatPadsTracksError(self,x):
        dfx=TMath.Power(math.sqrt(x)/(self.CatPadsPerRegion*self.tvt),2)
        dft=TMath.Power((x*self.dt)/(self.CatPadsPerRegion*self.tvt*self.tvt),2)
        return math.sqrt(dfx+dft)

    def RatioError(self,hi,tr):
        dfhi=TMath.Power(math.sqrt(hi)/tr,2)
        dftr=TMath.Power((hi*math.sqrt(tr))/(tr*tr),2)
        return math.sqrt(dfhi+dftr)

