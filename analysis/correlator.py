import numpy as np

hasroot = True
try:
    import ROOT
except ImportError:
    print("WARNING: ROOT is not installed. TH1 read-in feature disabled")
    hasroot = False

class HitCorrelator:
    """
    Generate correlations for HT and LT plots,
    to satisfy the diagonal and vertical projections
    args:
    ht: high threshold histogram, list or numpy array
    lt: low threshold histogram, list or numpy array
    """

    def __init__(self, **kwargs):
        self.debug = kwargs.get('debug', False)
        self.ht = self.process_input(kwargs.get('ht'))
        self.lt = self.process_input(kwargs.get('lt'))
        self.bkg = self.create_bkg_array(self.ht, self.lt)
        self.name = kwargs.get(name, "")
        #Assert that the data are of the same size
        assert(len(self.ht) == len(self.lt))
        #start with 2 row matrix. Fill new row with zeros
        self.matrix = np.vstack([self.ht, [0]*len(self.ht)])

    def dprint(self, msg):
        if self.debug:
            print msg
    
    def pprint_matrix(self, matrix):
        print np.flipud(matrix)
    
    def dprint_matrix(self, matrix):
        self.pprint_matrix(matrix)

    def process_input(self, data):
        """
        Read in numpy array, root TH1 or python list.
        Convert all data inputs to numpy arrays.
        """
        if isinstance(data, list):
            return np.asarray(data)
        elif isinstance(data, np.ndarray):
            return data
        else:
            try:
                return self.hist_to_array(data)
            except AttributeError as e:
                #shouldn't ever fail here if ROOT not installed, as there
                #would be no way to read in the histos in the first place.
                print('Data type not {} recognised.'.format(type(data)))
                raise e

    def hist_to_array(self, hist):
        """
        Convert root histogram to numpy array
        """
        return np.asarray([hist.GetBinContent(i) for i in xrange(1,hist.GetNbinsX()+1)])
    
    def matrix_to_hist(self, **kwargs):
        matrix = kwargs.get('matrix', self.matrix)
        hist = kwargs.get('hist')
        for x in matrix.shape[0]:
            for y in matrix.shape[1]:
                hist.Fill(matrix[x][y])
        return hist
    
    def create_bkg_array(self, ht, lt):
        """
        Calculate background hits array by subtracting high threshold MC from low.
        """
        return lt - ht

    def project(self, matrix, axis = 0):
        """
        Create vertical projection of matrix
        which should be consistent with ht
        """
        return np.sum(matrix, axis = axis)
    
    def build_correlation(self, **kwargs):
        """
        Main use function. If no arguments specified,
        uses own lt, ht and matrix and sets output type
        to numpy array.
        kwargs:
            matrix: 2D numpy array
            ht: high threshold MC data (numpy array, TH1 or list)
            lt: low threshold MC data (numpy array, TH1 or list)
            output: output type ('array' or 'hist')
        """
        matrix = kwargs.get('matrix', self.matrix)
        lt = self.process_input(kwargs.get('lt', self.lt))
        ht = self.process_input(kwargs.get('ht', self.ht))
        save = kwargs.get('save', True)
        output = kwargs.get('output', 'array')
        
        self.dprint("Starting point:")
        self.dprint_matrix(matrix)
        #first run the "zig-zag" algorithm on the two rows
        matrix = self.solve_zigzag(matrix, ht, lt)
        #build up correlation by propagating negatives and resolving diagonals
        matrix = self.propagate(matrix, lt)

        if save:
            self.matrix = matrix

        self.dprint("Solved as:")
        self.dprint_matrix(matrix)
        if output == "array":
            return matrix
        elif output == "hist":
            return self.matrix_to_hist(matrix)
        else:
            raise AttributeError('matrix output type {} not recognised'.format(output))

    def solve_zigzag(self, matrix, ht, lt):
        #generate initial point from 2D constraints
        self.dprint("Solving zigzag alg.")
        for i in range(matrix.shape[1]):
            matrix = self.solve_diagonal(i, matrix, lt)
            matrix = self.solve_vertical(i, matrix, ht)
        self.dprint_matrix(matrix)
        return matrix

    def propagate(self, matrix, lt):        
       #continue moving negatives and resolving until square matrix generated
        row = 0
        while matrix.shape[0] < matrix.shape[1]:
            matrix = np.vstack([matrix, [0]*matrix.shape[1]])
            matrix = self.move_negatives(row, matrix, lt)
        return matrix

    def solve_diagonal(self, c_index, matrix, lt):
        """
        Replace relevant element of matrix to satisfy the trace constraint
        from the lt array.
        c_index: column index
        matrix: current matrix
        lt: low threshold data
        """
        #since it is the "anti-trace", need to first flip the matrix.
        flipped_matrix = np.fliplr(matrix)
        #get row offset to propagate vertically up the matrix depending on n. of rows.
        trace_offset = matrix.shape[1]-c_index-1
        #get column offset to propagate backards (forwards in flipped matrix) along diagonal
        #as matrix is generated vertically.
        trace = flipped_matrix.trace(trace_offset)
        column_offset = 2 - matrix.shape[0] + c_index 
        #modify element to satisfy diagonal sum constraint 
        new_val = lt[c_index] + matrix[-2][column_offset] - trace
        #self.dprint('lt constrant: {}, row index: {}, row_value: {}, trace = {}, column index: {}'.format(
        #    lt[c_index],
        #    c_index,
        #    matrix[-2][column_offset],
        #    trace,
        #    column_offset))
        #self.dprint('new value = {} + {} - {} = {}'.format(lt[c_index], matrix[-2][column_offset], trace, new_val))
        #insert trace into new row
        matrix[-2][column_offset] = new_val
        return matrix
    
    def solve_vertical(self, c_index, matrix, ht):
        """
        Replace values in elements to satisfy vertical projection constrained to ht.
        c_index: column index
        matrix: current matrix
        ht: high threshold data
        """
        projection = self.project(matrix)  
        #calculate the vertical from contraints
        result = ht[c_index] - projection[c_index] 
        #put value into new row
        matrix[-1][c_index] = result
        return matrix
	
    def move_negatives(self, r_index, matrix, lt):
        """
        Propagate generated negative values up into next free element in top row and unsign.
        r_index: row to work on
        matrix: current matrix
        lt: low threshold data
        """
        #loop over bottom row and check for negatives
        next_free = 0
        for c_index in range(matrix.shape[1]):
            el = matrix[r_index][c_index]
            #if negative, move positive value to next free element and leave zero in its place
            if el < 0:
                matrix[-1][next_free] = -1*el
                matrix[r_index][c_index] = 0
                next_free+=1
                
                self.dprint("moving negative value {}".format(el))
                self.dprint_matrix(matrix)
                
                for k in range(matrix.shape[1]):
                    matrix = self.solve_diagonal(k, matrix, lt)

                self.dprint("resolving diagonals:")
                self.dprint_matrix(matrix)
        return matrix
