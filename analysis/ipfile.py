from ROOT import TH1D,TH2D, TH3D
from ROOT import MakeNullPointer,Math, TMath

from Configurables import LHCbApp
from Configurables import EventClockSvc

from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import *
import GaudiPython 
from GaudiPython import gbl
from GaudiConf import IOHelper

import pickle
from params import *

IOHelper('ROOT').inputFiles(['/afs/cern.ch/work/p/pgriffit/MUON/hcal_shielding_gauss/files/{}.sim'.format(jname)]),

importOptions('$APPCONFIGOPTS/DisableLFC.py') 
EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"
lhcbApp  = LHCbApp()
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'res')
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
import PartProp.Service, PartProp.decorators
DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle'
DataOnDemandSvc().AlgMap['/Event/MC/Vertices']  = 'UnpackMCVertex'
DataOnDemandSvc().AlgMap['/Event/MC/Muon/Hits']  = 'DataPacking::Unpack<LHCb::MCMuonHitPacker>/UnpackMCMuonHits'
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
lhcbApp.DataType = "Upgrade"
from Configurables import CondDB
CondDB().Upgrade = True
lhcbApp.Simulation  = True
import GaudiPython,ROOT,math,sys
from ROOT import *
from math import *
from array import *
import GaudiKernel.SystemOfUnits as units
import pickle, pprint
appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
sel.PrintFreq = 10000
evt = appMgr.evtsvc()


ntotevts=1
while ntotevts<=run_nevents :
    ntotevts=ntotevts+1
    appMgr.run(1)
    if not evt['MC/Particles']: break

    ps = evt['MC/Muon/Hits']
    for p in ps:
        print(p)
