import sys
import ROOT
import correlator

files = {'ht': ROOT.TFile.Open(sys.argv[1], "READONLY"),
         'lt': ROOT.TFile.Open(sys.argv[2], "READONLY")}

mc_types = ['ht', 'lt']
station_n = [2, 3, 4, 5]
gap_n = [1, 2, 3, 4]

def get_name(s, g):
    return 'M{}_{}gap'.format(s, g)

histos = {mc:{get_name(s, g): files[mc].Get(get_name(s, g))
histos_out = {}
for s in station_n for g in gap_n} for mc in mc_types}
print histos

for s in station_n:
    for g in gap_n:
        name = get_name(s, g)
        ht = histos['ht'][get_name(s, g)]
        lt = histos['lt'][get_name(s, g)]
        corr = correlator.HitCorrelator(ht=ht, lt=lt, debug=True)
        corr.build_correlation()
        print("FINAL RESULT:")
        corr.dprint_matrix(corr.matrix)
        histos_out[name] = corr.matrix_to_hist(
            matrix = ROOT.TH2F(
                name,
                name,
                ht.GetNBinsX()-1),
                ht.GetXMin(),
                ht.GetXMax(),
                lt.GetNBinsX()-1),
                lt.GetXMin(),
                lt.GetXMax(),
        )
        print histos_out

