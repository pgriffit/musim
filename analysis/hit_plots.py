import ROOT

ROOT.gStyle.SetOptStat(0)

f_std = ROOT.TFile.Open('./modeltracks_std.root')
f_lt = ROOT.TFile.Open('./modeltracks_lt.root')

f_std.cd()
ht_vars = f_std.Get('vars')

f_lt.cd()
lt_vars = f_lt.Get('vars')

hit_types = [1,2,3,4]
stations = [0,1,2,3]

lt_rplots_ttype = [[ROOT.TH1F('', '', 50, 0, 6000) for i in hit_types] for j in stations] 
ht_rplots_ttype = [[ROOT.TH1F('', '', 50, 0, 6000) for i in hit_types] for j in stations] 

ht_rplots_stats = [ROOT.TH1F('', '', 50, 0, 6000) for i in stations]
lt_rplots_stats = [ROOT.TH1F('', '', 50, 0, 6000) for i in stations]

for i, e in enumerate(lt_vars):
#    if i > 100:
#	break
    hit_type = int(e.m_hit_type)
    station = int(e.m_stat)

    lt_rplots_ttype[station][hit_type-1].Fill(e.m_r)
    lt_rplots_stats[station].Fill(e.m_r)
 
for i, e in enumerate(ht_vars):
#    if i > 100:
#	break
    hit_type = int(e.m_hit_type)
    station = int(e.m_stat)

    ht_rplots_ttype[station][hit_type-1].Fill(e.m_r)
    ht_rplots_stats[station].Fill(e.m_r)

canv0 = ROOT.TCanvas()
canv0.Divide(2,2)
for i in stations:
    canv0.cd(i+1)
    lt_rplots_stats[i].Draw()
    ht_rplots_stats[i].SetLineColor(2)
    ht_rplots_stats[i].Draw("SAME")
    canv = ROOT.TCanvas()
    canv.SetLogy()
    canv.Divide(2,2)
    for j in hit_types:
        canv.cd(j)
	print i, j, lt_rplots_ttype[i][j-1], ht_rplots_ttype[i][j-1]
	lt_rplots_ttype[i][j-1].Draw()
	ht_rplots_ttype[i][j-1].SetLineColor(2)
	ht_rplots_ttype[i][j-1].Draw("SAME")
    canv.SaveAs("rplots_hit_types_M{}.pdf".format(i+2))
    canv.Clear()        

canv0.SaveAs("rplots_stations.pdf")



