from ROOT import TH1D,TH2D, TH3D
from ROOT import MakeNullPointer,Math, TMath

from Configurables import LHCbApp
from Configurables import EventClockSvc

from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import *
import GaudiPython 
from GaudiPython import gbl
from GaudiConf import IOHelper

import pickle
from params import *

IOHelper('ROOT').inputFiles(['{}/{}.sim'.format(simpath, jname)]),

importOptions('$APPCONFIGOPTS/DisableLFC.py') 
EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"
lhcbApp  = LHCbApp()
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'res')
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
import PartProp.Service, PartProp.decorators
DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle'
DataOnDemandSvc().AlgMap['/Event/MC/Vertices']  = 'UnpackMCVertex'
DataOnDemandSvc().AlgMap['/Event/MC/Muon/Hits']  = 'DataPacking::Unpack<LHCb::MCMuonHitPacker>/UnpackMCMuonHits'
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
lhcbApp.DataType = "Upgrade"
from Configurables import CondDB
CondDB().Upgrade = True
lhcbApp.Simulation  = True
import GaudiPython,ROOT,math,sys
from ROOT import *
from math import *
from array import *
import GaudiKernel.SystemOfUnits as units
import pickle, pprint
appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
sel.PrintFreq = 10
evt = appMgr.evtsvc()

xa = ( -16000 , 16000 )
#ya = ( -16000 , 16000 )
ya = ( -4000 , 8000 )
#za = ( -2000, 30000 )
za = ( 18000, 26000 )


h_hitxy=TH2D("hitxy","xy of hits ", 200, xa[0], xa[1],200, ya[0], ya[1]);
h_hitxz=TH2D("hitxz","xz of hits ", 200,xa[0], xa[1], 200,za[0],za[1]);
h_hityz=TH2D("hityz","yz of hits ", 200, za[0], za[1], 200, ya[0], ya[1]);

h_hitxyz=TH3D("hitxyz","xyz of hits ",400, za[0], za[1], 400, xa[0], xa[1], 400, ya[0], ya[1]); #actually zyx not xyz

ntotevts=1
x_list = []
y_list = []
z_list = []
t_list = []

while ntotevts<=run_nevents:

    ntotevts=ntotevts+1
    appMgr.run(1)
    if not evt['MC/Particles']: break

    mcevents = evt['/Event/Gen']
    mcmuonhits=evt['MC/Vertices']
    print mcmuonhits[0].position().X()
    for hit in mcmuonhits:
        entx=hit.position().X()
        enty=hit.position().Y()
        entz=hit.position().Z()
        entt=hit.time()
        #print(entx, enty, entz)
        #print(hit)

        h_hitxy.Fill(entx, enty)
        h_hitxz.Fill(entx, entz)
        h_hityz.Fill(entz, enty)
        h_hitxyz.Fill(entz, entx, enty)

        x_list.append(entx)
        y_list.append(enty)
        z_list.append(entz)
        t_list.append(entt)


print('=========================')
print('x:', min(x_list), max(x_list))
print('y:', min(y_list), max(y_list))
print('z:', min(z_list), max(z_list))
print('=========================')

f=TFile("results/{}_hits.root".format(jname),"recreate")
h_hitxy.Write()
h_hitxz.Write()
h_hityz.Write()
h_hitxyz.Write()
f.Close()

with open('entry_points_test.dat', 'w') as df:
    for event in zip(x_list, y_list, z_list, t_list):
        df.write('{}, {}, {}, {}\n'.format(
            event[0], 
            event[1], 
            event[2],
            event[3]))


