import sys
from ROOT import TH1D,TH2D
from ROOT import MakeNullPointer,Math, TMath

from Configurables import LHCbApp
from Configurables import EventClockSvc

from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import *
import GaudiPython
from GaudiPython import gbl
from GaudiConf import IOHelper

import leb

import pickle
import gaudimodule
#from params import *
#from stations import MuStation

#mu = MuStation(WiresPerRegion, CatPadsPerRegion, tvt, dt)
n_evts = 10
#IOHelper('ROOT').inputFiles(['{}/{}.sim'.format(simpath, jname)]),
input_files = sys.argv[1:]
if n_evts < 0:
    n_evts = sum([[int(s) for s in f.split('_') if s.isdigit()][0] for f in input_files])
     
print "TOTAL EVENTS TO PROCESS: ", n_evts
IOHelper('ROOT').inputFiles(sys.argv[1:])
#IOHelper('ROOT').inputFiles(['{}/{}'.format(simpath, f) for f in data]),
print IOHelper('ROOT').inputFiles

### MORE IMPORTS ###

#importOptions('$APPCONFIGOPTS/DisableLFC.py')
#EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"
lhcbApp  = LHCbApp()
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'res')
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
import PartProp.Service, PartProp.decorators
DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle'
DataOnDemandSvc().AlgMap['/Event/MC/Vertices']  = 'UnpackMCVertex'
DataOnDemandSvc().AlgMap['/Event/MC/Muon/Hits']  = 'DataPacking::Unpack<LHCb::MCMuonHitPacker>/UnpackMCMuonHits'
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
lhcbApp.DataType = "Upgrade"
from Configurables import CondDB
CondDB().Upgrade = True
lhcbApp.Simulation  = True

lhcbApp.DDDBtag   = "dddb-20190223" 
lhcbApp.CondDBtag = "sim-20180530-vc-md100"

import GaudiPython,ROOT,math,sys
from ROOT import *
from math import *
from array import *
import GaudiKernel.SystemOfUnits as units
import pickle, pprint

appMgr = gaudimodule.AppMgr()
appMgr.initialize()
#appMgr = GaudiPython.AppMgr()
#appMgr.config(files=["upgradecond.py"])
sel    = appMgr.evtsel()
sel.PrintFreq = 100
evt = appMgr.evtsvc()

det= appMgr.detSvc()
print det
#muon_det = det['dd/Structure/LHCb/DownstreamRegion/Muon']
muondet = det['/dd/Structure/LHCb/DownstreamRegion/Muon']

m_stationNumber = muondet.stations();
m_regionNumber  = muondet.regions();
m_partition = m_regionNumber * m_stationNumber
gap = 0
for i in range(0, m_partition):
    stat = i / 4
    reg  = i - stat * 4
    gap = gap if gap >= muondet.gapsInRegion(stat, reg) else muondet.gapsInRegion(stat, reg)
    print stat, reg, gap




ntotevts=1
while ntotevts <= n_evts :
#    if 1000 % ntotevts == 0:
#    	print "EVENT:", ntotevts 
    ntotevts=ntotevts+1
    appMgr.run(1)
    if not evt['MC/Particles']: break

#    particles = evt['MC/Particles']
#    for particle in particles:
#	if abs(particle.particleID().pid()) == 2112:
#	    print particle

    hepevents = evt['Gen/HepMCEvents']
    hits=evt['MC/Muon/Hits']
    verts=evt['MC/Vertices']
    #for vert in mcmuonverts:
    #for hit in mcmuonhits:
