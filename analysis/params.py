from ROOT import TMath
#ana job settings
#path = '/afs/cern.ch/work/p/pgriffit/MUON/hcal_shielding_gauss/'
#simpath = '/afs/cern.ch/work/p/pgriffit/MUON/muwall_tests/'
simpath = '/st100-gr1/griffith/sim/'
#jname = 'muwall_1000'
jname = 'lt_minbias_QGSP_BERT_HP_100'
run_nevents = 400
hitdebug = False

#other
speed_of_light = 300 # mm/ns
T = 5*TMath.Power(10,-8) #bunch crossing [s]
tvt = run_nevents*T #total virtual time of nevtstorun events
dt = 5*T
M2R1area = 9000  #cm^2
M2R2area = 36290 #cm^2
M3R1area = 10500 #cm^2
M3R2area = 42305 #cm^2
WiresPerRegion = 576
CatPadsPerRegion = 768
M2R1chmarea =750 #cm^2
