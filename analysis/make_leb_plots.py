import ROOT

f_std = ROOT.TFile.Open('./modelplots_std.root')
f_lt = ROOT.TFile.Open('./modelplots_lt.root')

f_std.cd()
std_vars = f_std.Get('vars')

f_lt.cd()
lt_vars = f_lt.Get('vars')

#time plots
h_time_std = ROOT.TH1F('' ,'', 100, 0, 1e9)
h_time_lt = ROOT.TH1F('' ,'', 100, 0, 1e9)

#angle-radius plots
h_lt_thetaL_r = ROOT.TH2F('','',50, 0, 3.1459, 50, 0, 1000) 
h_std_thetaL_r = ROOT.TH2F('','', 50, 0, 3.1459, 50, 0, 1000) 


h_lt_phiL_r = ROOT.TH2F('','',50, -3.1569, 3.1459, 50, 0, 1000) 
h_std_phiL_r = ROOT.TH2F('','', 50, -3.1569, 3.1459, 50, 0, 1000) 

h_lt_phi_r = ROOT.TH2F('','',50, -3.1569, 3.1459, 50, 0, 1000) 
h_std_phi_r = ROOT.TH2F('','',50, -3.1569, 3.1459, 50, 0, 1000) 

h_lt_neutrons = ROOT.TH1F('' ,'', 100, 0, 20000)
h_lt_no_neutrons = ROOT.TH1F('' ,'', 100, 0, 20000)

#for i, e in enumerate(std_vars):
##    if i > 50000:
##	break
#    h_std.Fill(e.m_time)

#for i, e in enumerate(lt_vars):
##    if i > 50000:
##	break
#    h_lt.Fill(e.m_time)

for i, e in enumerate(lt_vars):
    if i > 27000:
	break
    if e.m_time!=20000:
        h_time_lt.Fill(e.m_time)
    
    h_lt_thetaL_r.Fill(e.m_loc_theta, e.m_r)
    h_lt_phiL_r.Fill(e.m_loc_phi, e.m_r)
    h_lt_phi_r.Fill(e.m_phi, e.m_r)

for i, e in enumerate(std_vars):
    if i > 27000:
	break
    if e.m_time!=20000:
        h_time_std.Fill(e.m_time)
   
    if e.m_stat==0: 
	h_std_thetaL_r.Fill(e.m_loc_theta, e.m_r)
	h_std_phiL_r.Fill(e.m_loc_phi, e.m_r)
	h_std_phi_r.Fill(e.m_phi, e.m_r)
#    if abs(e.m_mother_id) == 2112:
#	if e.m_time <1e9:
#	    h_lt_neutrons.Fill(e.m_time)
#    else:
#	if e.m_time !=20000 and e.m_time < 1e9:
#	    h_lt_no_neutrons.Fill(e.m_time)

c0 = ROOT.TCanvas()
c0.cd(0)
c0.SetLogy()
#lt time plot
h_time_lt.SetTitle("Hit timing lt")
h_time_lt.GetXaxis().SetTitle("time (ns)")
h_time_lt.Draw()
c0.SaveAs("pl_timing_lt.pdf")

c0.Clear()

h_time_std.SetTitle("Hit timing standard")
h_time_std.GetXaxis().SetTitle("time (ns)")
h_time_std.Draw()
c0.SaveAs("pl_timing_std.pdf")


c1 = ROOT.TCanvas()
c1.cd(0)
#phiL vs R plots
h_lt_phiL_r.SetTitle("#phi_{loc} vs R for M2 LT")
h_lt_phiL_r.GetXaxis().SetTitle("#phi_{loc}")
h_lt_phiL_r.GetYaxis().SetTitle("R (mm)")
h_lt_phiL_r.Draw("COLZ")
c1.SaveAs("pl_lt_phiL_r.pdf")

c1.Clear()

h_std_phiL_r.SetTitle("#phi_{loc} vs R for M2 STD")
h_std_phiL_r.GetXaxis().SetTitle("#phi_{loc}")
h_std_phiL_r.GetYaxis().SetTitle("R (mm)")
h_std_phiL_r.Draw("COLZ")
c1.SaveAs("pl_std_phiL_r.pdf")

c1.Clear()

#thetaL vs R plots
h_lt_thetaL_r.SetTitle("#theta_{loc} vs R for M2 LT")
h_lt_thetaL_r.GetXaxis().SetTitle("#theta_{loc}")
h_lt_thetaL_r.GetYaxis().SetTitle("R (mm)")
h_lt_thetaL_r.Draw("COLZ")
c1.SaveAs("pl_lt_thetaL_r.pdf")

c1.Clear()

h_std_thetaL_r.SetTitle("#theta_{loc} vs R for M2 STD")
h_std_thetaL_r.GetXaxis().SetTitle("#theta_{loc}")
h_std_thetaL_r.GetYaxis().SetTitle("R (mm)")
h_std_thetaL_r.Draw("COLZ")
c1.SaveAs("pl_std_thetaL_r.pdf")

#phi vs R plots
h_lt_phi_r.SetTitle("#phi vs R for M2 LT")
h_lt_phi_r.GetXaxis().SetTitle("#phi")
h_lt_phi_r.GetYaxis().SetTitle("R (mm)")
h_lt_phi_r.Draw("COLZ")
c1.SaveAs("pl_lt_phi_r.pdf")

c1.Clear()

h_std_phi_r.SetTitle("#phi vs R for M2 STD")
h_std_phi_r.GetXaxis().SetTitle("#phi")
h_std_phi_r.GetYaxis().SetTitle("R (mm)")
h_std_phi_r.Draw("COLZ")
c1.SaveAs("pl_std_phi_r.pdf")

c1.Clear()

h_lt_phi_r.Divide(h_std_phi_r)
h_lt_phi_r.SetTitle("#phi vs R diff for M2 LT")
h_lt_phi_r.GetXaxis().SetTitle("#phi")
h_lt_phi_r.GetYaxis().SetTitle("R (mm)")
h_lt_phi_r.Draw("COLZ")
c1.SaveAs("pl_diff_phi_r.pdf")

