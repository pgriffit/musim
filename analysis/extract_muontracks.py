import sys
from ROOT import TH1D,TH2D
from ROOT import MakeNullPointer,Math, TMath

from Configurables import LHCbApp
from Configurables import EventClockSvc

from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import *
import GaudiPython
from GaudiPython import gbl
from GaudiConf import IOHelper

import leb

import pickle
import gaudimodule
#from params import *
#from stations import MuStation

#mu = MuStation(WiresPerRegion, CatPadsPerRegion, tvt, dt)
savefile = True
n_evts = 37000
#n_evts = -1
#IOHelper('ROOT').inputFiles(['{}/{}.sim'.format(simpath, jname)]),
input_files = sys.argv[1:]
if n_evts < 0:
    n_evts = sum([[int(s) for s in f.split('_') if s.isdigit()][0] for f in input_files])
     
print "TOTAL EVENTS TO PROCESS: ", n_evts
IOHelper('ROOT').inputFiles(sys.argv[1:])
#IOHelper('ROOT').inputFiles(['{}/{}'.format(simpath, f) for f in data]),
print IOHelper('ROOT').inputFiles

### MORE IMPORTS ###

#importOptions('$APPCONFIGOPTS/DisableLFC.py')
#EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"
lhcbApp  = LHCbApp()
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'res')
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
import PartProp.Service, PartProp.decorators
DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle'
DataOnDemandSvc().AlgMap['/Event/MC/Vertices']  = 'UnpackMCVertex'
DataOnDemandSvc().AlgMap['/Event/MC/Muon/Hits']  = 'DataPacking::Unpack<LHCb::MCMuonHitPacker>/UnpackMCMuonHits'
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
lhcbApp.DataType = "Upgrade"
from Configurables import CondDB
CondDB().Upgrade = True
lhcbApp.Simulation  = True

lhcbApp.DDDBtag   = "dddb-20190223" 
lhcbApp.CondDBtag = "sim-20180530-vc-md100"

import GaudiPython,ROOT,math,sys
from ROOT import *
from math import *
from array import *
import GaudiKernel.SystemOfUnits as units
import pickle, pprint

appMgr = gaudimodule.AppMgr()
appMgr.initialize()
#appMgr = GaudiPython.AppMgr()
#appMgr.config(files=["upgradecond.py"])
sel = appMgr.evtsel()
sel.PrintFreq = 100
evt = appMgr.evtsvc()

det = appMgr.detSvc()
print det
#muon_det = det['dd/Structure/LHCb/DownstreamRegion/Muon']
muondet = det['/dd/Structure/LHCb/DownstreamRegion/Muon']

## Binning for Low and High Threshold simulation
h_m2_gap1_occu=TH1D("M2_1gap","hit occupancy M2, 1 gap  tracks",260,0,260);
h_m2_gap2_occu=TH1D("M2_2gap","hit occupancy M2, 2 gaps tracks",100,0,100);
h_m2_gap3_occu=TH1D("M2_3gap","hit occupancy M2, 3 gaps tracks",100,0,100);
h_m2_gap4_occu=TH1D("M2_4gap","hit occupancy M2, 4 gaps tracks",100,0,100);
#
h_m3_gap1_occu=TH1D("M3_1gap","hit occupancy M3, 1 gap  tracks",140,0,140);
h_m3_gap2_occu=TH1D("M3_2gap","hit occupancy M3, 2 gaps tracks",50,0,50);
h_m3_gap3_occu=TH1D("M3_3gap","hit occupancy M3, 3 gaps tracks",50,0,50);
h_m3_gap4_occu=TH1D("M3_4gap","hit occupancy M3, 4 gaps tracks",50,0,50);
#
h_m4_gap1_occu=TH1D("M4_1gap","hit occupancy M4, 1 gap  tracks",100,0,100);
h_m4_gap2_occu=TH1D("M4_2gap","hit occupancy M4, 2 gaps tracks",30,0,30);
h_m4_gap3_occu=TH1D("M4_3gap","hit occupancy M4, 3 gaps tracks",30,0,30);
h_m4_gap4_occu=TH1D("M4_4gap","hit occupancy M4, 4 gaps tracks",30,0,30);
#
h_m5_gap1_occu=TH1D("M5_1gap","hit occupancy M5, 1 gap  tracks",100,0,100);
h_m5_gap2_occu=TH1D("M5_2gap","hit occupancy M5, 2 gaps tracks",20,0,20);
h_m5_gap3_occu=TH1D("M5_3gap","hit occupancy M5, 3 gaps tracks",20,0,20);
h_m5_gap4_occu=TH1D("M5_4gap","hit occupancy M5, 4 gaps tracks",20,0,20);
## Binning for High Threshold simulation
#h_m2_gap1_occu=TH1D("M2_1gap","hit occupancy M2, 1 gap  tracks",80,0,80);
#h_m2_gap2_occu=TH1D("M2_2gap","hit occupancy M2, 2 gaps tracks",30,0,30);
#h_m2_gap3_occu=TH1D("M2_3gap","hit occupancy M2, 3 gaps tracks",20,0,20);
#h_m2_gap4_occu=TH1D("M2_4gap","hit occupancy M2, 4 gaps tracks",40,0,40);
#
#h_m3_gap1_occu=TH1D("M3_1gap","hit occupancy M3, 1 gap  tracks",30,0,30);
#h_m3_gap2_occu=TH1D("M3_2gap","hit occupancy M3, 2 gaps tracks",12,0,12);
#h_m3_gap3_occu=TH1D("M3_3gap","hit occupancy M3, 3 gaps tracks",12,0,12);
#h_m3_gap4_occu=TH1D("M3_4gap","hit occupancy M3, 4 gaps tracks",12,0,12);
#
#h_m4_gap1_occu=TH1D("M4_1gap","hit occupancy M4, 1 gap  tracks",50,0,50);
#h_m4_gap2_occu=TH1D("M4_2gap","hit occupancy M4, 2 gaps tracks",8,0,8);
#h_m4_gap3_occu=TH1D("M4_3gap","hit occupancy M4, 3 gaps tracks",8,0,8);
#h_m4_gap4_occu=TH1D("M4_4gap","hit occupancy M4, 4 gaps tracks",8,0,8);
#
#h_m5_gap1_occu=TH1D("M5_1gap","hit occupancy M5, 1 gap  tracks",30,0,30);
#h_m5_gap2_occu=TH1D("M5_2gap","hit occupancy M5, 2 gaps tracks",6,0,6);
#h_m5_gap3_occu=TH1D("M5_3gap","hit occupancy M5, 3 gaps tracks",6,0,6);
#h_m5_gap4_occu=TH1D("M5_4gap","hit occupancy M5, 4 gaps tracks",6,0,6);

f_out = ROOT.TFile.Open('modeltracks_LT.root', 'RECREATE')
f_out.cd()
tree = TTree('vars', 'vars')
vert_tree = TTree('vert_vars', 'vert_vars')
evt_tree = TTree('evt_tree', 'evt_tree')

m_x = array('f', [0])
tree.Branch('m_x', m_x, 'm_x/F')
m_y = array('f', [0])
tree.Branch('m_y', m_y, 'm_y/F')
m_z = array('f', [0])
tree.Branch('m_z', m_z, 'm_z/F')

n_x = array('f', [0])
tree.Branch('n_x', n_x, 'n_x/F')
n_y = array('f', [0])
tree.Branch('n_y', n_y, 'n_y/F')
n_z = array('f', [0])
tree.Branch('n_z', n_z, 'n_z/F')

m_r = array('f', [0])
tree.Branch('m_r', m_r, 'm_r/F')
m_phi = array('f', [0])
tree.Branch('m_phi', m_phi, 'm_phi/F')
m_stat = array('f', [0])
tree.Branch('m_stat', m_stat, 'm_stat/F')
m_reg = array('f', [0])
tree.Branch('m_reg', m_reg, 'm_reg/F')
m_time = array('f', [0])
tree.Branch('m_time', m_time, 'm_time/F')

# station 2, gaps 1, 2, 3, 4
m_mult_m2_g1 = array('f', [0])
evt_tree.Branch('m_mult_m2_g1', m_mult_m2_g1, 'm_mult_m2_g1/F')
m_mult_m2_g2 = array('f', [0])
evt_tree.Branch('m_mult_m2_g2', m_mult_m2_g2, 'm_mult_m2_g2/F')
m_mult_m2_g3 = array('f', [0])
evt_tree.Branch('m_mult_m2_g3', m_mult_m2_g3, 'm_mult_m2_g3/F')
m_mult_m2_g4 = array('f', [0])
evt_tree.Branch('m_mult_m2_g4', m_mult_m2_g4, 'm_mult_m2_g4/F')
# station 3, gaps 1, 2, 3, 4
m_mult_m3_g1 = array('f', [0])
evt_tree.Branch('m_mult_m3_g1', m_mult_m3_g1, 'm_mult_m3_g1/F')
m_mult_m3_g2 = array('f', [0])
evt_tree.Branch('m_mult_m3_g2', m_mult_m3_g2, 'm_mult_m3_g2/F')
m_mult_m3_g3 = array('f', [0])
evt_tree.Branch('m_mult_m3_g3', m_mult_m3_g3, 'm_mult_m3_g3/F')
m_mult_m3_g4 = array('f', [0])
evt_tree.Branch('m_mult_m3_g4', m_mult_m3_g4, 'm_mult_m3_g4/F')
# station 4, gaps 1, 2, 3, 4
m_mult_m4_g1 = array('f', [0])
evt_tree.Branch('m_mult_m4_g1', m_mult_m4_g1, 'm_mult_m4_g1/F')
m_mult_m4_g2 = array('f', [0])
evt_tree.Branch('m_mult_m4_g2', m_mult_m4_g2, 'm_mult_m4_g2/F')
m_mult_m4_g3 = array('f', [0])
evt_tree.Branch('m_mult_m4_g3', m_mult_m4_g3, 'm_mult_m4_g3/F')
m_mult_m4_g4 = array('f', [0])
evt_tree.Branch('m_mult_m4_g4', m_mult_m4_g4, 'm_mult_m4_g4/F')
# station 5, gaps 1, 2, 3, 4
m_mult_m5_g1 = array('f', [0])
evt_tree.Branch('m_mult_m5_g1', m_mult_m5_g1, 'm_mult_m5_g1/F')
m_mult_m5_g2 = array('f', [0])
evt_tree.Branch('m_mult_m5_g2', m_mult_m5_g2, 'm_mult_m5_g2/F')
m_mult_m5_g3 = array('f', [0])
evt_tree.Branch('m_mult_m5_g3', m_mult_m5_g3, 'm_mult_m5_g3/F')
m_mult_m5_g4 = array('f', [0])
evt_tree.Branch('m_mult_m5_g4', m_mult_m5_g4, 'm_mult_m5_g4/F')
#
#
m_evt = array('i', [0])
tree.Branch('m_evt', m_evt, 'm_evt/I')

#local track vars
m_loc_phi = array('f', [0])
tree.Branch('m_loc_phi', m_loc_phi, 'm_loc_phi/F')

m_loc_theta = array('f', [0])
tree.Branch('m_loc_theta', m_loc_theta, 'm_loc_theta/F')

m_pid = array('i', [0])
tree.Branch('m_pid', m_pid, 'm_pid/I')

m_mother_id = array('i', [0])
tree.Branch('m_mother_id', m_mother_id, 'm_mother_id/I')

m_length = array('f', [0])
tree.Branch('m_length', m_length, 'm_length/F')

m_hit_type = array('i', [0])
tree.Branch('m_hit_type', m_hit_type, 'm_hit_type/I')
##########
v_x = array('f', [0])
vert_tree.Branch('v_x', v_x, 'v_x/F')
v_y = array('f', [0])
vert_tree.Branch('v_y', v_y, 'v_y/F')
v_z = array('f', [0])
vert_tree.Branch('v_z', v_z, 'v_z/F')
v_t = array('f', [0])
vert_tree.Branch('v_t', v_t, 'v_t/F')
v_mother_id = array('i', [0])
vert_tree.Branch('v_mother_id', v_mother_id, 'v_mother_id/I')

v_length = array('f', [0])
vert_tree.Branch('v_length', v_length, 'v_length/F')

v_hit_type = array('i', [0])
vert_tree.Branch('v_hit_type', v_hit_type, 'v_hit_type/F')

#run_nevents = 1000*len(sys.argv[1:])
ntotevts=1
while ntotevts <= n_evts :
#    if 1000 % ntotevts == 0:
#    	print "EVENT:", ntotevts 
    ntotevts=ntotevts+1
    appMgr.run(1)

    if not evt['MC/Particles']:
	break

    hepmcevents = evt['Gen/HepMCEvents']
    mcmuonhits=evt['MC/Muon/Hits']
    
    tracker = leb.Tracker(muondet)
    multiplicity = 0
    for hit in mcmuonhits:
        tracker.add_hit(hit)
	multiplicity+=1

    for track in tracker.tracks:
        m_stat[0] = track.stat
        m_time[0] = track.time
        m_x[0] = track.en_x  
        m_y[0] = track.en_y
        m_z[0] = track.en_z
        m_r[0] = track.r

        n_x[0] = track.ex_x
        n_y[0] = track.ex_y
        n_z[0] = track.ex_z

        m_length[0] = track.length

        m_phi[0] = track.phi
        m_evt[0] = ntotevts

        m_loc_phi[0] = track.loc_phi
        m_loc_theta[0] = track.loc_theta
	
        m_pid[0] = track.pid
        m_hit_type[0] = track.track_type()

    	if savefile: 
            tree.Fill()

    m_mult_m2_g1[0] = len(tracker.track_filter(gaps=1, station=0))
    m_mult_m2_g2[0] = len(tracker.track_filter(gaps=2, station=0))
    m_mult_m2_g3[0] = len(tracker.track_filter(gaps=3, station=0))
    m_mult_m2_g4[0] = len(tracker.track_filter(gaps=4, station=0))
    h_m2_gap1_occu.Fill(m_mult_m2_g1[0])
    h_m2_gap2_occu.Fill(m_mult_m2_g2[0])
    h_m2_gap3_occu.Fill(m_mult_m2_g3[0])
    h_m2_gap4_occu.Fill(m_mult_m2_g4[0])

    m_mult_m3_g1[0] = len(tracker.track_filter(gaps=1, station=1))
    m_mult_m3_g2[0] = len(tracker.track_filter(gaps=2, station=1))
    m_mult_m3_g3[0] = len(tracker.track_filter(gaps=3, station=1))
    m_mult_m3_g4[0] = len(tracker.track_filter(gaps=4, station=1))
    h_m3_gap1_occu.Fill(m_mult_m3_g1[0])
    h_m3_gap2_occu.Fill(m_mult_m3_g2[0])
    h_m3_gap3_occu.Fill(m_mult_m3_g3[0])
    h_m3_gap4_occu.Fill(m_mult_m3_g4[0])

    m_mult_m4_g1[0] = len(tracker.track_filter(gaps=1, station=2))
    m_mult_m4_g2[0] = len(tracker.track_filter(gaps=2, station=2))
    m_mult_m4_g3[0] = len(tracker.track_filter(gaps=3, station=2))
    m_mult_m4_g4[0] = len(tracker.track_filter(gaps=4, station=2)) 
    h_m4_gap1_occu.Fill(m_mult_m4_g1[0])
    h_m4_gap2_occu.Fill(m_mult_m4_g2[0])
    h_m4_gap3_occu.Fill(m_mult_m4_g3[0])
    h_m4_gap4_occu.Fill(m_mult_m4_g4[0])
    
    m_mult_m5_g1[0] = len(tracker.track_filter(gaps=1, station=3))
    m_mult_m5_g2[0] = len(tracker.track_filter(gaps=2, station=3))
    m_mult_m5_g3[0] = len(tracker.track_filter(gaps=3, station=3))
    m_mult_m5_g4[0] = len(tracker.track_filter(gaps=4, station=3))
    h_m5_gap1_occu.Fill(m_mult_m5_g1[0])
    h_m5_gap2_occu.Fill(m_mult_m5_g2[0])
    h_m5_gap3_occu.Fill(m_mult_m5_g3[0])
    h_m5_gap4_occu.Fill(m_mult_m5_g4[0])
    
    if savefile: 
        evt_tree.Fill()

if savefile:
   
    f_out.Write()
    f_out.Close()

    f=TFile('occupancies_LT.root', 'RECREATE')
    h_m2_gap1_occu.Write()
    h_m2_gap2_occu.Write()
    h_m2_gap3_occu.Write()
    h_m2_gap4_occu.Write()
    h_m3_gap1_occu.Write()
    h_m3_gap2_occu.Write()
    h_m3_gap3_occu.Write()
    h_m3_gap4_occu.Write()
    h_m4_gap1_occu.Write()
    h_m4_gap2_occu.Write()
    h_m4_gap3_occu.Write()
    h_m4_gap4_occu.Write()
    h_m5_gap1_occu.Write()
    h_m5_gap2_occu.Write()
    h_m5_gap3_occu.Write()
    h_m5_gap4_occu.Write()
    f.Close()

