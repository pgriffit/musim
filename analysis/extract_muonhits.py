import sys
from ROOT import TH1D,TH2D
from ROOT import MakeNullPointer,Math, TMath

from Configurables import LHCbApp
from Configurables import EventClockSvc

from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import *
import GaudiPython
from GaudiPython import gbl
from GaudiConf import IOHelper

import leb

import pickle
import gaudimodule

savefile = False
n_evts = 11000
#IOHelper('ROOT').inputFiles(['{}/{}.sim'.format(simpath, jname)]),
input_files = sys.argv[1:]
#if n_evts is set to -1, get the total number of events from the filenames
if n_evts < 0:
    n_evts = sum([[int(s) for s in f.split('_') if s.isdigit()][0] for f in input_files])
     
print "TOTAL EVENTS TO PROCESS: ", n_evts
IOHelper('ROOT').inputFiles(sys.argv[1:])
print IOHelper('ROOT').inputFiles

lhcbApp  = LHCbApp()
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'res')
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
import PartProp.Service, PartProp.decorators
DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle'
DataOnDemandSvc().AlgMap['/Event/MC/Vertices']  = 'UnpackMCVertex'
DataOnDemandSvc().AlgMap['/Event/MC/Muon/Hits']  = 'DataPacking::Unpack<LHCb::MCMuonHitPacker>/UnpackMCMuonHits'
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ]
SequencerTimerTool().OutputLevel = INFO
lhcbApp.DataType = "Upgrade"
from Configurables import CondDB
CondDB().Upgrade = True
lhcbApp.Simulation  = True

lhcbApp.DDDBtag   = "dddb-20190223" 
lhcbApp.CondDBtag = "sim-20180530-vc-md100"

import GaudiPython,ROOT,math,sys
from ROOT import *
from math import *
from array import *
import GaudiKernel.SystemOfUnits as units
import pickle, pprint

appMgr = gaudimodule.AppMgr()
appMgr.initialize()
#appMgr = GaudiPython.AppMgr()
#appMgr.config(files=["upgradecond.py"])
sel    = appMgr.evtsel()
sel.PrintFreq = 100
evt = appMgr.evtsvc()

det= appMgr.detSvc()
print det
muondet = det['/dd/Structure/LHCb/DownstreamRegion/Muon']

#test histos
h_m2_hitxy=TH2D("M2 hitxy","xy of hits ",200,-6000,6000,200,5000,5000);
h_m3_hitxy=TH2D("M3 hitxy","xy of hits ",200,-6000,6000,200,5000,5000);
h_m4_hitxy=TH2D("M4 hitxy","xy of hits ",200,-6000,6000,200,5000,5000);
h_m5_hitxy=TH2D("M5 hitxy","xy of hits ",200,-6000,6000,200,5000,5000);

h_m2_hitr=TH2D("M2 r","r of hits ",200,-6000,6000,200,5000,5000);

f_out = ROOT.TFile.Open('modelplots.root', 'RECREATE')
f_out.cd()
tree = TTree('vars', 'vars')
vert_tree = TTree('vert_vars', 'vert_vars')

m_x_entry = array('f', [0])
tree.Branch('m_x_entry', m_x_entry, 'm_x_entry/F')
m_y_entry = array('f', [0])
tree.Branch('m_y_entry', m_y_entry, 'm_y_entry/F')
m_z_entry = array('f', [0])
tree.Branch('m_z_entry', m_z_entry, 'm_z_entry/F')
m_x_exit = array('f', [0])
tree.Branch('m_x_exit', m_x_exit, 'm_x_exit/F')
m_y_exit = array('f', [0])
tree.Branch('m_y_exit', m_y_exit, 'm_y_exit/F')
m_z_exit = array('f', [0])
tree.Branch('m_z_exit', m_z_exit, 'm_z_exit/F')
m_r = array('f', [0])
tree.Branch('m_r', m_r, 'm_r/F')
m_phi = array('f', [0])
tree.Branch('m_phi', m_phi, 'm_phi/F')
m_stat = array('f', [0])
tree.Branch('m_stat', m_stat, 'm_stat/F')
m_reg = array('f', [0])
tree.Branch('m_reg', m_reg, 'm_reg/F')
m_time = array('f', [0])
tree.Branch('m_time', m_time, 'm_time/F')

m_evt = array('i', [0])
tree.Branch('m_evt', m_evt, 'm_evt/I')

#local track vars
m_loc_phi = array('f', [0])
tree.Branch('m_loc_phi', m_loc_phi, 'm_loc_phi/F')

m_loc_theta = array('f', [0])
tree.Branch('m_loc_theta', m_loc_theta, 'm_loc_theta/F')

m_pid = array('i', [0])
tree.Branch('m_pid', m_pid, 'm_pid/I')

m_mother_id = array('i', [0])
tree.Branch('m_mother_id', m_mother_id, 'm_mother_id/I')

m_length = array('f', [0])
tree.Branch('m_length', m_length, 'm_length/F')

m_hit_type = array('i', [0])
tree.Branch('m_hit_type', m_hit_type, 'm_hit_type/F')
##########
v_x = array('f', [0])
vert_tree.Branch('v_x', v_x, 'v_x/F')
v_y = array('f', [0])
vert_tree.Branch('v_y', v_y, 'v_y/F')
v_z = array('f', [0])
vert_tree.Branch('v_z', v_z, 'v_z/F')
v_t = array('f', [0])
vert_tree.Branch('v_t', v_t, 'v_t/F')
v_mother_id = array('i', [0])
vert_tree.Branch('v_mother_id', v_mother_id, 'v_mother_id/I')


v_length = array('f', [0])
vert_tree.Branch('v_length', v_length, 'v_length/F')

v_hit_type = array('i', [0])
vert_tree.Branch('v_hit_type', v_hit_type, 'v_hit_type/F')

#run_nevents = 1000*len(sys.argv[1:])
evt_number=1
while evt_number <= n_evts :
    evt_number+=1
    appMgr.run(1)
    if not evt['MC/Particles']: break

    particles = evt['MC/Particles']
#    for particle in particles:
#	if abs(particle.particleID().pid()) == 2112:
#	    print particle

    hepmcevents = evt['Gen/HepMCEvents']
    mcmuonhits=evt['MC/Muon/Hits']
    mcmuonverts=evt['MC/Vertices']

    for vert in mcmuonverts:
	v_t[0] = vert.time()
	v_x[0] = vert.position().x()
	v_y[0] = vert.position().y()
	v_z[0] = vert.position().z()
	
	#v_ex[0] = vert.endVertex().x()
	#v_ey[0] = vert.endVertex().y()
	#v_ez[0] = vert.endVertex().z()

	#v_length[0] = vert.pathLength()
	try:
	    v_mother_id[0] = int(vert.mother().particleID().pid())
	except ReferenceError as e:
 	    print "caught:", e
	    v_mother_id[0] = -999
	if savefile:
            vert_tree.Fill()
	
    for hit in mcmuonhits:
#	if abs(hit.mcParticle().particleID().pid()) == 2112:
#	    print hit.entry().z()
	try:
#	    particle = hit.mcParticle()
	    mother_id = hit.mcParticle().mother().particleID().abspid()
	except ReferenceError as e:
	    print "caught: ", e  
	    mother_id = -999

	#else:
	    

        m_mother_id[0] = int(mother_id) 
#	if abs(mother_id) == 2112:
#            print mother_id
#            m_from_neut[0] = 1
#	else:
#	    m_from_neut[0] = 0

        sens_id = hit.sensDetID()
        sens_id = sens_id
        m_stat[0] = muondet.stationID(sens_id)
        chamber = muondet.stationID(sens_id)
        m_time[0] = hit.time()
        m_x_entry[0] = hit.entry().x()
        m_y_entry[0] = hit.entry().y()
        m_z_entry[0] = hit.entry().z()
        m_r[0] = leb.radius(hit)

        m_x_exit[0] = hit.exit().x()
        m_y_exit[0] = hit.exit().y()
        m_z_exit[0] = hit.exit().z()

	m_length[0] = sqrt(hit.displacement().Mag2())

#	entry_det = muondet.sensitiveVolumeID(hit.entry())
#	exit_det = muondet.sensitiveVolumeID(hit.exit())
#	if entry_det == exit_det:
#	    m_hit_type[0] = 1
#	else:
#	    print "sensitive detector difference"
#	    m_hit_type[0] = 2

        m_phi[0] = hit.entry().phi()
        m_reg[0] = muondet.regionID(sens_id)
        m_evt[0] = evt_number

	#get local track vector
	track = hit.exit()-hit.entry()
	m_loc_phi[0] = track.phi()
	m_loc_theta[0] = track.theta()
	
	m_pid[0] = hit.mcParticle().particleID().pid()

    	if savefile: 
            tree.Fill()

if savefile: 
    f_out.Write()
    f_out.Close()
#        id=hit.sensDetID()
#        chamber_id=id&0x1FFFFC
#        q=id>>15
#        m=(id>>12)&7
#        r=(id>>10)&3
#        chm=(id>>2)&255
#        gap=id&3
