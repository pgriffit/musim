import ROOT

f_std = ROOT.TFile.Open('./modelplots_std.root')
f_lt = ROOT.TFile.Open('./modelplots_lt.root')

f_std.cd()
std_vars = f_std.Get('vars')

f_lt.cd()
lt_vars = f_lt.Get('vars')

h_std = ROOT.TH1F('' ,'', 100, 0, 20000)
h_lt = ROOT.TH1F('' ,'', 100, 0, 20000)

h_lt_neutrons = ROOT.TH1F('' ,'', 100, 0, 20000)
h_lt_no_neutrons = ROOT.TH1F('' ,'', 100, 0, 20000)

#for i, e in enumerate(std_vars):
##    if i > 50000:
##	break
#    h_std.Fill(e.m_time)

#for i, e in enumerate(lt_vars):
##    if i > 50000:
##	break
#    h_lt.Fill(e.m_time)

for e in lt_vars:
    if abs(e.m_mother_id) == 2112:
	if e.m_time <1e9:
	    h_lt_neutrons.Fill(e.m_time)
    else:
	if e.m_time !=20000 and e.m_time < 1e9:
	    h_lt_no_neutrons.Fill(e.m_time)

c1 = ROOT.TCanvas()
c1.cd(0)
c1.SetLogy()
c1.SetLogx()
h_std.SetLineColor(3)
h_std.Draw()
h_lt.Draw("SAME")
c1.SaveAs('timings.pdf')

c2 = ROOT.TCanvas()
c2.cd(0)
#c2.SetLogy()

h_lt_no_neutrons.Scale(1/h_lt_no_neutrons.GetEntries())
h_lt_no_neutrons.Draw()
h_lt_neutrons.Scale(1/h_lt_neutrons.GetEntries())
h_lt_neutrons.SetLineColor(2)
h_lt_neutrons.Draw("SAME")
c2.SaveAs('timings_both.pdf')

c3 = ROOT.TCanvas()
c3.cd(0)
h_lt_neutrons.Draw()
c3.SaveAs('timings_neutrons.pdf')

