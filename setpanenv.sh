#source this file if you are getting openGL errors.

export PATH=/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/bin:${PATH}
export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/Resources/exlib/exlib/py
export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/Resources/CoinPython/scripts
export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/Resources/HEPVis/scripts/Python
export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/Resources/OnX/scripts/Python
export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/Resources/OnXLab/scripts/Python
export PYTHONPATH=${PYTHONPATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/bin
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/bin
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/lib
export OSC_HOME_DIR=/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt
export OSCLIB=/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/lib
export OSCBIN=/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt/bin
export OSC_home=/cvmfs/lhcb.cern.ch/lib/contrib/osc_vis/17.0.2/x86_64-slc6-gcc49-opt
unset SESSION_MANAGER
