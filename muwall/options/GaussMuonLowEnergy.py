# File containing options to lower thresholds for low energy
# simulation for muon system.
#
# @author Peter Griffith
# @date 2017-09-27


from Gaudi.Configuration import *
from GaudiKernel import SystemOfUnits
from Configurables import (GiGa, GiGaPhysListModular,
        GiGaRunActionSequence, TrCutsRunAction)
from Configurables import GiGaPhysConstructorOp, GiGaPhysConstructorHpd

giga = GiGa()

def setProductionCuts():
    from Gauss.Configuration import *

    Gauss().setPhysList(False)
    giga.addTool( GiGaPhysListModular("ModularPL") , name="ModularPL" ) 
    Gauss().PhysicsList = {"Em":'Opt1', "Hadron":'FTFP_BERT', "GeneralPhys":True, "LHCbPhys":True}

    #giga.ModularPL.PhysicsConstructors.append("GiGaPhysConstructorHpd")
   # giga.ModularPL.PhysicsConstructors +=  [ "GiGaExtPhysics/GeneralPhysics" ]
   # giga.ModularPL.PhysicsConstructors +=  [ "GiGaExtPhysics<EMPhysics>/EMPhysics" ]
   # giga.ModularPL.PhysicsConstructors +=  [ "GiGaExtPhysics<MuonPhysics>/MuonPhysics" ]
#  #  giga.ModularPL.PhysicsConstructors +=  [ "GiGaExtPhysics<HadronPhysicsLHEP>/LHEPPhysics" ]
   # giga.ModularPL.PhysicsConstructors +=  [ "GiGaExtPhysics<IonPhysics>/IonPhysics" ]
   # giga.ModularPL.PhysicsConstructors +=  [ "GiGaPhysConstructorOp" ]
   # giga.ModularPL.PhysicsConstructors +=  [ "GiGaPhysConstructorHpd" ]

   # giga.ModularPL.PhysicsConstructors.append("GiGaExtPhysics<HadronPhysicsFTFP_BERT_HP>/FTFP_BERT_HPPhysics")

                                                            #2005 cuts:
    giga.ModularPL.CutForElectron = 0.01 * SystemOfUnits.mm #0.5mm
    giga.ModularPL.CutForPositron = 0.01 * SystemOfUnits.mm #0.5mm
    giga.ModularPL.CutForGamma    = 0.01 * SystemOfUnits.mm #0.5mm

def setTrackingCuts():
    giga.addTool( GiGaRunActionSequence("RunSeq") , name="RunSeq" )
    giga.RunSeq.addTool( TrCutsRunAction("TrCuts") , name = "TrCuts" )
                                                                 #2005 cuts:
    giga.RunSeq.TrCuts.MuonTrCut     = 1.0 *  SystemOfUnits.MeV #10.0
    giga.RunSeq.TrCuts.pKpiCut       = 0.1  *  SystemOfUnits.MeV #0.1
    giga.RunSeq.TrCuts.NeutrinoTrCut = 0.0  *  SystemOfUnits.MeV #0.0
    giga.RunSeq.TrCuts.NeutronTrCut  = 0.0  *  SystemOfUnits.MeV #0.0
    giga.RunSeq.TrCuts.GammaTrCut    = 0.03 *  SystemOfUnits.MeV #0.03
    giga.RunSeq.TrCuts.ElectronTrCut = 0.03 *  SystemOfUnits.MeV #0.03
    giga.RunSeq.TrCuts.OtherTrCut    = 0.0  *  SystemOfUnits.MeV #0.0


def muonLowEnergySim():
    from Configurables import SimulationSvc
    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/MuonLowEnergy.xml"

muonLowEnergySim()
appendPostConfigAction(setProductionCuts) #ERROR
appendPostConfigAction(setTrackingCuts)
