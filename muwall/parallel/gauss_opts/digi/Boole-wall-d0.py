from Configurables import CondDB
CondDB().Upgrade     = True

from Configurables import Boole
#--Set database tags using those for Sim08
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20150424"
LHCbApp().CondDBtag = "sim-20140825-vc-mu100"

from Gaudi.Configuration import *
from Configurables import DDDBConf
DDDBConf(DbRoot = "/st100-gr1/griffith/databases/upgrade_iron_hcal/lhcb.xml")

from GaudiConf import IOHelper
input_files = ['/st100-gr1/griffith/sim/muwall_d0_1000.sim']
IOHelper('ROOT').inputFiles(input_files)

#Boole().DatasetName =  output
from Gaudi.Configuration import *
OutputStream("DigiWriter").Output = "DATAFILE='PFN:{}' TYP='POOL_ROOTTREE' OPT='REC'".format(
        input_files[0].replace('.sim','.digi')) 
