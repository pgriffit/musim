import os
import sys
from Configurables import CondDB
CondDB().Upgrade     = True

from Configurables import Boole
#--Set database tags using those for Sim08
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20150424"
LHCbApp().CondDBtag = "sim-20140825-vc-mu100"

from Gaudi.Configuration import *
from Configurables import DDDBConf
gtype = os.environ['gtype']
if gtype == 'hcal':
    DDDBConf(DbRoot = "/st100-gr1/griffith/databases/upgrade_reg_hcal/lhcb.xml")
elif gtype == 'wall':
    DDDBConf(DbRoot = "/st100-gr1/griffith/databases/upgrade_iron_hcal/lhcb.xml")
else:
    raise AttributeError('Geometry type "{}" not recognised')


from GaudiConf import IOHelper
input_files = [os.environ['simdata']]
IOHelper('ROOT').inputFiles(input_files)

#Boole().DatasetName =  output
from Gaudi.Configuration import *
OutputStream("DigiWriter").Output = "DATAFILE='PFN:{}' TYP='POOL_ROOTTREE' OPT='REC'".format(
        input_files[0].replace('.sim','.digi')) 
