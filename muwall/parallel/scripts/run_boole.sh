#location of options files
opts='/home/griffith/muon/muwall/parallel/boole_opts'

export simdata=$1

if [[ $1 == *"hcal"* ]]; then
    export gtype='hcal'
elif [[ $1 == *"wall"* ]]; then
    export gtype='wall'
else
    echo "Can't tell what geometry the file ${simdata} uses"
    exit 1
fi

logfile=$2
gaudirun.py $opts/Boole-Conf.py $opts/Boole-Upgrade-Job.py $opts/Boole-Upgrade-Geom.py  > $logfile
