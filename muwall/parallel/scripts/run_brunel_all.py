import os
import sys
from time import sleep

scripts_path = '/home/griffith/muon/muwall/parallel/'
for digifile_name in sys.argv[1:]:
    if '.digi' not in digifile_name:
        print('Skipping file: '+ digifile_name)
        continue


    logfile_name = os.path.join(
            scripts_path,
            'logs/brunel/',
            digifile_name.split('/')[-1].replace('.digi', '.log'))
    print(logfile_name)
    print('{} {} {} &'.format(
            os.path.join(scripts_path, 'scripts/run_brunel.sh'),
            digifile_name,
            logfile_name))
    print('Submitting Job for digi file: '+digifile_name)

    os.system('{} {} {} &'.format(
        os.path.join(scripts_path, 'scripts/run_brunel.sh'),
        digifile_name,
        logfile_name))
    sleep(20)
